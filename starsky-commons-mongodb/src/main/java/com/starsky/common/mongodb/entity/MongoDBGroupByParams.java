package com.starsky.common.mongodb.entity;

import lombok.Data;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;

/**
 * Title: MongoDBGroupByParams类
 * Description: TODO 类描述
 */
@Data
public class MongoDBGroupByParams {
    Criteria criteria;
    List<String> search;
    String groupName;
    String collectionName;
    private String sort = "_id";
    private Sort.Direction sortDes = Sort.Direction.DESC;
    private Integer pageNo = 0;
    private Integer pagteSize = 20;
}
