package com.starsky.common.mongodb.service;

import com.starsky.common.mongodb.dao.BookMongoDbDao;
import com.starsky.common.mongodb.entity.Book;
import com.starsky.common.mongodb.utils.MongodbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

/**
 * 描述:
 * mongo
 */
@Service
public class BookMongoDbService {
    private static final Logger logger = LoggerFactory.getLogger(BookMongoDbService.class);

    //注入新的CURD操作类
    @Autowired
    private BookMongoDbDao bookMongoDbDao;

    /**
     * 保存对象
     *
     * @param book
     * @return
     */
    public String saveObj2(Book book) {
        book.setCreateTime(new Date());
        book.setUpdateTime(new Date());
        //调用bookMongoDbDao父类中的添加方法
        bookMongoDbDao.queryById(1);
        bookMongoDbDao.save(book);
        return "添加成功";
    }

    public List<Book> find(Book book){
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.and("_id").is(book.getId());
        query.addCriteria(criteria);
        List<Book> books = MongodbUtils.find(query, Book.class);
        return books;
    }

}
