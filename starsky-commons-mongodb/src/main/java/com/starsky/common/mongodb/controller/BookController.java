package com.starsky.common.mongodb.controller;

import com.starsky.common.mongodb.entity.Book;
import com.starsky.common.mongodb.service.BookMongoDbService;
import com.starsky.common.mongodb.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author wangsh
 * @version 1.0
 * @date 2020/10/12 21:36
 * @email 1057718341@qq.com
 */
@Api("mongoTemplate直接操作数据库")
@RequestMapping("/mongo")
@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private BookMongoDbService bookMongoDbService;

    @ApiOperation("保存")
    @PostMapping("/save")
    public String saveObj(@RequestBody Book book) {
        return bookService.saveObj(book);
    }

    @ApiOperation("查询多条")
    @GetMapping("/findAll")
    public List<Book> findAll() {
        return bookService.findAll();
    }

    @ApiOperation("查询一条")
    @GetMapping("/findOne")
    public Book findOne(@RequestParam String id) {
        return bookService.getBookById(id);
    }

    @ApiOperation("根据名称查询")
    @GetMapping("/findOneByName")
    public Book findOneByName(@RequestParam String name) {
        return bookService.getBookByName(name);
    }

    @ApiOperation("更新")
    @PostMapping("/update")
    public String update(@RequestBody Book book) {
        return bookService.updateBook(book);
    }

    @ApiOperation("删除")
    @PostMapping("/delOne")
    public String delOne(@RequestBody Book book) {
        return bookService.deleteBook(book);
    }

    @ApiOperation("根据id删除")
    @GetMapping("/delById")
    public String delById(@RequestParam String id) {
        return bookService.deleteBookById(id);
    }

    @ApiOperation("查询所有")
    @GetMapping("/find")
    public List<Book> find(Book book) {
        List<Book> books = bookMongoDbService.find(book);
        return books;
    }

}
