package com.starsky.common.mongodb.service;

import com.starsky.common.data.RstData;
import com.starsky.common.mongodb.entity.Book;
import com.starsky.common.mongodb.utils.MongodbUtils;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 描述:
 * mongo
 *
 * @author zhengql
 * @date 2018/8/9 10:24
 */
@Service
public class Book2Service {
    private static final Logger logger = LoggerFactory.getLogger(Book2Service.class);

    /**
     * 保存对象
     *
     * @param book
     * @return
     */
    public RstData saveObj(Book book) {
        logger.info("--------------------->[MongoDB save start]");
        book.setCreateTime(new Date());
        book.setUpdateTime(new Date());
        MongodbUtils.save(book);
//        MongodbUtils.save(book,"book");
//        MongodbUtils.save(Arrays.asList(book),"book");

        return RstData.success();
    }


    /**
     * 查询所有
     *
     * @return
     */
    public List<Book> findAll() {
        logger.info("--------------------->[MongoDB find start]");
//        List<Book> list = MongodbUtils.findAll(Book.class);
        Map<String, Object> search = new HashMap<>();
        search.put("_id","123456");
        List<Book> list = MongodbUtils.find(search, Book.class);
        return list;
    }


    /***
     * 根据id查询
     * @param id
     * @return
     */
    public Book getBookById(String id) {
        logger.info("--------------------->[MongoDB find start]");
        Query query = new Query(Criteria.where("_id").is(id));
        return (Book)MongodbUtils.findOne(query, Book.class);
    }

    /**
     * 根据名称查询
     *
     * @param name
     * @return
     */
    public Book getBookByName(String name) {
        logger.info("--------------------->[MongoDB find start]");
        Query query = new Query(Criteria.where("name").is(name));
        return (Book)MongodbUtils.findOne(query, Book.class);
    }

    /**
     * 更新对象
     *
     * @param book
     * @return
     */
    public String updateBook(Book book) {
        logger.info("--------------------->[MongoDB update start]");
        Query query = new Query(Criteria.where("_id").is(book.getId()));
        Update update = new Update().set("publish", book.getPublish())
                .set("info", book.getInfo())
                .set("updateTime", new Date());
        //updateFirst 更新查询返回结果集的第一条
        MongodbUtils.updateFirst(query, update, Book.class);
        //updateMulti 更新查询返回结果集的全部
//        mongoTemplate.updateMulti(query,update,Book.class);
        //upsert 更新对象不存在则去添加
//        mongoTemplate.upsert(query,update,Book.class);
        return "success";
    }

    /***
     * 删除对象
     * @param book
     * @return
     */
    public String deleteBook(Book book) {
        logger.info("--------------------->[MongoDB delete start]");
        MongodbUtils.remove(book);
        return "success";
    }

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    public String deleteBookById(String id) {
        logger.info("--------------------->[MongoDB delete start]");
        //findOne
        Book book = getBookById(id);
        //delete
        deleteBook(book);
        return "success";
    }

    public List<Book> find(Book book) {
        List<Book> list = MongodbUtils.queryList(book, Book.class);
        return list;
    }

}
