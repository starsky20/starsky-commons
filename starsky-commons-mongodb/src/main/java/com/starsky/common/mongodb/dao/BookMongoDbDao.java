package com.starsky.common.mongodb.dao;
import com.starsky.common.mongodb.entity.Book;
import org.springframework.stereotype.Repository;

/**
 *
 * @author  wangsh
 * @date  2020/10/12 21:37
 * @version 1.0
  * @email 1057718341@qq.com
 */
@Repository
public class BookMongoDbDao extends BaseMongoDao<Book> {
    @Override
    protected Class<Book> getEntityClass() {
        return Book.class;
    }
}
