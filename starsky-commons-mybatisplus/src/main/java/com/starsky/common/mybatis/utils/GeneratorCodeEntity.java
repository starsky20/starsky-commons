package com.starsky.common.mybatis.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wansh
 * @version 1.0
 * @date 2020/9/20 19:52
 * @email 1057718341@qq.com
 */
@Data
@Slf4j
public class GeneratorCodeEntity {

    //项目路径
    private String projectPath;
    //作者
    private String author;
    //包名称
    private String packageName;
    //模块名称
    private String moduleName;
    //模板名称
    private String templatePath;
    //模板名称
    private String tableNames;// 表名，多个英文逗号分割;

}
