package com.starsky.common.mybatis.handler;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class PermissionController {
    //请求映射处理映射器
    //spring在启动时候将所有贴有请求映射标签：RequestMapper方法收集起来封装到该对象中
    @Autowired
    private RequestMappingHandlerMapping rmhm;
    //系统根路径
    @Value("${server.servlet.context-path}")
    private String contextPath;

    @RequestMapping("/reload")
    public String reload() throws Exception {
        //0：从数据库中查询出所有权限表达式，然后对比，如果已经存在了，跳过，不存在添加
//        List<String> resourcesList = permissionDAO.getAllResources();
        //1:获取controller中所有带有@RequestMapper标签的方法
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = rmhm.getHandlerMethods();
        for (RequestMappingInfo requestMappingInfo : handlerMethods.keySet()) {
            System.out.println("requestMappingInfo: " + requestMappingInfo);//{[/beaconArea],methods=[PUT]}
            //1、获取控制器请求路径
            String controllMappingUrl = "";
            String methodType = "";
            PatternsRequestCondition patternsCondition = requestMappingInfo.getPatternsCondition();
            Set<String> patterns = patternsCondition.getPatterns();
            if (patterns != null && patterns.size() > 0) {
                List<String> urls = new ArrayList<>(patterns);
                controllMappingUrl = urls.get(0);
            }
            Set<RequestMethod> methods = requestMappingInfo.getMethodsCondition().getMethods();
            if (methods != null && methods.size() > 0) {
                List<RequestMethod> urls = new ArrayList<>(methods);
                methodType = urls.get(0).name();
            }

            //2、获取方法请求路径
            HandlerMethod handlerMethod = handlerMethods.get(requestMappingInfo);
            System.out.println("handlerMethod: " + handlerMethod);
            String methodName = handlerMethod.getMethod().getName();

            String methodMappingUrl = "";
            String methodDesc = "";
            //获取方法所有注解
            Annotation[] annotations = handlerMethod.getMethod().getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof GetMapping) {//get请求
                    String methodUrl = ((GetMapping) annotation).value()[0];
                    methodMappingUrl = methodUrl.indexOf("/") != 1 ? methodUrl : "/" + methodUrl;
                } else if (annotation instanceof PostMapping) {//post请求
                    methodMappingUrl = "/" + methodName;
                } else if (annotation instanceof PutMapping) {//put请求
                    methodMappingUrl = "/" + methodName;
                } else if (annotation instanceof DeleteMapping) {//delete请求
                    methodMappingUrl = "/" + methodName;
                } else if (annotation instanceof ApiOperation) {//swagger接口标注
                    methodDesc = ((ApiOperation) annotation).value();
                }
            }
            //3.获取方法注解
            String url = contextPath + controllMappingUrl + methodMappingUrl;
            System.out.println("完整路径：" + url);
            System.out.println("方法描述：" + methodDesc);
            System.out.println("请求类型：" + methodType);
            //判断数据库已经存在url是否包含该路径即可
        }
        return "success";
    }

}
