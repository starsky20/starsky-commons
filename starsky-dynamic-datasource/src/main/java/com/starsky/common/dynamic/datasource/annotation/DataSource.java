package com.starsky.common.dynamic.datasource.annotation;

import java.lang.annotation.*;

/**
 * 多数据源注解，在service中使用该注解标识使用哪种数据源操作
 * 例如：@DataSource("slave1"),表示使用slave1数据源
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {
    String value() default "";
}
