package com.starsky.common.rabbitmq.direct;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
//@RabbitListener(queues = "TestDirectQueue")//监听的队列名称 TestDirectQueue
public class DirectReceiver2 {

    @RabbitHandler
    public void process(Map message) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>第二个： DirectReceiver消费者收到消息  : " +message );
    }

}
