package com.starsky.common.rabbitmq.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @desc: topic监听队列客户端
 * @author: wangsh
 * @time: 2020/11/13 15:39
 */
@Component
//@RabbitListener(queues = "topic.woman")
public class TopicReceiver2 {

    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println(">>>>>>>>topic.woman: >>>>>>TopicTotalReceiver消费者收到消息  : " + testMessage.toString());
    }
}
