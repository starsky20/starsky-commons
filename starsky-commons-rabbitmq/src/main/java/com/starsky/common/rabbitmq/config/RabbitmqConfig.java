package com.starsky.common.rabbitmq.config;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.SerializerMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class RabbitmqConfig {
    /**
     * rabbitmq初始化, 默认是单例模式，这里修改为多利，每个生产者、消费者一个示例bean
     *
     * @param connectionFactory
     * @return
     */
    @Bean
    @Scope("prototype")
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMandatory(true);
        template.setMessageConverter(new SerializerMessageConverter());

        //启消息确认机制,即确认消息已发送到交换机(Exchange)，用户查看手动、自动消息确认过程
//        template.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
//            @Override
//            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
//                System.out.println("ConfirmCallback:     " + "相关数据：" + correlationData);
//                System.out.println("ConfirmCallback:     " + "确认情况：" + ack);
//                System.out.println("ConfirmCallback:     " + "原因：" + cause);
//            }
//        });
//        //消息确认回调
//        template.setReturnCallback(new RabbitTemplate.ReturnCallback() {
//            @Override
//            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
//                System.out.println("ReturnCallback:     " + "消息：" + message);
//                System.out.println("ReturnCallback:     " + "回应码：" + replyCode);
//                System.out.println("ReturnCallback:     " + "回应信息：" + replyText);
//                System.out.println("ReturnCallback:     " + "交换机：" + exchange);
//                System.out.println("ReturnCallback:     " + "路由键：" + routingKey);
//            }
//        });

        return template;
    }

}
