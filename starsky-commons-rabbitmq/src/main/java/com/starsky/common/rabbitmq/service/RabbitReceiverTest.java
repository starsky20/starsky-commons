//package com.starsky.common.rabbitmq.service;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.TypeReference;
//import com.goo.commons.tools.utils.MQRstData;
//import com.goo.model.entity.VehicleEvent;
//import com.goo.mq.constant.QueueConstant;
//import com.goo.rabbitmq.event.EventService;
//import com.rabbitmq.client.Channel;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.rabbit.annotation.*;
//import org.springframework.amqp.support.AmqpHeaders;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.handler.annotation.Headers;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//import java.util.Map;
//
///**
// * 监听MQ
// */
//@Slf4j
//@Component
//public class ReceiverTest {
//
//    @Autowired
//    private EventService eventService;
//
//    /**
//     * 监听事件队列 EVENT_DIRECT_QUEUE
//     * 配置监听的哪一个队列，同时在没有queue和exchange的情况下会去创建并建立绑定关系
//     *
//     * @param channel
//     * @param json
//     * @param headers
//     */
//    @RabbitListener(bindings = @QueueBinding(
//            value = @Queue(value = QueueConstant.EVENT_DIRECT_QUEUE, durable = "true"),
//            exchange = @Exchange(name = QueueConstant.EVENT_DIRECT_EXCHANGE, type = "direct"),
//            key = QueueConstant.EVENT_DIRECT_ROUTING
//    ))
//    @RabbitHandler
//    public void receiveObjectDel(Channel channel, String json,
//                                 @Headers Map<String, Object> headers) {
//        log.info(">>>>>>>>>>>>>>>>>>>>>> 读取消息内容 Start >>>>>>>>>>>>>>>>>>>>>>");
//        log.info(">>>>>>>>入参：{}", json);
//
//        Long deliveryTag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
//        new com.goo.mq.service.ReadMQHandler() {
//            @Override
//            public void handler(String json) {
//                MQRstData<List<VehicleEvent>> mqRstData = null;
//                try {
//                    mqRstData = JSON.parseObject(json, new TypeReference<MQRstData<List<VehicleEvent>>>() {
//                    });
//                } catch (Exception e) {
//                    try {
//                        //消息被丢失
//                        channel.basicReject(deliveryTag, false);
//                    } catch (Exception e1) {
//                        e1.printStackTrace();
//                        log.error(">>>>>>>>反序列化数据,丢失消息异常：messageId：{}. 异常结果：{}", deliveryTag, e);
//                    }
//                    log.error(">>>>>>>>反序列化数据失败，丢弃该消息成功：messageId：{}", deliveryTag);
//                }
//                if (mqRstData == null) {
//                    log.info(">>>>>>>>>>消息内容为空");
//                    return;
//                }
//                List<VehicleEvent> list = mqRstData.getResult();
//                if (list == null || list.size() <= 0) {
//                    log.info(">>>>>>>>>>消息内容为空");
//                    return;
//                }
//
//                long time = System.currentTimeMillis();
//                //todo 业务处理
//                eventService.handler(mqRstData);
//                log.info(">>>>>>>>>>获取数据处理耗时(ms):" + (System.currentTimeMillis() - time));
//
//            }
//        }.readMq(channel, deliveryTag, json);
//    }
//}
