package com.starsky.common.rabbitmq.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @desc: topic监听队列客户端
 * @author: wangsh
 * @time: 2020/11/13 15:27
 */
@Component
//@RabbitListener(queues = "topic.man")
public class TopicReceiver {

    @RabbitHandler
    public void process(Map message) {
        System.out.println(">>>>>>>>>>>>>>>topic.man:>>>>TopicManReceiver消费者收到消息  : " + message.toString());
    }
}
