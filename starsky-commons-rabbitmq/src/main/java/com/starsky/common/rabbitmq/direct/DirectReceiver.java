package com.starsky.common.rabbitmq.direct;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
//@RabbitListener(queues = "TestDirectQueue")//监听的队列名称 TestDirectQueue
public class DirectReceiver {

    /**
     * Direct 默认即，点对点，一个生产者 一个消费者一一对应，消费者可以多个，但是每个消费者拿到的消息是不同的
     * 消费者是轮训从队列中取消息进行消费
     *
     * @param testMessage
     */
    @RabbitHandler
    public void process(Map testMessage) {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>第一个: DirectReceiver消费者收到消息  : " + testMessage.toString());
    }

}
