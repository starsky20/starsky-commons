#简介
===
    该组件主要提供基础工具类及其他基础公用组件，可用于其他任何项目提供基础支持。

##技术架构
===
    * SpringBoot2.2.4
    * mongodb
    * mybatis-plugs
    * hbase
    * rabbitmq
    * redis
    * beetlsql
    * Lombok
    * Druid数据监控 http://localhost:8080/druid
    * JavaMelody性能监控 http://localhost:8080/monitoring
    * Swagger接口描述文档 http://localhost:8080/swagger-ui.html

#组件介绍
## starsky-commons-beetlsql
    数据库操作组件beetlsql相关配置
     配置内容如下;
    #Local Config
    #在权限系统注册的系统编码
    sys.appNo=A8360D1FEBB74EF59DEA49457743A2D6
    #Auth Async Log Config
    #是否开启异步日志传输
    sys.logAsync=false
    #异步日志调用接口地址
    sys.logAsyncUrl=http://localhost:8801
    
    #Server
    server.servlet.context-path=/cache
    server.port=8901
    server.tomcat.uri-encoding=UTF-8
    server.tomcat.max-threads=100
    server.tomcat.min-spare-threads=20
    server.tomcat.max-connections=5000
    
    #Spring Config
    spring.http.encoding.charset=UTF-8
    spring.http.encoding.enabled=true
    spring.http.encoding.force=true
    spring.banner.charset=UTF-8
    spring.messages.encoding=UTF-8
    
    #Logging Config
    logging.path=/home/cache
    logging.config=classpath:logback-spring.xml
    logging.level.root=info
    logging.level.springfox.documentation=info
    logging.level.org.apache.coyote.http11.Http11InputBuffer=info
    logging.level.org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor=info
    
    ##database master Config
    #datasource.master.url=jdbc:mysql://localhost:3306/sys/wbs_ocs?useUnicode=true&characterEncoding=UTF-8&useSSL=false&allowMultiQueries=true
    datasource.master.url=jdbc:mysql://localhost:3306/sys/wbs_ocs?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&rewriteBatchedStatements=true&useSSL=false
    datasource.master.userName=root
    datasource.master.pwd=123456
    datasource.master.driver=com.mysql.jdbc.Driver
    datasource.master.maxActive=10
    datasource.master.initialSize=1
    datasource.master.maxWaitMillis=15000
    datasource.master.minIdle=5
    datasource.master.timeBetweenEvictionRunsMillis=60000
    datasource.master.minEvictableIdleTimeMillis=300000
    datasource.master.validationQuery=SELECT 'x'
    datasource.master.testWhileIdle=true
    datasource.master.testOnBorrow=false
    datasource.master.testOnReturn=false
    datasource.master.poolPreparedStatements=true
    datasource.master.maxPoolPreparedStatementPerConnectionSize=20
    datasource.master.filters=stat,slf4j
    datasource.master.connectionProperties=druid.stat.mergeSql=true;druid.stat.slowSqlMillis=2000
    datasource.master.useGlobalDataSourceStat=true
    
    #druid Config
    druidMonitor.allow=
    druidMonitor.deny=
    druidMonitor.loginUsername=boot
    druidMonitor.loginPassword=123456
    druidMonitor.resetEnable=false
    
    #swagger Config
    #是否开启Swagger
    swagger.enable=true
    swagger.contact=Wbs-Auth
    swagger.package=com.starsky
    
    #redis pub Config 公用配置不允许修改
    #redis pub Config
    # 是否开启redis缓存  true开启   false关闭
    spring.redis.open=false
    spring.redis.host=localhost
    spring.redis.port=6379
    # 密码（默认为空）
    spring.redis.password=123456
    #链接数据库
    spring.redis.database=12
    # 连接超时时长（毫秒）
    spring.redis.timeout=120000
    spring.redis.testOnBorrow=false
    # 连接池最大连接数（使用负值表示没有限制）
    spring.redis.jedis.pool.max-active=200
    # 连接池中的最大空闲连接
    spring.redis.jedis.pool.max-idle=20
    # 连接池中的最小空闲连接
    spring.redis.jedis.pool.min-idle=5
    # 连接池最大阻塞等待时间（使用负值表示没有限制）
    spring.redis.jedis.pool.max-wait=-1
    
    #spring session
    spring.session.store-type=redis
    spring.session.timeout=10D
    spring.session.redis.cleanup-cron=0 * * * * *
    spring.session.redis.namespace=spring
    spring.session.redis.flush-mode=on_save
    
    ## cache config
    ##设置缓存失效时间,默认单位秒（60*60*24*7L=7天）
    basedata.cache.expiretime=604800

## starsky-commons-cache
    缓存工具类
    配置内容如下;
    
## starsky-commons-framework
    基础配置相关
     配置内容如下;
     #Local Config
     #在权限系统注册的系统编码
     sys.appNo=A8360D1FEBB74EF59DEA49457743A2D6
     #Auth Async Log Config
     #是否开启异步日志传输
     sys.logAsync=false
     #异步日志调用接口地址
     sys.logAsyncUrl=http://localhost:8801
     
     #Server
     server.servlet.context-path=/framework
     server.port=8901
     server.tomcat.uri-encoding=UTF-8
     server.tomcat.max-threads=100
     server.tomcat.min-spare-threads=20
     server.tomcat.max-connections=5000
     
     #Spring Config
     spring.http.encoding.charset=UTF-8
     spring.http.encoding.enabled=true
     spring.http.encoding.force=true
     spring.banner.charset=UTF-8
     spring.messages.encoding=UTF-8
     
     #Logging Config
     logging.path=/home/framework
     logging.config=classpath:logback-spring.xml
     logging.level.root=info
     logging.level.springfox.documentation=info
     logging.level.org.apache.coyote.http11.Http11InputBuffer=info
     logging.level.org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor=info


## starsky-commons-hbase
    hbase操作及工具类
    配置内容如下;
    # hbase config
    hbase.zookeeper.property.clientPort = 2181
    hbase.zookeeper.quorum = 127.0.0.1
    hbase.client.retries.number=3
    zookeeper.recovery.retry=3

     
## starsky-commons-kafka
    kafka消息队列
     配置内容如下;
    #============== kafka==================
    kafka.consumer.zookeeper.connect=localhost:2181
    #============== kafka 生产者===================
    ## 指定kafka server的地址，集群配多个，中间，逗号隔开
    kafka.producer.servers=localhost:9092
    ## 写入失败时，重试次数。当leader节点失效，一个repli节点会替代成为leader节点，此时可能出现写入失败，
    ## 当retris为0时，produce不会重复。retirs重发，此时repli节点完全成为leader节点，不会产生消息丢失。
    kafka.producer.retries=0
    ## 每次批量发送消息的数量,produce积累到一定数据，一次发送
    kafka.producer.batch.size=4096
    kafka.producer.linger=1
    ## produce积累数据一次发送，缓存大小达到buffer.memory就发送数据
    kafka.producer.buffer.memory=40960
    ##procedure要求leader在考虑完成请求之前收到的确认数，用于控制发送记录在服务端的持久化，其值可以为如下：
    ##acks = 0 如果设置为零，则生产者将不会等待来自服务器的任何确认，该记录将立即添加到套接字缓冲区并视为已发送。在这种情况下，无法保证服务器已收到记录，并且重试配置将不会生效（因为客户端通常不会知道任何故障），为每条记录返回的偏移量始终设置为-1。
    ##acks = 1 这意味着leader会将记录写入其本地日志，但无需等待所有副本服务器的完全确认即可做出回应，在这种情况下，如果leader在确认记录后立即失败，但在将数据复制到所有的副本服务器之前，则记录将会丢失。
    ##acks = all 这意味着leader将等待完整的同步副本集以确认记录，这保证了只要至少一个同步副本服务器仍然存活，记录就不会丢失，这是最强有力的保证，这相当于acks = -1的设置。
    ##可以设置的值为：all, -1, 0, 1
    kafka.producer.acks=1
    
    #============== kafka =消费者==================
    ## 指定kafka server的地址，集群配多个，中间，逗号隔开
    kafka.consumer.servers=localhost:9092
    #指定topic
    kafka.consumer.topic=test
    ## 指定默认消费者group id --> 由于在kafka中，同一组中的consumer不会读取到同一个消息，依靠groud.id设置组名
    kafka.consumer.group.id=test
    ## smallest和largest才有效，如果smallest重新0开始读取，如果是largest从logfile的offset读取。一般情况下我们都是设置smallest
    kafka.consumer.auto.offset.reset=latest
    kafka.consumer.concurrency=10
    ## enable.auto.commit:true --> 设置自动提交offset
    kafka.consumer.enable.auto.commit=false
    ##如果'enable.auto.commit'为true，则消费者偏移自动提交给Kafka的频率（以毫秒为单位），默认值为5000。
    kafka.consumer.auto.commit.interval=100
    # 设置session回话超时时间
    kafka.consumer.session.timeout=6000


## starsky-commons-mongodb
    mongodb数据库
     配置内容如下;
    spring:
      data:
        mongodb:
          uri: mongodb://msguser:123456@localhost:27017/msg
    logging:
      level:
        org.springframework.data.mongodb.core.MongoTemplate: DEBUG

## starsky-commons-mybatisplus
    mybatisplus配置
    配置内容如下;
    server:
      port: 9990
    #数据源
    spring:
      datasource:
        dynamic:
          primary: master
          strict: false
          datasource:
    #      主库链接
            master:
              username: root
              password: test2016
              driver-class-name: com.mysql.cj.jdbc.Driver
              url: jdbc:mysql://localhost:3306/test?characterEncoding=utf8&useSSL=false
    #          从库链接
            slave_1:
              username: root
              password: test2016
              driver-class-name: com.mysql.cj.jdbc.Driver
              url: jdbc:mysql://localhost:3306/test?characterEncoding=utf8&useSSL=false
    #          从库链接
            slave_2:
              username: root
              password: test2016
              driver-class-name: com.mysql.cj.jdbc.Driver
              url: jdbc:mysql://localhost:3306/test?characterEncoding=utf8&useSSL=false
    #mybatis
    mybatis-plus:
      mapper-locations: classpath:mapper/*.xml
      type-aliases-package: com.*
      global-config:
        db-config:
          id-type: auto
          field-strategy: NOT_EMPTY
          db-type: MYSQL
      configuration:
        map-underscore-to-camel-case: true
        call-setters-on-nulls: true
    #日志
    logging:
      level:
        root: info

    
## starsky-commons-rabbitmq
    rabbitmq消费者及生产者
    配置内容如下;
    #rabbit config
    spring.rabbitmq.addresses=localhost:5672
    #登录用户
    spring.rabbitmq.username=admin
    #登录密码
    spring.rabbitmq.password=123456
    spring.rabbitmq.virtual-host=/
    #链接超时时间
    spring.rabbitmq.connection-timeout=10000
    # 开启消息确认机制
    spring.rabbitmq.publisher-confirms=true
    # 开启消息确认机制
    spring.rabbitmq.publisher-returns=true
    spring.rabbitmq.template.mandatory=true
    # 监听配置
    # 签收方式 # 手动应答
    spring.rabbitmq.listener.simple.acknowledge-mode=manual
    spring.rabbitmq.listener.simple.auto-startup=true
    # 不重回队列
    spring.rabbitmq.listener.simple.default-requeue-rejected=false
    # 初始并发量
    spring.rabbitmq.listener.simple.concurrency=5
    # 最大并发量
    spring.rabbitmq.listener.simple.max-concurrency=20
    # 最多一次消费多少条数据 -限流
    spring.rabbitmq.listener.simple.prefetch=1
    spring.rabbitmq.listener.simple.retry.enabled=true
    

## starsky-commons-redis
    redis缓存，配置如下：
    ##swagger Config
    ##是否开启Swagger
    swagger.enable=true
    #联系人
    swagger.contact=1057718431@qq.com
    #接口指定初始化包路径
    swagger.package=com.*

    #redis pub Config
    # 是否开启redis缓存  true开启   false关闭
    spring.redis.open=false
    spring.redis.host=localhost
    spring.redis.port=6379
    # 密码（默认为空）
    spring.redis.password=123456
    #链接数据库
    spring.redis.database=12
    # 连接超时时长（毫秒）
    spring.redis.timeout=120000
    spring.redis.testOnBorrow=false
    # 连接池最大连接数（使用负值表示没有限制）
    spring.redis.jedis.pool.max-active=200
    # 连接池中的最大空闲连接
    spring.redis.jedis.pool.max-idle=20
    # 连接池中的最小空闲连接
    spring.redis.jedis.pool.min-idle=5
    # 连接池最大阻塞等待时间（使用负值表示没有限制）
    spring.redis.jedis.pool.max-wait=-1
    
    #spring session
    spring.session.store-type=redis
    spring.session.timeout=10D
    spring.session.redis.cleanup-cron=0 * * * * *
    spring.session.redis.namespace=spring
    spring.session.redis.flush-mode=on_save

## starsky-commons-tools
    工具类

配置文件
===
