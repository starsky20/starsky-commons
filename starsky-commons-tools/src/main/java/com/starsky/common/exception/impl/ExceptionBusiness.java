package com.starsky.common.exception.impl;

import com.starsky.common.enums.EnumInfoType;
import com.starsky.common.enums.impl.EnumResult;
import com.starsky.common.exception.ExceptionInfoType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @Description TODO 业务层异常处理
 */
@Slf4j
public class ExceptionBusiness extends ExceptionInfoType {

    /**
     * 构造方法
     */
    private ExceptionBusiness(String code, String msg) {
        super(code, msg);
    }

    //=====================================================================
    public static ExceptionInfoType getIntance(EnumInfoType eit, String msg) {
//        String msgTemp = eit.getMsg() + (StringUtils.isNotBlank(msg) ? "，具体原因：" + msg : "");
        msg = (StringUtils.isEmpty(msg)) ? eit.getMsg() : msg;
        return new ExceptionBusiness(eit.getCode(), msg);
    }

    public static ExceptionInfoType getIntance(EnumInfoType eit) {
        return getIntance(eit, eit.getMsg());
    }

    public static ExceptionInfoType getIntance() {
        return getIntance(EnumResult.FAILED);
    }

    public static ExceptionInfoType getIntanceFmt(EnumInfoType eit, Object... arguments) {
        return new ExceptionBusiness(eit.getCode(), String.format(eit.getMsg(), arguments));
    }
    //=====================================================================
}
