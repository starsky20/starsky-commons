package com.starsky.common.exception;

import com.starsky.common.data.RstData;
import com.starsky.common.enums.impl.EnumResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(ExceptionInfoType.class)
    public RstData frsException(ExceptionInfoType e) {
        logger.error("捕获到业务异常", e);
        return RstData.faild(e);
    }

    @ResponseBody
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public RstData frsException(HttpMessageNotReadableException e) {
        logger.error("捕获到参数异常", e);
        return RstData.faild(EnumResult.W000001);
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public RstData frsException(Exception e) {
        logger.error("捕获到未知异常", e);
        return RstData.faild("未知异常");
    }
}
