package com.starsky.common.exception;

import com.starsky.common.enums.EnumInfoType;
import com.starsky.common.exception.impl.ExceptionBusiness;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description TODO 业务层异常处理
 */
@Slf4j
public abstract class ExceptionInfoType extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String code; // 异常对应的返回码
    private String msg; // 异常对应的描述信息

    /**
     * 构造方法
     *
     * @param code 异常对应的返回码
     * @param msg  异常对应的描述信息
     */
    public ExceptionInfoType(String code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }

    //===================继承类必须实现的方法===============================
    public static ExceptionInfoType getIntance(EnumInfoType eit, String msg) {
        return ExceptionBusiness.getIntance(eit, msg);
    }

    public static ExceptionInfoType getIntance(EnumInfoType eit) {
        return ExceptionBusiness.getIntance(eit,  eit.getMsg());
    }

    public static ExceptionInfoType getIntance() {
        return ExceptionBusiness.getIntance();
    }
    //=====================================================================
}
