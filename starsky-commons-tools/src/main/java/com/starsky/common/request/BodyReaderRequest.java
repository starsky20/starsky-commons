package com.starsky.common.request;

import com.starsky.common.utils.HttpUtils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * @Description TODO 获取请求参数
 */
public class BodyReaderRequest extends HttpServletRequestWrapper {

	private final byte[] body;

	public BodyReaderRequest(HttpServletRequest request) throws IOException {
		super(request);
		body = HttpUtils.getBodyString(request).getBytes(Charset.forName("UTF-8"));
	}

	public BodyReaderRequest(HttpServletRequest request, String body) throws IOException {
		super(request);
		this.body = body.getBytes(Charset.forName("UTF-8"));
	}


	@Override
	public BufferedReader getReader() throws IOException {
		return new BufferedReader(new InputStreamReader(getInputStream()));
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {

		final ByteArrayInputStream bais = new ByteArrayInputStream(body);

		return new ServletInputStream() {

			@Override
			public int read() throws IOException {
				return bais.read();
			}

			@Override
			public boolean isFinished() {
				return false;
			}

			@Override
			public boolean isReady() {
				return false;
			}

			@Override
			public void setReadListener(ReadListener readListener) {

			}
		};
	}
}
