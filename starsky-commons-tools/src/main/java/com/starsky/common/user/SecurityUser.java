package com.starsky.common.user;

import com.starsky.common.utils.HttpContextUtils;
import org.apache.commons.lang3.StringUtils;
import javax.servlet.http.HttpServletRequest;

/**
 * 用户
 */
public class SecurityUser {
//    private static UserDetailRedis userDetailRedis;
//
//    static {
//        userDetailRedis = SpringContextUtils.getBean(UserDetailRedis.class);
//    }
//
//    /**
//     * 获取用户信息
//     */
//    public static UserDetail getUser() {
//
//		//根据id获取用户
//        Long userId = getUserId();
//        if (userId != null) {
//            UserDetail user = userDetailRedis.get(userId);
//            return user;
//        }
//
//        //根据token获取用户
//        String token = getToken();
//        if (token != null && !"".equals(token)) {
//            UserDetail user = userDetailRedis.get(token);
//            return user;
//        }
//        return null;
//    }

    /**
     * 获取用户ID
     */
    public static Long getUserId() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (request == null) {
            return null;
        }
        String userId = request.getHeader("userId");
        if (StringUtils.isBlank(userId)) {
            return null;
        }
        return Long.parseLong(userId);
    }


    /**
     * 获取用户ID
     */
    public static String getToken() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (request == null) {
            return null;
        }
        String token = request.getHeader("token");
        return token;
    }


    /**
     * 获取部门ID
     */
    public static String getDeptId() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (request == null) {
            return null;
        }
        String deptId = request.getHeader("deptId");
        return deptId;
    }
}
