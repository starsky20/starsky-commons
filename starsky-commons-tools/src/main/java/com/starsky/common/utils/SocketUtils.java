package com.starsky.common.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * socket工具类
 */
public class SocketUtils {
    private static Log log = LogFactory.getLog(SocketUtils.class);
    //tcp连接地址
    private String host = "192.168.161.2";
    private int port = 8888;

    private SocketUtils() {
    }

    private SocketUtils(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public static SocketUtils getInstance(String host, int port) {
        SocketUtils socketUtils = new SocketUtils(host, port);
        return socketUtils;
    }

    /**
     * 发送数据到 RSU 设备
     *
     * @param params
     */
    public Map<Object, Object> sendOutMsg(byte[] params) {
        Map<Object, Object> map = new HashMap();
        //1:创建客户端的套接字
        Socket socket = null;
        try {
            //建立连接
            socket = new Socket(this.host, this.port);
            log.info(">>>客户端建立连接：host: " + host + ",port: " + port);
            //2:获取输出流
            OutputStream outputStream = socket.getOutputStream();
            //3 获取输入流
            InputStream inputStream = socket.getInputStream();
            //3:写数据到服务端
            outputStream.write(params);
            outputStream.flush();
            //告诉服务器。客户端这边数据写入完毕
            socket.shutdownOutput();

            //4:读取从服务器响应的数据
            List<String> resList = new ArrayList<String>();
            int len = 0;
            byte[] buf = new byte[2048];
            //如果available()的返回值为0,说明没有响应数据,可能是对方已经断开连接,
            if (inputStream.available() == 0) {
                log.info("服务端连接关闭");
            } else if (inputStream.available() > 0) {
                //如果available()的返回值大于0,说明有响应数据
                log.info("服务端连接正常，获取返回数据");
                resList = IOUtils.readLines(inputStream);
                log.info(">>>>>>>>服务端响应数据：" + resList);
            } else {
                log.info("服务端连接关闭");
            }
            map = getResult(resList, 200, "发送成功");
            log.info("发送成功");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("发送数据异常：" + e);
            map = getResult(null, 500, "发送数据异常: \r\n" + e.getMessage());
        } finally {
            IOUtils.closeQuietly(socket);
        }
        log.info("socket客户端关闭.........");

        return map;
    }

    /**
     * 生成接口
     *
     * @param obj：数据
     * @param code：状态码
     * @param msg：结果信息
     * @return
     */
    public static Map getResult(Object obj, int code, String msg) {
        HashMap<Object, Object> map = new HashMap();
        map.put("code", code);
        map.put("msg", msg);
        map.put("data", obj);
        return map;
    }

    /**
     * 生成接口
     *
     * @param obj：数据
     * @return
     */
    public static Map getResult(Object obj) {
        HashMap<Object, Object> map = new HashMap();
        map.put("code", 200);
        map.put("msg", "操作成功");
        map.put("data", obj);
        return map;
    }

    public static void main(String[] args) {
        byte[] buf = {1, 2};
        Map map = SocketUtils.getInstance("192.168.161.2", 8888).sendOutMsg(buf);
        System.out.println("执行结果：" + map);
    }


}
