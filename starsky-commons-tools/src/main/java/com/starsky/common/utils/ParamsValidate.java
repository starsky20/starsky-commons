package com.starsky.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.starsky.common.enums.impl.EnumResult;
import com.starsky.common.exception.impl.ExceptionBusiness;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @author wangsh
 * @Desc 参数校验
 */
public class ParamsValidate {

    /**
     * 验证报文字段是否必输项 obj为类对象
     */
    public static void validatePara(Object obj, Map<String, String> paraMap) {

        if (obj == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000001);
        }

        // 全部采用集合方式传递数据 一级json
        if (paraMap == null || paraMap.size() == 0) {
            return;
        }

        JSONObject jObject = (JSONObject) JSONObject.toJSON(obj);

        if (paraMap.size() > jObject.size()) {
            throw ExceptionBusiness.getIntance(EnumResult.W000001);
        }

        // 循环验证必输字段
        for (String key : paraMap.keySet()) {
            if (!jObject.containsKey(key)) {
                throw ExceptionBusiness.getIntance(EnumResult.W000001, paraMap.get(key) + "不能为空");
            }
            if (StringUtils.isBlank(jObject.getString(key))) {
                throw ExceptionBusiness.getIntance(EnumResult.W000001, paraMap.get(key) + "不能为空");
            }
        }
    }
}
