package com.starsky.common.utils.easypoi;
import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.models.auth.In;

import java.util.Date;

/**
 * @author wansh
 * @date 2020/9/22 22:08
 * @version 1.0
 * @email 1057718341@qq.com
 */
public class ExcelTestEntity {
    @Excel(name = "编号")
    private Integer id;
    @Excel(name = "文件名称")
    private String fileName;
    @Excel(name = "文件路径")
    private String filePath;
    @Excel(name = "文件类型")
    private Integer type;
    @Excel(name = "用户名")
    private String creatorName;
    @Excel(name = "创建时间", format = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;


    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
