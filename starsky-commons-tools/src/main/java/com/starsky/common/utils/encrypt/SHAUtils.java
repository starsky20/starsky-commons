package com.starsky.common.utils.encrypt;

import lombok.extern.slf4j.Slf4j;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @desc: sha加密
 * @author: wangsh
 */
@Slf4j
public class SHAUtils {

    public static byte[] sha(String input) {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA");
            sha.update(input.getBytes());
            return sha.digest();
        } catch (NoSuchAlgorithmException e) {
            log.error("=======SHA加密失败========");
        }
        return null;
    }
}
