package com.starsky.common.utils.word;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO word主题,各个分级标题集合,内容集
 */
public class WordDataInfo {
	
	/**
	 * 该word文档的标题(首页显示)
	 */
	private String subject;
	/**
	 * 标题(注意顺序)
	 */
	private List<Title> titles;
	/**
	 * word内容
	 */
	private Map<Title, List<WordContent>> content = new LinkedHashMap<Title, List<WordContent>>();
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public List<Title> getTitles() {
		return titles;
	}
	public void setTitles(List<Title> titles) {
		this.titles = titles;
	}
	public Map<Title, List<WordContent>> getContent() {
		return content;
	}
	public void setContent(Map<Title, List<WordContent>> content) {
		this.content = content;
	}
	
}
