package com.starsky.common.utils.os;

/**
 * @desc:判断操作系统工具类
 * @author: wangsh
 * @time: 2020/11/24 8:55
 */
public class OSUtils {

    //获取当前操作系统,转为小写
    public static String OS_NAME = System.getProperty("os.name").toLowerCase();

    /**
     * 获取当前操作系统,转为小写
     */
    public static String getOsName() {
        String OsName = System.getProperty("os.name");
        return OsName;
    }

    /**
     * 操作系统的架构
     */
    public static String getOsArch() {
        String osArch = System.getProperty("os.arch");
        return osArch;
    }

    /**
     * 操作系统的版本
     */
    public static String getOSVersion() {
        String OSVersion = System.getProperty("os.version");
        return OSVersion;
    }

    /**
     * 文件分隔符（在 UNIX 系统中是“/”）
     */
    public static String getFileSeparator() {
        String fileSeparator = System.getProperty("file.separator");
        return fileSeparator;
    }

    /**
     * 路径分隔符（在 UNIX 系统中是“:”）
     */
    public static String getPathSeparator() {
        String pathSeparator = System.getProperty("path.separator");
        return pathSeparator;
    }

    /**
     * 行分隔符（在 UNIX 系统中是“/”）
     */
    public static String getLineSeparator() {
        String lineSeparator = System.getProperty("line.separator");
        return lineSeparator;
    }

    /**
     * 用户的账户名称
     *
     * @return
     */
    public static String getUserName() {
        String userName = System.getProperty("user.name");
        return userName;
    }

    /**
     * 用户的主目录
     */
    public static String getUserHome() {
        String userHome = System.getProperty("user.home");
        return userHome;
    }

    /**
     * 用户的当前工作目录
     *
     * @return
     */
    public static String getUserDir() {
        String userDir = System.getProperty("user.dir");
        return userDir;
    }

    /**
     * Java 安装目录
     *
     * @return
     */
    public static String getJavaHome() {
        String javaHome = System.getProperty("java.home");
        return javaHome;
    }

    /**
     * Java 运行时环境版本
     *
     * @return
     */
    public static String getJavaVersion() {
        String javaVersion = System.getProperty("java.version");
        return javaVersion;
    }

    /**
     * Java 运行时环境供应商
     */
    public static String getJavaVendor() {
        String javaVendor = System.getProperty("java.vendor");
        return javaVendor;
    }

    /**
     * Java 供应商的 URL
     */
    public static String getJavaVendorUrl() {
        String javaVendor = System.getProperty("java.vendor.url");
        return javaVendor;
    }

    /**
     * Java 虚拟机实现版本
     */
    public static String getJavaVmVersion() {
        String javaVmVersion = System.getProperty("java.vm.version");
        return javaVmVersion;
    }

    /**
     * Java 虚拟机实现供应商
     */
    public static String getJavaVmVendor() {
        String javaVmVendor = System.getProperty("java.vm.vendor");
        return javaVmVendor;
    }

    /**
     * Java 虚拟机实现名称
     */
    public static String getJavaVmName() {
        String javaVmName = System.getProperty("java.vm.name");
        return javaVmName;
    }

    /**
     * Java 类格式版本号
     */
    public static String getJavaClassVersion() {
        String javaClassVersion = System.getProperty("java.class.version");
        return javaClassVersion;
    }

    /**
     * Java 类路径
     */
    public static String getJavaClassPath() {
        String javaClassPath = System.getProperty("java.class.path");
        return javaClassPath;
    }

    /**
     * 加载库时搜索的路径列表
     */
    public static String getJavaLibraryPath() {
        String javaLibraryPath = System.getProperty("java.library.path");
        return javaLibraryPath;
    }

    /**
     * 默认的临时文件路径
     */
    public static String getJavaIoTmpdir() {
        String javaIoTmpdir = System.getProperty("java.io.tmpdir");
        return javaIoTmpdir;
    }

    /**
     * 要使用的 JIT 编译器的名称
     */
    public static String getJavaCompiler() {
        String javaCompiler = System.getProperty("java.compiler");
        return javaCompiler;
    }

    /**
     * 一个或多个扩展目录的路径
     */
    public static String getJavaExtDirs() {
        String javaExtDirs = System.getProperty("java.ext.dirs");
        return javaExtDirs;
    }

    /**
     * 获取操作系统名字
     *
     * @return 操作系统名
     */
    public static OSEnum getOSname() {
        if (isAix()) {
            return OSEnum.AIX;
        } else if (isDigitalUnix()) {
            return OSEnum.Digital_Unix;
        } else if (isFreeBSD()) {
            return OSEnum.FreeBSD;
        } else if (isHPUX()) {
            return OSEnum.HP_UX;
        } else if (isIrix()) {
            return OSEnum.Irix;
        } else if (isLinux()) {
            return OSEnum.Linux;
        } else if (isMacOS()) {
            return OSEnum.Mac_OS;
        } else if (isMacOSX()) {
            return OSEnum.Mac_OS_X;
        } else if (isMPEiX()) {
            return OSEnum.MPEiX;
        } else if (isNetWare()) {
            return OSEnum.NetWare_411;
        } else if (isOpenVMS()) {
            return OSEnum.OpenVMS;
        } else if (isOS2()) {
            return OSEnum.OS2;
        } else if (isOS390()) {
            return OSEnum.OS390;
        } else if (isOSF1()) {
            return OSEnum.OSF1;
        } else if (isSolaris()) {
            return OSEnum.Solaris;
        } else if (isSunOS()) {
            return OSEnum.SunOS;
        } else if (isWindows()) {
            return OSEnum.Windows;
        } else {
            return OSEnum.Others;
        }
    }


    public static boolean isLinux() {
        return OS_NAME.indexOf("linux") >= 0;
    }

    public static boolean isMacOS() {
        return OS_NAME.indexOf("mac") >= 0 && OS_NAME.indexOf("os") > 0 && OS_NAME.indexOf("x") < 0;
    }

    public static boolean isMacOSX() {
        return OS_NAME.indexOf("mac") >= 0 && OS_NAME.indexOf("os") > 0 && OS_NAME.indexOf("x") > 0;
    }

    public static boolean isWindows() {
        return OS_NAME.indexOf("windows") >= 0;
    }

    public static boolean isOS2() {
        return OS_NAME.indexOf("os/2") >= 0;
    }

    public static boolean isSolaris() {
        return OS_NAME.indexOf("solaris") >= 0;
    }

    public static boolean isSunOS() {
        return OS_NAME.indexOf("sunos") >= 0;
    }

    public static boolean isMPEiX() {
        return OS_NAME.indexOf("mpe/ix") >= 0;
    }

    public static boolean isHPUX() {
        return OS_NAME.indexOf("hp-ux") >= 0;
    }

    public static boolean isAix() {
        return OS_NAME.indexOf("aix") >= 0;
    }

    public static boolean isOS390() {
        return OS_NAME.indexOf("os/390") >= 0;
    }

    public static boolean isFreeBSD() {
        return OS_NAME.indexOf("freebsd") >= 0;
    }

    public static boolean isIrix() {
        return OS_NAME.indexOf("irix") >= 0;
    }

    public static boolean isDigitalUnix() {
        return OS_NAME.indexOf("digital") >= 0 && OS_NAME.indexOf("unix") > 0;
    }

    public static boolean isNetWare() {
        return OS_NAME.indexOf("netware") >= 0;
    }

    public static boolean isOSF1() {
        return OS_NAME.indexOf("osf1") >= 0;
    }

    public static boolean isOpenVMS() {
        return OS_NAME.indexOf("openvms") >= 0;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(OSUtils.getOSname());// 获取系统类型
        System.out.println(OSUtils.isWindows());// 判断是否为windows系统
    }

}

