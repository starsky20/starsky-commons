package com.starsky.common.utils.word;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * TODO word内容类型
 */
public enum ContentType {
	Null(0),
	Text(1),
	Picture(2),
	Table(3)
	;

	private int value;

	private ContentType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	private static final Map<Integer, ContentType> toEnumMap = new LinkedHashMap<Integer, ContentType>();

	static {
		for (ContentType type : values()) {
			toEnumMap.put(type.getValue(), type);
		}
	}

	public static ContentType toEnum(int type) {
		if (toEnumMap.containsKey(type))
			return toEnumMap.get(type);
		return ContentType.Null;
	}
}
