//package com.starsky.common.utils.mail;
//
//import com.starsky.common.data.RstData;
//import org.apache.commons.lang.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.stereotype.Component;
//
//
///**
// * 使用该工具类，需要引入spring-boot-starter-mail，并将该工具列注入到容器中
// *
// * @author wangsh
// * @version 1.0
// * @date 2021/1/15 21:33
// * @email 1057718341@qq.com
// */
//@Component
//public class EmailSender {
//    private Logger log = LoggerFactory.getLogger(EmailSender.class);
//    @Autowired
//    private JavaMailSender sender;
//
//    private SimpleMailMessage getSimpleMailMessage(String from, String to, String subject, String content) {
//        SimpleMailMessage message = new SimpleMailMessage();
//        // 发送者邮箱
//        message.setFrom(from);
//        // 接收者邮箱
//        message.setTo(to);
//        // 主题
//        message.setSubject(subject);
//        // 邮件内容
//        message.setText(content);
//        return message;
//    }
//
//    /**
//     * 接受告警通知并发送至邮箱
//     */
//    public RstData sendMessage(String from, String to, String subject, String content) {
//        subject = StringUtils.isEmpty(subject) ? "告警邮件" : subject;
//        SimpleMailMessage message = getSimpleMailMessage(from, to, subject, content);
//        sender.send(message);
//        log.info("告警邮件已发送...");
//
//        return RstData.success("发送成功");
//    }
//
//    /**
//     * 测试产生告警
//     *
//     * @return
//     */
//    public String producer() {
//        log.info("received a request");
//        int i = 1 / 0;
//        return "this message from producer";
//    }
//
//}
