package com.starsky.common.utils.mail;

import lombok.Data;

/**
 * Title: MailDto类
 * Description: TODO 类描述
 */
@Data
public class MailDto {

    // 邮件发送协议
    private String protocol = "smtp";

    // SMTP邮件服务器
    private String host = "smtp.sina.com";

    // SMTP邮件服务器默认端口
    private String port = "25";

    // 是否要求身份认证
    private Boolean auth = true;

    // 是否启用调试模式（启用调试模式可打印客户端与服务器交互过程时一问一答的响应消息）
    private Boolean debug = false;

    // 发件人
    private String from;

    // 收件人
    private String to;

    // 发信密码
    private String secret;


    public static MailDto getData(String from, String to, String secret) {
        MailDto entry = new MailDto();
        entry.setFrom(from);
        entry.setTo(to);
        entry.setSecret(secret);
        return entry;
    }

}
