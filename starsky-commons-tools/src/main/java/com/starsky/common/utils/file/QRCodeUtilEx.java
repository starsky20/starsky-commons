package com.starsky.common.utils.file;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.font.FontDesignMetrics;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Hashtable;


/**
 * 生成二位码： 支持生成待文字、logo、代跳转路径二维码
 */
public class QRCodeUtilEx {
    private static Logger logger = LoggerFactory.getLogger(QRCodeUtilEx.class);
    private static final String CHARSET = "utf-8";
    private static final String FORMAT_NAME = "JPG";
    // 二维码尺寸
    private static final int QRCODE_SIZE = 300;
    // LOGO宽度
    private static final int WIDTH = 60;
    // LOGO高度
    private static final int HEIGHT = 60;
    // 字体大小
    private static final int FONT_SIZE = 18;


    /**
     * 测试生成二维码
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        // 存放在二维码中的内容
        String text = "我是小铭";
        // 嵌入二维码的图片路径
        String imgPath = "";
        // 生成的二维码的路径及名称
        String destPath = "D:\\home\\firealarm\\images\\1112.jpg";
        //生成二维码
//        encode（String content, String bottomDes, String imgPath, boolean needCompress）
        QRCodeUtilEx.encode(text, "测试生成底部文字", imgPath, destPath, true);
        // 解析二维码
//        String str = QRCodeUtilEx.decode(destPath);
        // 打印出解析出的内容
//        System.out.println(str);
    }

    /**
     * 创建二维码
     *
     * @param content:图片内容，比如填些url地址，一些自定义标识符等等
     * @param bottomDes：文字说明，在图片底部生成文字说明
     * @param imgPath：插入图片，使用的logo等
     * @param needCompress：是否压缩
     * @return
     * @throws Exception
     */
    private static BufferedImage createImage(String content, String bottomDes, String imgPath, boolean needCompress) throws Exception {

        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        //解码设置编码方式为：utf-8
        hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QRCODE_SIZE, QRCODE_SIZE, hints);
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        int tempHeight = height;

        boolean needDescription = (null != bottomDes && !"".equals(bottomDes));
        if (needDescription) {
            tempHeight += 30;
        }

        BufferedImage image = new BufferedImage(width, tempHeight, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
            }
        }

        // 插入图片
        if (imgPath != null && !"".equals(imgPath)) {
            QRCodeUtilEx.insertImage(image, imgPath, needCompress);
        }

        //添加底部文字
        if (needDescription) {
            QRCodeUtilEx.addFontImage(image, bottomDes);
        }

        return image;
    }

    /**
     * 添加 底部图片文字
     *
     * @param source      图片源
     * @param declareText 文字本文
     */
    private static void addFontImage(BufferedImage source, String declareText) throws UnsupportedEncodingException {
        BufferedImage textImage = strToImage(declareText, QRCODE_SIZE, 50);
        Graphics2D graph = source.createGraphics();
        //开启文字抗锯齿
        graph.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        int width = textImage.getWidth(null);
        int height = textImage.getHeight(null);

        Image src = textImage;
        graph.drawImage(src, 0, QRCODE_SIZE - 20, width, height, null);
        graph.dispose();
    }

    /**
     * 将文字添加到图片底部
     *
     * @param str：文字内容
     * @param width：二维码宽度
     * @param height：文字高度
     * @return
     */
    private static BufferedImage strToImage(String str, int width, int height) throws UnsupportedEncodingException {
        BufferedImage textImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = (Graphics2D) textImage.getGraphics();
        //开启文字抗锯齿
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setBackground(Color.WHITE);
        g2.clearRect(0, 0, width, height);
        g2.setPaint(Color.BLACK);
        FontRenderContext context = g2.getFontRenderContext();
        Font font = new Font("微软雅黑", Font.BOLD, FONT_SIZE);
        g2.setFont(font);

        str = new String(str.getBytes(), "utf-8");//有没有这句话
//        str = new String(str.getBytes("UTF-8"), "ISO-8859-1");
        //如果不想更改源码，则将字符串转换成ISO-8859-1编码
        LineMetrics lineMetrics = font.getLineMetrics(str, context);
        FontMetrics fontMetrics = FontDesignMetrics.getMetrics(font);
        float offset = (width - fontMetrics.stringWidth(str)) / 2;
        float y = (height + lineMetrics.getAscent() - lineMetrics.getDescent() - lineMetrics.getLeading()) / 2;

        g2.drawString(str, (int) offset, (int) y);

        return textImage;
    }

    /**
     * 插入图片
     *
     * @param source
     * @param imgPath：插入图片
     * @param needCompress：是否压缩
     * @throws Exception
     */
    private static void insertImage(BufferedImage source, String imgPath, boolean needCompress) throws Exception {
        File file = new File(imgPath);
        if (!file.exists()) {
            System.err.println("" + imgPath + "   该文件不存在！");
            return;
        }
        Image src = ImageIO.read(new File(imgPath));
        int width = src.getWidth(null);
        int height = src.getHeight(null);
        if (needCompress) { // 压缩LOGO
            if (width > WIDTH) {
                width = WIDTH;
            }
            if (height > HEIGHT) {
                height = HEIGHT;
            }
            Image image = src.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null); // 绘制缩小后的图
            g.dispose();
            src = image;
        }
        // 插入LOGO
        Graphics2D graph = source.createGraphics();
        int x = (QRCODE_SIZE - width) / 2;
        int y = (QRCODE_SIZE - height) / 2;
        graph.drawImage(src, x, y, width, height, null);
        Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
        graph.setStroke(new BasicStroke(3f));
        graph.draw(shape);
        graph.dispose();
    }

    /**
     * 创建目录
     *
     * @param destPath
     */
    public static void mkdirs(String destPath) {
        File file = new File(destPath);
        // 当文件夹不存在时，mkdirs会自动创建多层目录，区别于mkdir．(mkdir如果父目录不存在则会抛出异常)
        if (!file.exists() && !file.isDirectory()) {
            file.mkdirs();
        }
    }

    /**
     * 生成二维码
     *
     * @param content:图片内容，比如填些url地址，一些自定义标识符等等
     * @param bottomDes：文字说明，在图片底部生成文字说明
     * @param imgPath：生成二维码时，插入图片,使用的logo等
     * @param needCompress：是否压缩
     * @param destPath：生成二维码存储文件
     * @throws Exception
     */
    public static void encode(String content, String bottomDes, String imgPath, String destPath, boolean needCompress) throws Exception {
        ImageIO.setUseCache(false);
        BufferedImage image = QRCodeUtilEx.createImage(content, bottomDes, imgPath, needCompress);
        mkdirs(destPath);
//        String file = new Random().nextInt(99999999) + ".jpg";
//        ImageIO.write(image, FORMAT_NAME, new File(destPath + "/" + file));
        ImageIO.write(image, FORMAT_NAME, new File(destPath));
    }

    /**
     * 生成二维码
     *
     * @param content:图片内容，比如填些url地址，一些自定义标识符等等
     * @param bottomDes：文字说明，在图片底部生成文字说明
     * @param imgPath：生成二维码时，插入图片,使用的logo等
     * @param needCompress：是否压缩
     * @throws Exception
     */
    public static BufferedImage encode(String content, String bottomDes, String imgPath, boolean needCompress) throws Exception {
        BufferedImage image = QRCodeUtilEx.createImage(content, bottomDes, imgPath, needCompress);
        return image;
    }

    /**
     * 生成二维码
     *
     * @param content:图片内容，比如填些url地址，一些自定义标识符等等
     * @param bottomDes：文字说明，在图片底部生成文字说明
     * @param imgPath：生成二维码时，插入图片,使用的logo等
     * @throws Exception
     */
    public static void encode(String content, String bottomDes, String imgPath, String destPath) throws Exception {
        QRCodeUtilEx.encode(content, bottomDes, imgPath, destPath, false);
    }
    // 被注释的方法
    /*
     * public static void encode(String content, String destPath, boolean
     * needCompress) throws Exception { QRCodeUtil.encode(content, null, destPath,
     * needCompress); }
     */


    /**
     * 生成一个包含内容，底部有文字的二维码
     *
     * @param content:图片内容，比如填些url地址，一些自定义标识符等等
     * @param bottomDes：文字说明，在图片底部生成文字说明
     * @param destPath：生成二维码输出图片
     * @throws Exception
     */
    public static void encode(String content, String bottomDes, String destPath) throws Exception {
        QRCodeUtilEx.encode(content, bottomDes, null, destPath, false);
    }

    /**
     * 生成一个包含内容，底部有文字，有logo的二维码
     *
     * @param content:图片内容，比如填些url地址，一些自定义标识符等等
     * @param bottomDes：文字说明，在图片底部生成文字说明
     * @param imgPath：二维码图片
     * @param output：生成二维码输出图片
     * @throws Exception
     */
    public static void encode(String content, String bottomDes, String imgPath, OutputStream output, boolean needCompress)
            throws Exception {
        BufferedImage image = QRCodeUtilEx.createImage(content, bottomDes, imgPath, needCompress);
        ImageIO.write(image, FORMAT_NAME, output);
    }

    /**
     * 生成一个包含内容，底部有文字的二维码
     *
     * @param content:图片内容，比如填些url地址，一些自定义标识符等等
     * @param bottomDes：文字说明，在图片底部生成文字说明
     * @param output：输出二维码图片
     * @throws Exception
     */
    public static void encode(String content, String bottomDes, OutputStream output) throws Exception {
        QRCodeUtilEx.encode(content, bottomDes, null, output, false);
    }

    /**
     * 解析二维码内容
     *
     * @throws Exception
     */
    public static String decode(InputStream in) throws Exception {

        BufferedImage image = ImageIO.read(in);
        if (image == null) {
            return null;
        }
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Result result;
        Hashtable hints = new Hashtable();
        hints.put(DecodeHintType.CHARACTER_SET, CHARSET);
        result = new MultiFormatReader().decode(bitmap, hints);
        String resultStr = result.getText();

        return resultStr;
    }


    /**
     * 解析二维码内容
     *
     * @throws Exception
     */
    public static String decode(File file) throws Exception {
        FileInputStream inputStream = new FileInputStream(file);
        String resultStr = decode(inputStream);
        return resultStr;
    }

    /**
     * 解析二维码内容
     *
     * @param path
     * @return
     * @throws Exception
     */
    public static String decode(String path) throws Exception {
        return QRCodeUtilEx.decode(new File(path));
    }

}

