package com.starsky.common.utils.word;

import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

/**
 * @author  wansh
 * @des 数据导出成word工具
 * @date  2020/9/19 22:20
 * @version 1.0
 * @email 1057718341@qq.com
 */
public class WordFactory {

	/**
	 * TODO 得到单元格第一个Paragraph
	 *
	 */
	public static XWPFParagraph getCellFirstParagraph(XWPFTableCell cell) {
		XWPFParagraph p;
		if (cell.getParagraphs() != null && cell.getParagraphs().size() > 0) {
			p = cell.getParagraphs().get(0);
		} else {
			p = cell.addParagraph();
		}
		return p;
	}

	/**
	 * TODO 获取段落中的文本域
	 *
	 * @param isInsert
	 *            是否是新增
	 * 
	 * @param isNewLine
	 *            是否新行
	 */
	public static XWPFRun getOrAddParagraphFirstRun(XWPFParagraph paragraph, boolean isInsert, boolean isNewLine) {
		XWPFRun pRun = null;
		if (isInsert) {
			pRun = paragraph.createRun();
		} else {
			if (paragraph.getRuns() != null && paragraph.getRuns().size() > 0) {
				pRun = paragraph.getRuns().get(0);
			} else {
				pRun = paragraph.createRun();
			}
		}
		if (isNewLine) {
			pRun.addBreak();
		}
		return pRun;
	}

	/**
	 * TODO 得到段落CTPPr
	 *
	 */
	public static CTPPr getParagraphCTPPr(XWPFParagraph paragraph) {
		CTPPr pPPr = null;
		if (paragraph.getCTP() != null) {
			if (paragraph.getCTP().getPPr() != null) {
				pPPr = paragraph.getCTP().getPPr();
			} else {
				pPPr = paragraph.getCTP().addNewPPr();
			}
		}
		return pPPr;
	}

	/**
	 * TODO 得到XWPFRun的CTRPr
	 *
	 */
	public static CTRPr getRunCTRPr(XWPFParagraph paragraph, XWPFRun pRun) {
		CTRPr pRpr = null;
		if (pRun.getCTR() != null) {
			pRpr = pRun.getCTR().getRPr();
			if (pRpr == null) {
				pRpr = pRun.getCTR().addNewRPr();
			}
		} else {
			pRpr = paragraph.getCTP().addNewR().addNewRPr();
		}
		return pRpr;
	}

	/**
	 * TODO 得到Table的CTTblPr,不存在则新建
	 */
	public static CTTblPr getTableCTTblPr(XWPFTable table) {
		CTTbl ttbl = table.getCTTbl();
		CTTblPr tblPr = ttbl.getTblPr() == null ? ttbl.addNewTblPr() : ttbl.getTblPr();
		return tblPr;
	}

	/**
	 * TODO 得到Cell的CTTcPr,不存在则新建
	 */
	public static CTTcPr getCellCTTcPr(XWPFTableCell cell) {
		CTTc cttc = cell.getCTTc();
		CTTcPr tcPr = cttc.isSetTcPr() ? cttc.getTcPr() : cttc.addNewTcPr();
		return tcPr;
	}

	/**
	 * TODO 得到CTTrPr,不存在则新建
	 * 
	 */
	public static CTTrPr getRowCTTrPr(XWPFTableRow row) {
		CTRow ctRow = row.getCtRow();
		CTTrPr trPr = ctRow.isSetTrPr() ? ctRow.getTrPr() : ctRow.addNewTrPr();
		return trPr;
	}
}
