package com.starsky.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @Description TODO 密码生成单元
 */
public class GenPwdUtil {
    //密码能包含的特殊字符
    private static final char[] allowedSpecialCharactors = {'@', '#', '$', '%', '^', '&', '*', '-', '_', '=', '+', '|', '.', '?'};
    private static final int letterRange = 26;
    private static final int numberRange = 10;
    private static final int spCharactorRange = allowedSpecialCharactors.length;
    private static final Random random = new Random();
    public static final String[] TYPE_ULCN = {"U", "L", "C", "N"};
    public static final String[] TYPE_LCN = {"L", "C", "N"};
    public static final String[] TYPE_UNC = {"U", "C", "N"};
    public static final String[] TYPE_ULN = {"U", "L", "N"};
    public static final String[] TYPE_ULC = {"U", "L", "C"};
    public static final String[] TYPE_UL = {"U", "L"};
    public static final String[] TYPE_UC = {"U", "C"};
    public static final String[] TYPE_UN = {"U", "N"};
    public static final String[] TYPE_LC = {"L", "C"};
    public static final String[] TYPE_LN = {"L", "N"};
    public static final String[] TYPE_CN = {"C", "N"};
    public static final String[] TYPE_U = {"U"};
    public static final String[] TYPE_L = {"L"};
    public static final String[] TYPE_C = {"C"};
    public static final String[] TYPE_N = {"N"};

    private GenPwdUtil() {
    }

    public static String genPwd() {
        return genPwd(16, TYPE_ULCN);
    }

    public static String genPwd(int pwdLen, String[] type) {
        char[] password = new char[pwdLen];
        List<Integer> pwCharsIndex = new ArrayList();
        for (int i = 0; i < password.length; i++) {
            pwCharsIndex.add(i);
        }
        while (pwCharsIndex.size() > 0) {
            int pwIndex = pwCharsIndex.remove(random.nextInt(pwCharsIndex.size()));//随机填充一位密码
            Integer num = random.nextInt(type.length);
            password[pwIndex] = generateCharacter(type[num]).charValue();
        }
        return String.valueOf(password);
    }

    private static Character generateCharacter(String type) {
        Character c = null;
        int rand;
        switch (type) {
            case "L"://随机小写字母
                rand = random.nextInt(letterRange);
                rand += 97;
                c = new Character((char) rand);
                break;
            case "U"://随机大写字母
                rand = random.nextInt(letterRange);
                rand += 65;
                c = new Character((char) rand);
                break;
            case "N"://随机数字
                rand = random.nextInt(numberRange);
                rand += 48;
                c = new Character((char) rand);
                break;
            case "C"://随机特殊字符
                rand = random.nextInt(spCharactorRange);
                c = new Character(allowedSpecialCharactors[rand]);
                break;
        }
        return c;
    }

    public static void main(String[] args) {
        System.out.println(genPwd(64, TYPE_ULCN));
    }
}
