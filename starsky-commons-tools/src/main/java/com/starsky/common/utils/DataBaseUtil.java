package com.starsky.common.utils;

import com.starsky.common.constant.DataBaseEnum;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wangsh
 * @date 2019/8/1 8:48
 * @Description 数据库工具类
 */
public class DataBaseUtil {

    private static final String FORMAT_DATE24 = "yyyy-MM-dd HH:mm:ss";

    private static final String FORMAT_DATE = "yyyy-MM-dd";

    private static final String FORMAT_MM = "MM";
    private static final String FORMAT_YY = "yyyy";

    private static SimpleDateFormat DATE_FORMAT_MM = new SimpleDateFormat(FORMAT_MM);
    private static SimpleDateFormat DATE_FORMAT_YY = new SimpleDateFormat(FORMAT_YY);
    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(FORMAT_DATE);


    public static String getTableSubfix(int type) {

        String subffix = null;
        if (type == 1) {
            subffix = DataBaseEnum.ONE.getValue();
        } else if (type == 2) {
            subffix = DataBaseEnum.SIX.getValue();
        } else if (type == 3) {
            subffix = DataBaseEnum.NINE.getValue();
        } else if (type == 4) {
            subffix = DataBaseEnum.TWELVE.getValue();
        }
        if (StringUtils.isNotBlank(subffix)) {
            String year = DATE_FORMAT_YY.format(new Date());
            subffix = year + subffix;
        }
        return subffix;
    }


    /**
     * 根据月份获取对应的季度后缀
     *
     * @param subffix
     * @return
     */
    private static String getTableEnum(String subffix) {

        //月份后缀：如01,02,03等
        if (StringUtils.isEmpty(subffix)) {
            return "";
        }

        String tableSubffix = null;
        DataBaseEnum[] values = DataBaseEnum.values();
        for (DataBaseEnum dataBase : values) {
            if (subffix.trim().equals(dataBase.getKey().trim())) {
                tableSubffix = dataBase.getValue().trim();
                break;
            }
        }
        return tableSubffix;
    }

    /**
     * 获取表名称后缀
     *
     * @param date
     * @return
     */
    public static String getTableSubffix(Date date, String regex) {

        if (date == null) {
            String tableSubffix = getTableSubffix(new Date(), null);
            return tableSubffix;
        }

        if (StringUtils.isEmpty(regex)) {
            regex = FORMAT_MM;
        }
        SimpleDateFormat format = new SimpleDateFormat(regex);
        String subffix = format.format(date);

        // 根据月份获取对应的季度后缀
        String tableSubffix = getTableEnum(subffix);
        if (StringUtils.isNotBlank(tableSubffix)) {
            String year = DATE_FORMAT_YY.format(date);
            tableSubffix = year + tableSubffix;
        }
        return tableSubffix;
    }


    /**
     * 获取表名称后缀
     *
     * @param date
     * @return
     */
    public static String getTableSubffix(Date date) {

        String tableSubffix = getTableSubffix(date, null);
        return tableSubffix;
    }

    /**
     * 获取表名称后缀
     *
     * @param time
     * @return
     */
    public static String getTableSubffix(String time, String regex) throws ParseException {

        if (StringUtils.isEmpty(time)) {
            String tableSubffix = getTableSubffix(new Date(), null);
            return tableSubffix;
        }
        if (StringUtils.isEmpty(regex)) {
            regex = FORMAT_DATE;
        }
        SimpleDateFormat format = new SimpleDateFormat(regex);
        Date date = format.parse(time);
        String tableSubffix = getTableSubffix(date, null);
        return tableSubffix;
    }

    /**
     * 获取表名称后缀
     *
     * @param time
     * @return
     */
    public static String getTableSubffix(String time) throws ParseException {

        String tableSubffix = getTableSubffix(time, null);
        return tableSubffix;
    }


    public static void main(String[] args) throws ParseException {

        String tableSubffix = DataBaseUtil.getTableSubffix(new Date());
        System.out.println("tableSubffix = " + tableSubffix);

        String tableSubffix1 = DataBaseUtil.getTableSubffix("2019-03-01 10:41:20");
        System.out.println("tableSubffix1 = " + tableSubffix1);
    }
}
