package com.starsky.common.utils.word;

import java.util.List;

/**
 * TODO word 分级标题
 */
public class Title {
	
	/**
	 * 子标题
	 */
	private List<Title> childs;
	/**
	 * 标题级别 
	 */
	private TitleLevel titleLevel;  //最多9级
	/**
	 * 标题内容
	 */
	private String titleName;
	
	public List<Title> getChilds() {
		return childs;
	}
	public void setChilds(List<Title> childs) {
		this.childs = childs;
	}
	public TitleLevel getTitleLevel() {
		return titleLevel;
	}
	public void setTitleLevel(TitleLevel titleLevel) {
		this.titleLevel = titleLevel;
	}
	public String getTitleName() {
		return titleName;
	}
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((titleLevel == null) ? 0 : titleLevel.hashCode());
		result = prime * result
				+ ((titleName == null) ? 0 : titleName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Title other = (Title) obj;
		if (titleLevel != other.titleLevel)
			return false;
		if (titleName == null) {
			if (other.titleName != null)
				return false;
		} else if (!titleName.equals(other.titleName))
			return false;
		return true;
	}
	
}
