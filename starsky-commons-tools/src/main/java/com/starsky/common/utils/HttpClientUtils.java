package com.starsky.common.utils;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * http工具类
 */
public class HttpClientUtils {

    private static final Logger log = (Logger) LoggerFactory.getLogger(HttpClientUtils.class);

    private static final String API_TOKEN = "";
    private static RestTemplate restTemplate = new RestTemplate();
    private static HttpClientUtils client = null;

    private HttpClientUtils() {
    }

    public static synchronized HttpClientUtils getInstance() {
        if (client == null) {
            client = new HttpClientUtils();
        }
        return client;
    }

    /**
     * @param url：请求地址
     * @param resType：    返回类型
     * @param headers：请求头
     * @return
     * @
     */
    public <T> T sendGet(String url, Class<T> resType, Map<String, String> headers) {

        HttpHeaders httpHeaders = new HttpHeaders();
        if (headers != null && headers.size() > 0) {
            for (String key : headers.keySet()) {
                httpHeaders.add(key, headers.get(key));
            }
        } else {
            httpHeaders.add("x-access-token", API_TOKEN);
        }
        HttpEntity<String> reqEntity = new HttpEntity(httpHeaders);
        ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.GET, reqEntity, resType);
        return response.getBody();
    }


    /**
     * @param url：请求地址
     * @param resType： 返回类型
     * @return
     * @
     */
    public <T> T sendGet(String url, Class<T> resType) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-access-token", API_TOKEN);
        HttpEntity<String> reqEntity = new HttpEntity(headers);
        ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.GET, reqEntity, resType);
        return response.getBody();
    }

    /**
     * @param url：请求地址
     * @param resType： 返回类型
     * @param object：  发送数据
     * @param headers： 请求头
     * @return
     * @
     */
    public <T> T sendPost(String url, Class<T> resType, Object object, Map<String, String> headers) {
        HttpHeaders httpHeaders = new HttpHeaders();
        if (headers != null && headers.size() > 0) {
            for (String key : headers.keySet()) {
                httpHeaders.add(key, headers.get(key));
            }
        } else {
            httpHeaders.add("x-access-token", API_TOKEN);
        }
        HttpEntity<String> reqEntity = new HttpEntity(object, httpHeaders);
        ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.POST, reqEntity, resType);
        return response.getBody();
    }

    /**
     * @param url：请求地址
     * @param resType： 返回类型
     * @param object：  发送数据
     * @return
     * @
     */
    public <T> T sendPost(String url, Class<T> resType, Object object) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-access-token", API_TOKEN);
        HttpEntity<String> reqEntity = new HttpEntity(object, headers);
        ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.POST, reqEntity, resType);
        return response.getBody();
    }

    /**
     * @param url：请求地址
     * @param resType：    返回类型
     * @param jsonObject： 发送数据
     * @return
     * @
     */
    public <T> T sendPost(String url, Class<T> resType, JSONObject jsonObject) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-access-token", API_TOKEN);
        HttpEntity<String> reqEntity = new HttpEntity(jsonObject, headers);
        ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.POST, reqEntity, resType);
        return response.getBody();
    }

}
