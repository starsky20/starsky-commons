package com.starsky.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author wangsh
 * @desc 百分比、小数等数字处理工具类
 */
public class NumberUtil {
    //保留两位小数
    private static final DecimalFormat format = new DecimalFormat("#0.00");

    /**
     * desc：格式化数字,保留2位小数
     *
     * @param
     * @return String
     */
    public static String format(double num) {
        String res = format.format(num);
        return res;
    }


    /**
     * desc：格式化数字,默认保留一位小数
     *
     * @param
     * @return String
     */
    public static String format(double num, String regex) {
        if (StringUtils.isEmpty(regex)) {
            regex = "#0.00";
        }
        DecimalFormat dformat = new DecimalFormat(regex);
        String res = dformat.format(num);
        return res;
    }

    /**
     * desc：格式化为百分比数字：当前数值乘以100，后缀为%,默认保留一位小数
     *
     * @param
     * @return String
     */
    public static String formatPercent(double num, String regex) {
        num = num * 100;
        String str = format(num, regex) + "%";
        return str;
    }

    /**
     * 四舍五入，保留两位小数
     *
     * @param data
     * @param scale
     * @return
     */
    public static BigDecimal round(float data, int scale) {
        //利用BigDecimal来实现四舍五入.保留一位小数
        //1代表保留1位小数,保留两位小数就是2,依此累推
        //BigDecimal.ROUND_HALF_UP 代表使用四舍五入的方式
        BigDecimal bigDecimal = new BigDecimal(data).setScale(scale, BigDecimal.ROUND_HALF_UP);
        return bigDecimal;
    }

    /**
     * 四舍五入，保留两位小数
     *
     * @param data
     * @return
     */
    public static float round(float data) {
        float result = round(data, 2).floatValue();
        return result;
    }

    /**
     * 四舍五入，保留两位小数
     *
     * @param data
     * @param scale
     * @return
     */
    public static BigDecimal round(double data, int scale) {
        //利用BigDecimal来实现四舍五入.保留一位小数
        //1代表保留1位小数,保留两位小数就是2,依此累推
        //BigDecimal.ROUND_HALF_UP 代表使用四舍五入的方式
        BigDecimal bigDecimal = new BigDecimal(data).setScale(scale, BigDecimal.ROUND_HALF_UP);
        return bigDecimal;
    }


    /**
     * 四舍五入，保留两位小数
     *
     * @param data
     * @return
     */
    public static double round(double data) {
        double result = round(data, 2).doubleValue();
        return result;
    }


    /**
     * 加法
     *
     * @param a
     * @param b
     * @return
     */
    public static float add(float a, float b) {
        BigDecimal bigDecimal = new BigDecimal(a+"");
        BigDecimal subtract = bigDecimal.add(new BigDecimal(b+""));
        return subtract.floatValue();
    }

    /**
     * 减法
     *
     * @param a
     * @param b
     * @return
     */
    public static float subtract(float a, float b) {
        BigDecimal bigDecimal = new BigDecimal(a+"");
        BigDecimal subtract = bigDecimal.subtract(new BigDecimal(b+""));
        return subtract.floatValue();
    }

    /**
     * 乘法
     *
     * @param a
     * @param b
     * @return
     */
    public static float multiply(float a, float b) {
        BigDecimal bigDecimal = new BigDecimal(a+"");
        BigDecimal subtract = bigDecimal.multiply(new BigDecimal(b+""));
        return subtract.floatValue();
    }

    /**
     * 除法
     *
     * @param a
     * @param b
     * @return
     */
    public static float divide(float a, float b) {
        BigDecimal bigDecimal = new BigDecimal(a+"");
        BigDecimal subtract = bigDecimal.divide(new BigDecimal(b+""));
        System.out.println(subtract.floatValue());
        return subtract.floatValue();
    }


    public static void main(String[] args) {
        System.out.println(format(0.023));
        System.out.println(format(10.023, "#0.0"));
        System.out.println(format(0.023, "#0.0"));
        System.out.println(formatPercent(0.023, "#0.0"));
    }
}
