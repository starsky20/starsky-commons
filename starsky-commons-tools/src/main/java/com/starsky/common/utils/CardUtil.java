package com.starsky.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author  wansh
 * @des 身份证信息算法类
 * @date  2020/9/19 22:22
 * @version 1.0
  * @email 1057718341@qq.com
 */
public class CardUtil {

	/** 中国公民身份证号码最小长度。 */
	public final static int CHINA_ID_MIN_LENGTH = 15;

	/** 中国公民身份证号码最大长度。 */
	public final static int CHINA_ID_MAX_LENGTH = 18;

	/**
	 * 校检身份证号
	 * @Description: TODO
	 * @param @param IDNumber
	 */
	public static boolean isIDNumber(String IDNumber) {
        if (IDNumber == null || "".equals(IDNumber)) {
            return false;
        }
        // 定义判别用户身份证号的正则表达式（15位或者18位，最后一位可以为字母）
        String regularExpression = "(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|" +
                "(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";
        //假设18位身份证号码:41000119910101123X  410001 19910101 123X
        //^开头
        //[1-9] 第一位1-9中的一个      4
        //\\d{5} 五位数字           10001（前六位省市县地区）
        //(18|19|20)                19（现阶段可能取值范围18xx-20xx年）
        //\\d{2}                    91（年份）
        //((0[1-9])|(10|11|12))     01（月份）
        //(([0-2][1-9])|10|20|30|31)01（日期）
        //\\d{3} 三位数字            123（第十七位奇数代表男，偶数代表女）
        //[0-9Xx] 0123456789Xx其中的一个 X（第十八位为校验值）
        //$结尾

        //假设15位身份证号码:410001910101123  410001 910101 123
        //^开头
        //[1-9] 第一位1-9中的一个      4
        //\\d{5} 五位数字           10001（前六位省市县地区）
        //\\d{2}                    91（年份）
        //((0[1-9])|(10|11|12))     01（月份）
        //(([0-2][1-9])|10|20|30|31)01（日期）
        //\\d{3} 三位数字            123（第十五位奇数代表男，偶数代表女），15位身份证不含X
        //$结尾
        boolean matches = IDNumber.matches(regularExpression);
        //判断第18位校验值
        if (matches) {

            if (IDNumber.length() == 18) {
                try {
                    char[] charArray = IDNumber.toCharArray();
                    //前十七位加权因子
                    int[] idCardWi = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
                    //这是除以11后，可能产生的11位余数对应的验证码
                    String[] idCardY = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};
                    int sum = 0;
                    for (int i = 0; i < idCardWi.length; i++) {
                        int current = Integer.parseInt(String.valueOf(charArray[i]));
                        int count = current * idCardWi[i];
                        sum += count;
                    }
                    char idCardLast = charArray[17];
                    int idCardMod = sum % 11;
                    if (idCardY[idCardMod].toUpperCase().equals(String.valueOf(idCardLast).toUpperCase())) {
                        return true;
                    } else {
                        System.out.println("身份证最后一位:" + String.valueOf(idCardLast).toUpperCase() + 
                                "错误,正确的应该是:" + idCardY[idCardMod].toUpperCase());
                        return false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("异常:" + IDNumber);
                    return false;
                }
            }

        }
        return matches;
    }
	
	/**
	 * 根据身份编号获取生日年
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(yyyy)
	 */
	public static String getYearByCardCode(String idCard) {

		if (StringUtils.isEmpty(idCard)) {
			return "";
		}
		String year = "";
		if (idCard.length() == CHINA_ID_MIN_LENGTH) {
			// 15位
			year = "19" + idCard.substring(6, 8);
		} else {
			// 18位
			year = idCard.substring(6, 10);
		}
		return year;
	}

	/**
	 * 根据身份编号获取生日月
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(MM)
	 */
	public static String getMonthByCardCode(String idCard) {

		if (StringUtils.isEmpty(idCard)) {
			return "";
		}
		String month = "";
		if (idCard.length() == CHINA_ID_MIN_LENGTH) {
			// 15位
			month = idCard.substring(8, 10);
		} else {
			// 18位
			month = idCard.substring(10, 12);
		}
		return month;
	}

	/**
	 * 根据身份编号获取生日天
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(dd)
	 */
	public static String getDateByCardCode(String idCard) {

		if (StringUtils.isEmpty(idCard)) {
			return "";
		}
		String day = "";
		if (idCard.length() == CHINA_ID_MIN_LENGTH) {
			// 15位
			day = idCard.substring(10, 12);
		} else {
			// 18位
			day = idCard.substring(12, 14);
		}
		return day;
	}

	/**
	 * 根据身份编号获取生日
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(yyyyMMdd)
	 */
	public static String getBirthDayByCardCode(String idCard) {
		if (StringUtils.isEmpty(idCard)) {
			return "";
		}
		String birthDay = "";
		if (idCard.length() == CHINA_ID_MIN_LENGTH) {
			// 15位
			birthDay = "19" + idCard.substring(6, 12);// 日
		} else {
			// 18位
			birthDay = idCard.substring(6, 14);
		}
		return birthDay;
	}

	/**
	 * 根据身份编号获取年龄
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 年龄
	 */
	public static int getAgeByCardCode(String idCard) {
		if (StringUtils.isEmpty(idCard)) {
			return 0;
		}
		String year = "";
		if (idCard.length() == CHINA_ID_MIN_LENGTH) {
			// 15位
			year = "19" + idCard.substring(6, 8);
		} else {
			// 18位
			year = idCard.substring(6, 10);
		}
		int iCurrYear = DateUtils.getYear(new Date());
		int iAge = iCurrYear - Integer.valueOf(year);
		return iAge;
	}

	/**
	 * 根据身份编号获取性别
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 性别(M-男，F-女，N-未知)
	 */
	public static String getGenderByCardCode(String idCard) {

		if (StringUtils.isEmpty(idCard)) {
			return "";
		}
		String gender = "3";//3-未知
		String sCardNum = "";
		if (idCard.length() == CHINA_ID_MIN_LENGTH) {
			// 15位
			sCardNum = idCard.substring(14, 15);// 用户的性别
		} else {
			// 18位
			sCardNum = idCard.substring(16, 17);
		}
		if (Integer.parseInt(sCardNum) % 2 != 0) {
			gender = "1";// 1-男
		} else {
			gender = "2";// 2-女
		}
		return gender;
	}

	/**
	 * 根据身份证的号码算出当前身份证持有者的性别和年龄
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Object> getCardCodeInfo(String cardCode) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		if (StringUtils.isEmpty(cardCode)) {
			return map;
		}

		String year = getYearByCardCode(cardCode);// 年
		String month = getMonthByCardCode(cardCode);// 月
		String day = getDateByCardCode(cardCode);// 日
		String birthDay = getBirthDayByCardCode(cardCode);// 生日
		String sex = getGenderByCardCode(cardCode);// 性别
		int age = getAgeByCardCode(cardCode);// 年龄

		map.put("year", year);
		map.put("month", month);
		map.put("day", day);
		map.put("birthDay", birthDay);
		map.put("sex", sex);
		map.put("age", age);

		return map;
	}

	public static void main(String[] a) {

		String idcard = "22088119760331256X";
		// String idcard = "460200920927111";

		String sex = getGenderByCardCode(idcard);
		System.out.println("性别:" + sex);

		int age = getAgeByCardCode(idcard);
		System.out.println("年龄:" + age);

		String nian = getYearByCardCode(idcard);
		String yue = getMonthByCardCode(idcard);
		String ri = getDateByCardCode(idcard);
		System.out.println(nian + "年" + yue + "月" + ri + "日");

		String sr = getBirthDayByCardCode(idcard);
		System.out.println("生日:" + sr);
		System.out.println("身份证号判断:" + isIDNumber(idcard));
	}

}
