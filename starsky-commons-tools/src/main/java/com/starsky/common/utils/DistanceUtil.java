package com.starsky.common.utils;

/**
 *@desc: 根据经纬度，计算距离
 *@author: wangsh
 *
 */
public class DistanceUtil {
	private static double EARTH_RADIUS = 6378.137;

	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	/**
	 * TODO 根据经纬度，计算距离
	 */
	public static double getDistance(double lng1, double lat1, double lng2,
			double lat2) {
		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);
		double a = radLat1 - radLat2;
		double b = rad(lng1) - rad(lng2);
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radLat1) * Math.cos(radLat2)
				* Math.pow(Math.sin(b / 2), 2)));
		s = s * EARTH_RADIUS;
		s = Math.round(s * 1000);
		return s;
	}

	/**
	 * 生成以中心点为中心的四方形经纬度
	 * 
	 * @param lat
	 * @param lon
	 * @param raidus
	 *            (以米为单位)
	 * @return
	 */
	public static double[] getAround(double lat, double lon, double raidus) {

		Double latitude = lat;
		Double longitude = lon;
		double raidusMile = raidus;

		Double degree = (24901 * 1609) / 360.0;

		Double dpmLat = 1 / degree;
		Double radiusLat = dpmLat * raidusMile;
		Double minLat = latitude - radiusLat;
		Double maxLat = latitude + radiusLat;

		Double mpdLng = degree * Math.cos(latitude * (Math.PI / 180));
		Double dpmLng = 1 / mpdLng;
		Double radiusLng = dpmLng * raidusMile;
		Double minLng = longitude - radiusLng;
		Double maxLng = longitude + radiusLng;
		double[] d = new double[] { minLat, minLng, maxLat, maxLng };
		return d;

	}

	public static void main(String[] args) {

		// double e =0.0;
		// double e2=0.0;
		// double e3 =0.0;
		// double e4 =0.0;
		// double[] d=getAround(34.34, 108.9, 500);
		// for (int i = 0; i < d.length; i++) {
		// e= d[0];
		// e2= d[1];
		// e3= d[2];
		// e4= d[3];
		// }
		// System.out.println(e);
		// System.out.println(e2);
		// System.out.println(e3);
		// System.out.println(e4);
		double d2 = getDistance(108.93340619444555, 34.34206295435595,
				108.951416015625, 34.27083595165);
		System.out.println(d2);
	}
}
