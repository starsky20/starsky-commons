package com.starsky.common.utils.word;

import java.util.List;

/**
 * TODO word表头和标题
 */
public class TableDesc {

    private String tableTitle;

    /**
     * 列标头显示值与过滤值关系集合
     */
    private List<FiledDescription> filedDescriptions;

    public String getTableTitle() {
        return tableTitle;
    }

    public void setTableTitle(String tableTitle) {
        this.tableTitle = tableTitle;
    }

    public List<FiledDescription> getFiledDescriptions() {
        return filedDescriptions;
    }

    public void setFiledDescriptions(List<FiledDescription> filedDescriptions) {
        this.filedDescriptions = filedDescriptions;
    }

}
