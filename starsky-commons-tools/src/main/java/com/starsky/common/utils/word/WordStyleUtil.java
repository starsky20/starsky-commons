package com.starsky.common.utils.word;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.*;

import java.math.BigInteger;

/**
 * @author  wansh
 * @des word的一些样式(文本、表格)
 * @date  2020/9/19 22:20
 * @version 1.0
  * @email 1057718341@qq.com
 */
public class WordStyleUtil {
	
	/**
	 * 增加自定义标题样式
	 * 
	 * @param docxDocument
	 *            目标文档
	 * @param titleLevel
	 *            样式级别名称
	 */
	public static void addCustomHeadingStyle(XWPFDocument docxDocument,
			TitleLevel titleLevel) {
		CTStyle ctStyle = CTStyle.Factory.newInstance();
		ctStyle.setStyleId(titleLevel.toString());
		// 样式的名称
		CTString styleName = CTString.Factory.newInstance();
		styleName.setVal(titleLevel.toString());
		ctStyle.setName(styleName);

		// 设置样式级别
		CTDecimalNumber indentNumber = CTDecimalNumber.Factory.newInstance();
		indentNumber.setVal(BigInteger.valueOf(titleLevel.getValue()));
		// 在格式栏排序靠前
		ctStyle.setUiPriority(indentNumber);
		CTOnOff onoffnull = CTOnOff.Factory.newInstance();
		ctStyle.setUnhideWhenUsed(onoffnull);
		// 将样式显示在格式栏
		ctStyle.setQFormat(onoffnull);

		// 定义给定的级别
		CTPPr ppr = CTPPr.Factory.newInstance();
		ppr.setOutlineLvl(indentNumber);
		ctStyle.setPPr(ppr);

		// 创建自定义的样式
		XWPFStyle style = new XWPFStyle(ctStyle);
		XWPFStyles styles = docxDocument.createStyles();
		style.setType(STStyleType.PARAGRAPH);
		styles.addStyle(style);
	}

	/**
	 * TODO 设置段落间距信息,一行=100 一磅=20
	 * @param isSpace 是否设置段间距
	 * 
	 * @param before 段前磅数
	 * 
	 * @param after 段后磅数
	 * 
	 * @param beforeLines 段前行数
	 * 
	 * @param afterLines 段后行数
	 * 
	 * @param after 设置
	 * 
	 * @param line 行距
	 * 
	 * @param lineValue 单位
	 */
	public static void setParagraphSpacingInfo(XWPFParagraph paragraph, boolean isSpace,
			Integer before, Integer after, Integer beforeLines, Integer afterLines,
			boolean isLine, Integer line, STLineSpacingRule.Enum lineValue) {
		CTPPr pPPr = WordFactory.getParagraphCTPPr(paragraph);
		CTSpacing pSpacing = pPPr.getSpacing() != null ? pPPr.getSpacing()
				: pPPr.addNewSpacing();
		if (isSpace) {
			// 段前磅数
			if (before != null) {
				pSpacing.setBefore(BigInteger.valueOf(before));
			}
			// 段后磅数
			if (after != null) {
				pSpacing.setAfter(BigInteger.valueOf(after));
			}
			// 段前行数
			if (beforeLines != null) {
				pSpacing.setBeforeLines(BigInteger.valueOf(beforeLines));
			}
			// 段后行数
			if (afterLines != null) {
				pSpacing.setAfterLines(BigInteger.valueOf(afterLines));
			}
		}
		// 间距
		if (isLine) {
			if (line != null) {
				pSpacing.setLine(BigInteger.valueOf(line));
			}
			if (lineValue != null) {
				pSpacing.setLineRule(lineValue);
			}
		}
	}

	/**
	 * TODO 设置段落文本样式(高亮与底纹显示效果不同)设置字符间距信息(CTSignedTwipsMeasure)
	 *
	 * @param cnFontFamily  中文字体
	 * 
	 * @param enFontFamily	西文字体
	 * 
	 * @param fontSize 自号
	 * 
	 * @param position
	 *            字符间距位置	磅值*2 如3磅=6(区分正负,正提升,负降低)
	 * @param spacingValue
	 *            字符间距间距	磅值*20 如2磅=40(区分正负,正加宽,负紧缩)
	 * @param indent
	 *            字符间距缩进 <100 缩
	 * @param isBlod 是否加粗
	 * 
	 * @param isItalic 是否倾斜
	 * 
	 * @param isStrike 是否有删除线
	 * 
	 * @param isShd 是否设置底纹
	 * 
	 * @param shdStyle 底纹样式
	 * 
	 * @param shdColor 底纹颜色
	 */
	public static void setParagraphRunFontInfo(XWPFParagraph paragraph, XWPFRun pRun,
			String cnFontFamily, String enFontFamily,
			Integer fontSize, boolean isBlod, boolean isItalic,
			boolean isStrike, boolean isShd, String shdColor,
			STShd.Enum shdStyle, int position, Integer spacingValue, int indent) {
		CTRPr pRpr = WordFactory.getRunCTRPr(paragraph, pRun);
		// 设置字体
		CTFonts fonts = pRpr.isSetRFonts() ? pRpr.getRFonts() : pRpr
				.addNewRFonts();
		if (StringUtils.isNotBlank(enFontFamily)) {
			fonts.setAscii(enFontFamily);
			fonts.setHAnsi(enFontFamily);
		}
		if (StringUtils.isNotBlank(cnFontFamily)) {
			fonts.setEastAsia(cnFontFamily);
//			fonts.setHint(STHint.EAST_ASIA);
		}
		// 设置字体大小
		CTHpsMeasure sz = pRpr.isSetSz() ? pRpr.getSz() : pRpr.addNewSz();
		sz.setVal(BigInteger.valueOf(fontSize));

		CTHpsMeasure szCs = pRpr.isSetSzCs() ? pRpr.getSzCs() : pRpr
				.addNewSzCs();
		szCs.setVal(BigInteger.valueOf(fontSize));

		// 设置字体样式
		// 加粗
		if (isBlod) {
			pRun.setBold(isBlod);
		}
		// 倾斜
		if (isItalic) {
			pRun.setItalic(isItalic);
		}
		// 删除线
		if (isStrike) {
			pRun.setStrike(isStrike);
		}
		if (isShd) {
			// 设置底纹
			CTShd shd = pRpr.isSetShd() ? pRpr.getShd() : pRpr.addNewShd();
			if (shdStyle != null) {
				shd.setVal(shdStyle);
			}
			if (shdColor != null) {
				shd.setColor(shdColor);
				shd.setFill(shdColor);
			}
		}

		// 设置文本位置
		if (position != 0) {
			pRun.setTextPosition(position);
		}
		if (spacingValue > 0) {
			// 设置字符间距信息
			CTSignedTwipsMeasure ctSTwipsMeasure = pRpr.isSetSpacing() ? pRpr
					.getSpacing() : pRpr.addNewSpacing();
			ctSTwipsMeasure
					.setVal(BigInteger.valueOf(spacingValue));
		}
		if (indent > 0) {
			CTTextScale paramCTTextScale = pRpr.isSetW() ? pRpr.getW() : pRpr
					.addNewW();
			paramCTTextScale.setVal(indent);
		}
	}
	
	/**
	 * TODO 段落缩进信息
	 * @param firstLine  首行缩进   缩进磅值
	 * @param firstLineChar	首行缩进  缩进字符
	 * @param hanging	悬挂缩进  缩进磅值
	 * @param hangingChar	悬挂缩进,缩进字符
	 * @param right		缩进右侧,磅值
	 * @param rigthChar		缩进右侧,字符
	 * @param left	缩进左侧,磅值
	 * @param leftChar	缩进左侧,字符
	 */
	public static void setParagraphIndInfo(XWPFParagraph paragraph, Integer firstLine,
			Integer firstLineChar, Integer hanging, Integer hangingChar,
			Integer right, Integer rigthChar, Integer left, Integer leftChar) {
		CTPPr pPPr = WordFactory.getParagraphCTPPr(paragraph);
		CTInd pInd = pPPr.getInd() != null ? pPPr.getInd() : pPPr.addNewInd();
		
		if (firstLine != null) {
			pInd.setFirstLine(BigInteger.valueOf(firstLine));
		}
		if (firstLineChar != null) {
			pInd.setFirstLineChars(BigInteger.valueOf(firstLineChar));
		}
		
		if (hanging != null) {
			pInd.setHanging(BigInteger.valueOf(hanging));
		}
		if (hangingChar != null) {
			pInd.setHangingChars(BigInteger.valueOf(hangingChar));
		}
		
		if (left != null) {
			pInd.setLeft(BigInteger.valueOf(left));
		}
		if (leftChar != null) {
			pInd.setLeftChars(BigInteger.valueOf(leftChar));
		}
		
		if (right != null) {
			pInd.setRight(BigInteger.valueOf(right));
		}
		if (rigthChar != null) {
			pInd.setRightChars(BigInteger.valueOf(rigthChar));
		}
	}
	
	/**
	 * TODO 设置段落文本样式(高亮与底纹显示效果不同)设置字符间距信息(CTSignedTwipsMeasure)
	 *
	 * @param cnFontFamily  中文字体
	 * 
	 * @param enFontFamily	西文字体
	 * 
	 * @param fontSize 自号
	 * 
	 * @param position
	 *            字符间距位置	磅值*2 如3磅=6(区分正负,正提升,负降低)
	 * @param spacingValue
	 *            字符间距间距	磅值*20 如2磅=40(区分正负,正加宽,负紧缩)
	 * @param indent
	 *            字符间距缩进 <100 缩
	 * @param isBlod 是否加粗
	 * 
	 * @param isItalic 是否倾斜
	 * 
	 * @param isStrike 是否有删除线
	 * 
	 * @param isShd 是否设置底纹
	 * 
	 * @param shdStyle 底纹样式
	 * 
	 * @param shdColor 底纹颜色
	 * 
	 * @param verticalAlign 上下标
	 * 
	 */
	public static void setParagraphRunFontInfo(XWPFParagraph paragraph, XWPFRun pRun,
			String cnFontFamily, String enFontFamily,
			Integer fontSize, boolean isBlod, boolean isItalic,
			boolean isStrike, boolean isShd, String shdColor,
			STShd.Enum shdStyle, int position, Integer spacingValue, int indent, VerticalAlign verticalAlign) {
		CTRPr pRpr = WordFactory.getRunCTRPr(paragraph, pRun);
		// 设置字体
		CTFonts fonts = pRpr.isSetRFonts() ? pRpr.getRFonts() : pRpr
				.addNewRFonts();
		if (StringUtils.isNotBlank(enFontFamily)) {
			fonts.setAscii(enFontFamily);
			fonts.setHAnsi(enFontFamily);
		}
		if (StringUtils.isNotBlank(cnFontFamily)) {
			fonts.setEastAsia(cnFontFamily);
//			fonts.setHint(STHint.EAST_ASIA);
		}
		// 设置字体大小
		CTHpsMeasure sz = pRpr.isSetSz() ? pRpr.getSz() : pRpr.addNewSz();
		sz.setVal(BigInteger.valueOf(fontSize));

		CTHpsMeasure szCs = pRpr.isSetSzCs() ? pRpr.getSzCs() : pRpr
				.addNewSzCs();
		szCs.setVal(BigInteger.valueOf(fontSize));

		// 设置字体样式
		// 加粗
		if (isBlod) {
			pRun.setBold(isBlod);
		}
		// 倾斜
		if (isItalic) {
			pRun.setItalic(isItalic);
		}
		// 删除线
		if (isStrike) {
			pRun.setStrike(isStrike);
		}
		if (isShd) {
			// 设置底纹
			CTShd shd = pRpr.isSetShd() ? pRpr.getShd() : pRpr.addNewShd();
			if (shdStyle != null) {
				shd.setVal(shdStyle);
			}
			if (shdColor != null) {
				shd.setColor(shdColor);
				shd.setFill(shdColor);
			}
		}

		// 设置文本位置
		if (position != 0) {
			pRun.setTextPosition(position);
		}
		if (spacingValue > 0) {
			// 设置字符间距信息
			CTSignedTwipsMeasure ctSTwipsMeasure = pRpr.isSetSpacing() ? pRpr
					.getSpacing() : pRpr.addNewSpacing();
			ctSTwipsMeasure
					.setVal(BigInteger.valueOf(spacingValue));
		}
		if (indent > 0) {
			CTTextScale paramCTTextScale = pRpr.isSetW() ? pRpr.getW() : pRpr
					.addNewW();
			paramCTTextScale.setVal(indent);
		}
		if (verticalAlign != null) {
			pRun.setSubscript(verticalAlign);
		}
	}
	
	/**
	 * TODO 设置段落文本样式(高亮与底纹显示效果不同)设置字符间距信息(CTSignedTwipsMeasure)
	 *
	 * @param fontFamily  字体
	 * 
	 * @param fontSize 自号
	 * 
	 * @param position
	 *            字符间距位置	磅值*2 如3磅=6(区分正负,正提升,负降低)
	 * @param spacingValue
	 *            字符间距间距	磅值*20 如2磅=40(区分正负,正加宽,负紧缩)
	 * @param isBlod 是否加粗
	 * 
	 * @param isItalic 是否倾斜
	 * 
	 * @param isStrike 是否有删除线
	 * 
	 */
	public static void appendExternalHyperlink(String url, String content,
			XWPFParagraph paragraph, String fontFamily, Integer fontSize,
			boolean isBlod, boolean isItalic, boolean isStrike,
			STVerticalAlignRun.Enum verticalAlign, Integer position,String spacingValue) {
		// Add the link as External relationship
		String id = paragraph
				.getDocument()
				.getPackagePart()
				.addExternalRelationship(url,
						XWPFRelation.HYPERLINK.getRelation()).getId();
		// Append the link and bind it to the relationship
		CTHyperlink cLink = paragraph.getCTP().addNewHyperlink();
		cLink.setId(id);

		// Create the linked text
		CTText ctText = CTText.Factory.newInstance();
		ctText.setStringValue(content);
		CTR ctr = CTR.Factory.newInstance();
		CTRPr rpr = ctr.addNewRPr();

		// 设置超链接样式
		CTColor color = CTColor.Factory.newInstance();
		color.setVal("0000FF");
		rpr.setColor(color);
		rpr.addNewU().setVal(STUnderline.SINGLE);
		if (isBlod) {
			rpr.addNewB().setVal(STOnOff.Enum.forString("true"));
		}
		if (isItalic) {
			rpr.addNewI().setVal(STOnOff.Enum.forString("true"));
		}
		if (isStrike) {
			rpr.addNewStrike().setVal(STOnOff.Enum.forString("true"));
		}
		if (verticalAlign != null) {
			rpr.addNewVertAlign().setVal(verticalAlign);
		}
		rpr.addNewPosition().setVal(BigInteger.valueOf(position));

		// 设置字体
		CTFonts fonts = rpr.isSetRFonts() ? rpr.getRFonts() : rpr
				.addNewRFonts();
		fonts.setAscii(fontFamily);
		fonts.setEastAsia(fontFamily);
		fonts.setHAnsi(fontFamily);

		// 设置字体大小
		CTHpsMeasure sz = rpr.isSetSz() ? rpr.getSz() : rpr.addNewSz();
		sz.setVal(BigInteger.valueOf(fontSize));

		CTHpsMeasure szCs = rpr.isSetSzCs() ? rpr.getSzCs() : rpr.addNewSzCs();
		szCs.setVal(BigInteger.valueOf(fontSize));

		if(spacingValue!=null){
			//设置字符间距信息
			CTSignedTwipsMeasure ctSTwipsMeasure=rpr.isSetSpacing()?rpr.getSpacing(): rpr.addNewSpacing();
			ctSTwipsMeasure.setVal(new BigInteger(spacingValue));
		}
		
		ctr.setTArray(new CTText[] { ctText });
		cLink.setRArray(new CTR[] { ctr });
	}

	/**
	 * TODO 设置段落对齐
	 *
	 * @param pAlign 段落对其方式
	 * 
	 * @param valign 段落中文本对其方式
	 */
	public static void setParagraphAlignInfo(XWPFParagraph paragraph,
			ParagraphAlignment pAlign, TextAlignment valign) {
		if (pAlign != null) {
			paragraph.setAlignment(pAlign);
		}
		if (valign != null) {
			paragraph.setVerticalAlignment(valign);
		}
	}

	/**
	 * TODO 设置Table的边框
	 *
	 * @param borderType 边框类型
	 * 
	 * @param size 边框宽度
	 * 
	 * @param color 边框颜色rgb格式
	 * 
	 * @param space 内外边框间距
	 * 
	 */
	public static void setTableBorders(XWPFTable table, STBorder.Enum borderType,
			Integer size, String color, Integer space) {
		CTTblPr tblPr = WordFactory.getTableCTTblPr(table);
		//整体边框
		CTTblBorders borders = tblPr.isSetTblBorders() ? tblPr.getTblBorders()
				: tblPr.addNewTblBorders();
		//内部横线
		CTBorder hBorder = borders.isSetInsideH() ? borders.getInsideH()
				: borders.addNewInsideH();
		hBorder.setVal(borderType);
		hBorder.setSz(BigInteger.valueOf(size));
		hBorder.setColor(color);
		hBorder.setSpace(BigInteger.valueOf(space));
		//内部竖线
		CTBorder vBorder = borders.isSetInsideV() ? borders.getInsideV()
				: borders.addNewInsideV();
		vBorder.setVal(borderType);
		vBorder.setSz(BigInteger.valueOf(size));
		vBorder.setColor(color);
		vBorder.setSpace(BigInteger.valueOf(space));
		//左框线
		CTBorder lBorder = borders.isSetLeft() ? borders.getLeft() : borders
				.addNewLeft();
		lBorder.setVal(borderType);
		lBorder.setSz(BigInteger.valueOf(size));
		lBorder.setColor(color);
		lBorder.setSpace(BigInteger.valueOf(space));
		//右框线
		CTBorder rBorder = borders.isSetRight() ? borders.getRight() : borders
				.addNewRight();
		rBorder.setVal(borderType);
		rBorder.setSz(BigInteger.valueOf(size));
		rBorder.setColor(color);
		rBorder.setSpace(BigInteger.valueOf(space));
		//上框线
		CTBorder tBorder = borders.isSetTop() ? borders.getTop() : borders
				.addNewTop();
		tBorder.setVal(borderType);
		tBorder.setSz(BigInteger.valueOf(size));
		tBorder.setColor(color);
		tBorder.setSpace(BigInteger.valueOf(space));
		//下框线
		CTBorder bBorder = borders.isSetBottom() ? borders.getBottom()
				: borders.addNewBottom();
		bBorder.setVal(borderType);
		bBorder.setSz(BigInteger.valueOf(size));
		bBorder.setColor(color);
		bBorder.setSpace(BigInteger.valueOf(space));
	}

	/**
	 * TODO 设置列宽和垂直对齐方式
	 * @param width 列宽
	 * 
	 * @param typeEnum 单元格宽度类型
	 * 
	 * @param vAlign 单元格对其方式
	 * 
	 */
	public static void setCellWidthAndVAlign(XWPFTableCell cell, Integer width,
			STTblWidth.Enum typeEnum, STVerticalJc.Enum vAlign) {
		CTTcPr tcPr = WordFactory.getCellCTTcPr(cell);
		CTTblWidth tcw = tcPr.isSetTcW() ? tcPr.getTcW() : tcPr.addNewTcW();
		if (width != null) {
			tcw.setW(BigInteger.valueOf(width));
		}
		if (typeEnum != null) {
			tcw.setType(typeEnum);
		}
		if (vAlign != null) {
			CTVerticalJc vJc = tcPr.isSetVAlign() ? tcPr.getVAlign() : tcPr
					.addNewVAlign();
			vJc.setVal(vAlign);
		}
	}

	/**
	 * TODO 跨列合并
	 *
	 * @param row 起始行
	 * 
	 * @param fromCell 起始列
	 */
	public static void mergeCellsHorizontal(XWPFTable table, int row, int fromCell,
			int toCell) {
		for (int cellIndex = fromCell; cellIndex <= toCell; cellIndex++) {
			XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
			if (cellIndex == fromCell) {
				// The first merged cell is set with RESTART merge value
				WordFactory.getCellCTTcPr(cell).addNewHMerge().setVal(STMerge.RESTART);
			} else {
				// Cells which join (merge) the first one,are set with CONTINUE
				WordFactory.getCellCTTcPr(cell).addNewHMerge().setVal(STMerge.CONTINUE);
			}
		}
	}
	
	/**
	 * TODO 设置表格列宽
	 *
	 */
	public static void setTableGridCol(XWPFTable table, int[] colWidths) {
		CTTbl ttbl = table.getCTTbl();
		CTTblGrid tblGrid = ttbl.getTblGrid() != null ? ttbl.getTblGrid()
				: ttbl.addNewTblGrid();
		for (int j = 0, len = colWidths.length; j < len; j++) {
			CTTblGridCol gridCol = tblGrid.addNewGridCol();
			gridCol.setW(new BigInteger(String.valueOf(colWidths[j])));
		}
	}

	/**
	 * TODO 设置表格总宽度与水平对齐方式
	 *
	 */
	public static void setTableWidthAndHAlign(XWPFTable table, Integer width,
			STJc.Enum enumValue) {
		CTTblPr tblPr = WordFactory.getTableCTTblPr(table);
		CTTblWidth tblWidth = tblPr.isSetTblW() ? tblPr.getTblW() : tblPr
				.addNewTblW();
		if (enumValue != null) {
			CTJc cTJc = tblPr.addNewJc();
			cTJc.setVal(enumValue);
		}
		tblWidth.setW(BigInteger.valueOf(width));
		tblWidth.setType(STTblWidth.DXA);
	}

	/**
	 * TODO 设置单元格Margin
	 *
	 */
	public static void setTableCellMargin(XWPFTable table, int top, int left,
			int bottom, int right) {
		table.setCellMargins(top, left, bottom, right);
	}
	
	/**
	 * TODO 设置行高
	 *
	 * @param hight 行高
	 * 
	 * @param heigthEnum 行高值类型
	 */
	public static void setRowHeight(XWPFTableRow row, Integer hight,
			STHeightRule.Enum heigthEnum) {
		CTTrPr trPr = WordFactory.getRowCTTrPr(row);
		CTHeight trHeight;
		if (trPr.getTrHeightList() != null && trPr.getTrHeightList().size() > 0) {
			trHeight = trPr.getTrHeightList().get(0);
		} else {
			trHeight = trPr.addNewTrHeight();
		}
		trHeight.setVal(BigInteger.valueOf(hight));
		if (heigthEnum != null) {
			trHeight.setHRule(heigthEnum);
		}
	}

	/**
	 * TODO 设置底纹
	 * 
	 * @param isShd 是否设置底纹
	 * 
	 * @param shdColor 底纹颜色
	 * 
	 * @param shdStyle 底纹样式
	 */
	public static void setCellShdStyle(XWPFTableCell cell, boolean isShd,
			String shdColor, STShd.Enum shdStyle) {
		CTTcPr tcPr = WordFactory.getCellCTTcPr(cell);
		if (isShd) {
			// 设置底纹
			CTShd shd = tcPr.isSetShd() ? tcPr.getShd() : tcPr.addNewShd();
			if (shdStyle != null) {
				shd.setVal(shdStyle);
			}
			if (shdColor != null) {
				shd.setColor(shdColor);
				shd.setFill(shdColor);
			}
		}
	}
	
}
