package com.starsky.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.JSONLibDataFormatSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import org.dom4j.*;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.util.List;
import java.util.Map;

/**
 * @desc: fastjson工具类
 * @author: wangsh
 */
public class FastJsonUtils {

    private static final SerializeConfig config = new SerializeConfig();

    private static SimpleDateFormatSerializer formatSerializer = new SimpleDateFormatSerializer("yyyy-MM-dd HH:mm:ss");

    static {
//        config.put(java.util.Date.class, formatSerializer);
        // 使用和json-lib兼容的日期输出格式
        config.put(java.util.Date.class, new JSONLibDataFormatSerializer());
        // 使用和json-lib兼容的日期输出格式
        config.put(java.sql.Date.class, new JSONLibDataFormatSerializer());
    }

    private static final SerializerFeature[] features = {
            // 输出空置字段
            SerializerFeature.WriteMapNullValue,
            // list字段如果为null，输出为[]，而不是null
            SerializerFeature.WriteNullListAsEmpty,
            // 数值字段如果为null，输出为0，而不是null
            SerializerFeature.WriteNullNumberAsZero,
            // Boolean字段如果为null，输出为false，而不是null
            SerializerFeature.WriteNullBooleanAsFalse,
            // 字符类型字段如果为null，输出为""，而不是null
            SerializerFeature.WriteNullStringAsEmpty
    };

    /**
     * POJO 转 JSON
     *
     * @param <T>
     * @return
     */
    public static <T> String toJson(T t) {
        String pojo = JSONObject.toJSONString(t);
        return pojo;
    }

    /**
     * 对象转为json字符串
     *
     * @param t
     * @return
     */
    public static <T> String toJSONString(T t) {
        return JSONObject.toJSONString(t, config, features);
    }

    /**
     * 对象转为json字符串
     *
     * @param object
     * @return
     */
    public static <T> String toJSONNoFeatures(T object) {
        return JSONObject.toJSONString(object, config);
    }

    /**
     * 将json字符串转为JsonObject对象
     *
     * @param json
     * @return
     */
    public static JSONObject jsonToObject(String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        return jsonObject;
    }

    /**
     * 转为对象
     *
     * @param json
     * @return
     */
    public static Object jsonToBean(String json) {
        return JSONObject.parse(json);
    }

    /**
     * 转为对象
     *
     * @param jsonObj
     * @return
     */
    public static <T> T jsonToBeanObject(String jsonObj) {
        return JSONObject.parseObject(jsonObj, new TypeReference<T>() {
        });
    }

    /**
     * 转为对象
     *
     * @return
     */
    public static <T> T toJavaObject(JSONObject jsonObject, Class<T> clazz) {
        return JSONObject.toJavaObject(jsonObject, clazz);
    }

    /**
     * json转为josn对象
     *
     * @param text
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T jsonToBean(String text, Class<T> clazz) {
        return JSONObject.parseObject(text, clazz);
    }

    /**
     * 反序列化为泛型类
     */
    public static <T> T jsonToBean(String text, TypeReference<T> reference) {
        return JSONObject.parseObject(text, reference);
    }

    /**
     * 转换为数组
     */
    public static <T> Object[] josnToArray(String text) {
        return josnToArray(text, null);
    }

    /**
     * 转换为数组
     *
     * @param text
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> Object[] josnToArray(String text, Class<T> clazz) {
        return JSONArray.parseArray(text, clazz).toArray();
    }

    /**
     * 转换为数组
     *
     * @param jsonArray
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> parseArray(String jsonArray, Class<T> clazz) {
        return JSONArray.parseArray(jsonArray, clazz);
    }

    /**
     * 转换为List
     *
     * @param text
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> jsonToList(String text, Class<T> clazz) {
        return JSONArray.parseArray(text, clazz);
    }


    /**
     *List<T> 转 json 保存到数据库
     *      
     */
    public static <T> String listToJson(List<T> ts) {
        String jsons = JSONArray.toJSONString(ts);
        return jsons;
    }


    /**
     * 将jsonobject对象转为Map
     *      
     */
    public static <T> T jsonToMap(JSONObject object, Class<T> clazz) {
        return JSONObject.toJavaObject(object, clazz);
    }

    /**
     * 将jsonobject对象转为Map
     *      
     */
    public static <T> T jsonToMap(String json, Class<T> clazz) {
        JSONObject object = JSONObject.parseObject(json);
        return JSONObject.toJavaObject(object, clazz);
    }

    /**
     * 功能描述：把JSON数据转换成较为复杂的List<Map<String, Object>>
     *
     * @param json JSON数据
     */
    public static List<Map<String, Object>> jsonToListMap(String json) {
        return JSONObject.parseObject(json, new TypeReference<List<Map<String, Object>>>() {
        });
    }


    /**
     * 将javabean转化为序列化的json字符串
     *
     * @param keyvalue
     * @return
     */
    public static Object beanToJson(KeyValue keyvalue) {
        String textJson = JSONObject.toJSONString(keyvalue);
        Object objectJson = JSONObject.parse(textJson);
        return objectJson;
    }

    /**
     * 将string转化为序列化的json字符串
     *
     * @param text
     * @return
     */
    public static Object textToJson(String text) {
        Object objectJson = JSONObject.parse(text);
        return objectJson;
    }

    /**
     * json字符串转化为map
     *
     * @param json
     * @return
     */
    public static Map stringToMap(String json) {
        Map m = JSONObject.parseObject(json);
        return m;
    }

    /**
     * 将map转化为string
     *
     * @param m
     * @return
     */
    public static String mapToString(Map m) {
        String s = JSONObject.toJSONString(m);
        return s;
    }

    /**
     * xml转json
     *
     * @param xmlStr
     * @return
     * @throws DocumentException
     */
    public static JSONObject xml2Json(String xmlStr) throws DocumentException {
        Document doc = DocumentHelper.parseText(xmlStr);
        JSONObject json = new JSONObject();
        dom4j2Json(doc.getRootElement(), json);
        return json;
    }

    /**
     * xml转json
     *
     * @param element
     * @param json
     */
    public static void dom4j2Json(Element element, JSONObject json) {
        //如果是属性
        for (Object o : element.attributes()) {
            Attribute attr = (Attribute) o;
            if (!isEmpty(attr.getValue())) {
                json.put("@" + attr.getName(), attr.getValue());
            }
        }
        List<Element> chdEl = element.elements();
        if (chdEl.isEmpty() && !isEmpty(element.getText())) {
            //如果没有子元素,只有一个值
            json.put(element.getName(), element.getText());
        }

        for (Element e : chdEl) {
            //有子元素
            if (!e.elements().isEmpty()) {
                //子元素也有子元素
                JSONObject chdjson = new JSONObject();
                dom4j2Json(e, chdjson);
                Object o = json.get(e.getName());
                if (o != null) {
                    JSONArray jsona = null;
                    if (o instanceof JSONObject) {
                        //如果此元素已存在,则转为jsonArray
                        JSONObject jsono = (JSONObject) o;
                        json.remove(e.getName());
                        jsona = new JSONArray();
                        jsona.add(jsono);
                        jsona.add(chdjson);
                    }
                    if (o instanceof JSONArray) {
                        jsona = (JSONArray) o;
                        jsona.add(chdjson);
                    }
                    json.put(e.getName(), jsona);
                } else {
                    if (!chdjson.isEmpty()) {
                        json.put(e.getName(), chdjson);
                    }
                }

            } else {
                //子元素没有子元素
                for (Object o : element.attributes()) {
                    Attribute attr = (Attribute) o;
                    if (!isEmpty(attr.getValue())) {
                        json.put("@" + attr.getName(), attr.getValue());
                    }
                }
                if (!e.getText().isEmpty()) {
                    json.put(e.getName(), e.getText());
                }
            }
        }
    }

    public static boolean isEmpty(String str) {

        if (str == null || str.trim().isEmpty() || "null".equals(str)) {
            return true;
        }
        return false;
    }

}
