package com.starsky.common.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author wansh
 * @version 1.0
 * @des 日期时间工具籿
 * @email 1057718341@qq.com
 */
public class DateUtils {

    public static String YEAR = "yyyy";
    public static String YEAR_MONTH = "yyyyMM";
    public static String YEAR_MONTH_DAY = "yyyyMMdd";
    public static final String HMS = "HH:mm:ss";
    public static final String YMD = "yyyy-MM-dd";
    public static final String YMDHMSS2 = "yyyyMMdd HHmmss";
    public static final String YMDHM = "yyyy-MM-dd HH:mm";
    public static final String YMDHMS = "yyyy-MM-dd HH:mm:ss";
    public static final String YMDHMSZ = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String YMDHMSSZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String YMDHMSS = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String[] dayOfWeek = new String[]{"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};


    /**
     * 日期格式化 将字符串日期转为其他格式字符串
     *
     * @param time      日期字符串
     * @param orgFormat 还原日期格式
     * @param desFormat 转换日期格式
     * @return 返回指定格式日期字符串
     */
    public static String format(String time, String orgFormat, String desFormat) {
        Date date = parse(time, orgFormat);
        String timeStr = format(date, desFormat);
        return timeStr;
    }

    /**
     * 日期格式化 将日期转为其他格式日期
     *
     * @param date      日期
     * @param orgFormat 还原日期格式
     * @param desFormat 转换日期格式
     * @return 返回指定格式日期字符串
     */
    public static Date parse(Date date, String orgFormat, String desFormat) {
        String timeStr = format(date, orgFormat);
        Date format = parse(timeStr, desFormat);
        return format;
    }

    /**
     * 将日期字符串格式化为制定格式日期字符串
     * 默认格式化 yyyy-MM-dd HH:mm:ss
     *
     * @param date: 日期
     * @return 默认yyyy-MM-dd HH:mm:ss
     */
    public static String format(Date date) {
        if (date == null) {
            date = new Date();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(YMDHMS);
        return dateFormat.format(date);
    }

    /**
     * 将日期字符串格式化为制定格式日期字符串
     * 默认格式化 yyyy-MM-dd HH:mm:ss
     *
     * @param date:                     日期
     * @param regex：格式化表达式，默认yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String format(Date date, String regex) {
        if (StringUtils.isEmpty(regex)) {
            regex = YMDHMS;
        }
        if (date == null) {
            date = new Date();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(regex);
        return dateFormat.format(date);
    }

    /**
     * 根据规则解析日期字符串
     *
     * @param dateStr 日期字符串
     * @return 格式化表达式，默认yyyy-MM-dd HH:mm:ss
     */
    public static Date parse(String dateStr) {
        try {
            if (dateStr == null) {
                return new Date();
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat(YMDHMS);
            Date time = dateFormat.parse(dateStr);
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据规则解析日期字符串
     *
     * @param dateStr 日期字符串
     * @param regex   格式化表达式，默认yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Date parse(String dateStr, String regex) {
        try {
            if (StringUtils.isEmpty(regex)) {
                regex = YMDHMS;
            }
            if (dateStr == null) {
                return new Date();
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat(regex);
            Date time = dateFormat.parse(dateStr);
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获得当前日期时间
     * <p>
     * 日期时间格式yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static String getDateTimeStr(String regex) {
        return format(new Date(), regex);
    }

    /**
     * 获得当前日期时间
     * <p>
     * 日期时间格式yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static Date getDateTime(String regex) {
        return parse(format(new Date(), regex), regex);
    }

    /**
     * 将日期字符串格式化为制定格式日期字符串
     * 默认格式化 yyyy-MM-dd HH:mm:ss
     *
     * @param dateStr 日期字符串
     * @param regex   格式化表达式，默认yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getDateTimeStr(String dateStr, String regex) {
        if (StringUtils.isEmpty(regex)) {
            regex = YMDHMS;
        }
        return format(parse(dateStr, regex), regex);
    }

    /**
     * 将日期格式化为制定格式日期字符串
     * 默认格式化 yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static String getDateTimeStr(Date date, String regex) {
        if (StringUtils.isEmpty(regex)) {
            regex = YMDHMS;
        }
        if (date == null) {
            date = new Date();
        }
        return format(date, regex);
    }

    /**
     * 根据指定日期时间戳格式化为制定日期
     * 默认格式化 yyyy-MM-dd HH:mm:ss
     *
     * @param datetime 日期毫秒值
     * @param regex    日期格式
     * @return
     */
    public static Date getDateTime(long datetime, String regex) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(datetime);

        String date = format(calendar.getTime(), regex);
        return parse(date, regex);
    }

    /**
     * 获得日期时间,根据指定规则格式化时间
     * 默认格式化 yyyy-MM-dd HH:mm:ss
     *
     * @param dateStr:                  日期字符串
     * @param regex：格式化表达式，默认yyyy-MM-dd HH:mm:ss
     */
    public static Date getDateTime(String dateStr, String regex) {
        if (StringUtils.isEmpty(regex)) {
            regex = YMDHMS;
        }
        return parse(dateStr, regex);
    }

    /**
     * 获得日期时间,根据指定规则格式化时间
     * 默认格式化 yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static Date getDateTime(Date date, String regex) {
        if (StringUtils.isEmpty(regex)) {
            regex = YMDHMS;
        }
        if (date == null) {
            date = new Date();
        }
        return parse(format(date, regex), regex);
    }

    /**
     * 给制定日期添加自定字段时间
     *
     * @param field  指定字段类型,Calendar里包含的几个类型（YEAR-年、MONTH-月、DAY-日、HOUR-时、分、秒）
     * @param amount 年、月、日、时、分、秒正负数，加减几x后的时间
     * @return
     */
    public static Date addDate(Date date, int field, int amount) {
        // 获取的系统当前时间
//        Calendar calendar = GregorianCalendar.getInstance(Locale.CHINESE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // 得到前、后几天
        calendar.add(field, amount);
        return calendar.getTime();
    }

    /**
     * 获取指定日期的前、后时间
     *
     * @param date：时间
     * @param year：正负数，正数标识以后前几、负数标识以前几年
     * @return
     */
    public static Date addDateYear(Date date, int year) {
        return addDate(date, Calendar.YEAR, year);
    }

    /**
     * 获取指定日期的前、后时间
     *
     * @param date：时间
     * @param month：正负数，正数标识以后前几、负数标识以前几月
     * @return
     */
    public static Date addDateMonth(Date date, int month) {
        return addDate(date, Calendar.MONTH, month);
    }

    /**
     * 获取指定日期的前、后时间
     *
     * @param date：时间
     * @param day：正负数，正数标识以后几、负数标识以前几天
     * @return
     */
    public static Date addDateDay(Date date, int day) {
        return addDate(date, Calendar.DAY_OF_MONTH, day);
    }

    /**
     * 获取指定日期的前、后时间
     *
     * @param date：时间
     * @param week：正负数，加减天
     * @return
     */
    private static Date addDateWeekDay(Date date, int week) {
        return addDate(date, Calendar.DAY_OF_WEEK, week);
    }

    /**
     * 获取指定日期的前、后时间
     *
     * @param date：时间
     * @param hour：正负数，加减几小时后的日期
     * @return
     */
    public static Date addDateHour(Date date, int hour) {
        return addDate(date, Calendar.HOUR, hour);
    }

    /**
     * 获取指定日期的前、后时间
     *
     * @param date：时间
     * @param minute：正负数，加减几分钟
     * @return
     */
    public static Date addDateMinute(Date date, int minute) {
        return addDate(date, Calendar.MINUTE, minute);
    }

    /**
     * 获取指定日期的前、后时间
     *
     * @param date：时间
     * @param second：正负数，加减几秒后的日期
     * @return
     */
    public static Date addDateSecond(Date date, int second) {
        return addDate(date, Calendar.SECOND, second);
    }

    public static Calendar calendar() {
//        Calendar cal = GregorianCalendar.getInstance(Locale.CHINESE);
//        cal.setFirstDayOfWeek(Calendar.MONDAY);
        Calendar calendar = Calendar.getInstance();
        return calendar;
    }

    public static Calendar calendar(Date date) {
//        Calendar cal = GregorianCalendar.getInstance(Locale.CHINESE);
        Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
        }
        return calendar;
    }

    /**
     * 获得当前时间的毫秒数
     * <p>
     * 详覺{@link System#currentTimeMillis()}
     *
     * @return
     */
    public static long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    /**
     * 获得当前Chinese年份
     *
     * @return
     */
    public static int getYear(Date date) {
        return calendar(date).get(Calendar.YEAR);
    }

    /**
     * 获得当前Chinese月份
     *
     * @return
     */
    public static int getMonth(Date date) {
        return calendar(date).get(Calendar.MONTH) + 1;
    }

    /**
     * 获得当前时间是月份中的第几天
     *
     * @return
     */
    public static int getDayOfMonth(Date date) {
        return calendar(date).get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 今天是星期的第几天
     *
     * @return
     */
    public static int getDayOfWeek(Date date) {
        return calendar(date).get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 今天是年中的第几天
     *
     * @return
     */
    public static int getDayOfYear(Date date) {
        return calendar(date).get(Calendar.DAY_OF_YEAR);
    }

    /**
     * 判断原日期是否在目标日期之前
     *
     * @param src：源日期
     * @param dst：目标日期
     * @return
     */
    public static boolean isBefore(Date src, Date dst) {
        return src.before(dst);
    }

    /**
     * 判断原日期是否在目标日期之后
     *
     * @param src：源日期
     * @param dst：目标日期
     * @return
     */
    public static boolean isAfter(Date src, Date dst) {
        return src.after(dst);
    }

    /**
     * 判断两日期是否相等
     *
     * @param srcDate：源日期
     * @param dstDate：目标日期
     * @return
     */
    public static boolean isEqual(Date srcDate, Date dstDate) {
        return srcDate.compareTo(dstDate) == 0;
    }

    /**
     * 判断某个日期是否在【beginDate，endDate】日期范围内
     *
     * @param beginDate 开始日期
     * @param endDate   结束日期
     * @param src       判断的日期
     * @return
     */
    public static boolean between(Date beginDate, Date endDate, Date src) {
        return beginDate.before(src) && endDate.after(src);
    }

    /**
     * 获得当前月的最后日期
     * <p>
     * HH:mm:ss丿，毫秒为999
     *
     * @return
     */
    public static Date lastDayOfMonth() {
        Calendar cal = calendar();
        cal.set(Calendar.DAY_OF_MONTH, 0); // M月置零
        cal.set(Calendar.HOUR_OF_DAY, 0);// H置零
        cal.set(Calendar.MINUTE, 0);// m置零
        cal.set(Calendar.SECOND, 0);// s置零
        cal.set(Calendar.MILLISECOND, 0);// S置零
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);// 月份+1
        cal.set(Calendar.MILLISECOND, -1);// 毫秝-1
        return cal.getTime();
    }

    /**
     * 获得当前月的第一天日期
     * <p>
     * HH:mm:ss SS为零
     *
     * @return
     */
    public static Date firstDayOfMonth() {
        Calendar cal = calendar();
        cal.set(Calendar.DAY_OF_MONTH, 1); // M月置1
        cal.set(Calendar.HOUR_OF_DAY, 0);// H置零
        cal.set(Calendar.MINUTE, 0);// m置零
        cal.set(Calendar.SECOND, 0);// s置零
        cal.set(Calendar.MILLISECOND, 0);// S置零
        return cal.getTime();
    }


    /**
     * 获取星期中的第一天
     *
     * @return
     */
    public static Date monday() {
        return addDateWeekDay(new Date(), Calendar.MONDAY);
    }

    /**
     * 获得周五日期
     * <p>
     * 注：日历工厂方法{@link #calendar()}设置类每个星期的第一天为Monday，US等每星期第一天为sunday
     *
     * @return
     */
    public static Date friday() {
        return addDateWeekDay(new Date(), Calendar.FRIDAY);
    }

    /**
     * 获得周六日期
     * <p>
     * 注：日历工厂方法{@link #calendar()}设置类每个星期的第一天为Monday，US等每星期第一天为sunday
     *
     * @return
     */
    public static Date saturday() {
        return addDateWeekDay(new Date(), Calendar.SATURDAY);
    }

    /**
     * 获得周日日期
     * <p>
     * 注：日历工厂方法{@link #calendar()}设置类每个星期的第一天为Monday，US等每星期第一天为sunday
     *
     * @return
     */
    public static Date sunday() {
        return addDateWeekDay(new Date(), Calendar.SUNDAY);
    }

    /**
     * 获取当前日期的前几天日期
     *
     * @param day   int 加减天数
     * @param regex 日期格式化
     */
    public static String getDateDayStr(int day, String regex) {
        return getDateTimeStr(addDateDay(new Date(), day), regex);
    }

    /**
     * 获取上/下个月时间
     *
     * @param month 加减月数
     * @param regex 日期格式化
     */
    public static String getDateMonthStr(int month, String regex) {
        return getDateTimeStr(addDateMonth(new Date(), month), regex);
    }

    /**
     * 获取当前日期是星期几
     *
     * @return
     */
    public static String dayOfWeekInChines() {
        int dayOfWeek = DateUtils.getDayOfWeek(new Date());
        return DateUtils.dayOfWeek[dayOfWeek - 1];
    }

    /**
     * 获取日期是星期几
     *
     * @param dateStr
     * @return
     */
    public static String getWeek(String dateStr) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDateTime(dateStr, YMD));
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        String rst = dayOfWeek[week_index];
        return rst;
    }

    /**
     * 获取两个日期之间有多少个星期中文字符（每个星期只有五天，即不包含周六，周日）
     *
     * @param beginTime 开始日期
     * @param endTime   结束日期
     * @return
     */
    public static List<String> getFiveWorkDayFromTody(String beginTime, String endTime) {
        List<String> resList = new ArrayList<String>();
        if (beginTime == null) {
            beginTime = DateUtils.getDateDayStr(-7, YMD);
        }
        if (endTime == null) {
            endTime = DateUtils.getDateTimeStr(YMD);
        }
        Date startDate = getDateTime(beginTime, YMD);
        Date today = getDateTime(endTime, YMD);
        while (!startDate.after(today)) {
            String week = getWeek(getDateTimeStr(startDate, YMD));
            if (!(dayOfWeek[0].equals(week) || dayOfWeek[6].equals(week))) {
                resList.add(week);
            }
            startDate = addDate(startDate, Calendar.DAY_OF_MONTH, 1);
        }
        return resList;
    }

    /**
     * 计算两个日期之间相差的天数
     *
     * @param startDateStr 较小的时闿
     * @param endDateStr   较大的时闿
     * @return 相差天数
     */
    public static int daysBetween(String startDateStr, String endDateStr) {
        Date startDate = getDateTime(startDateStr, YMD);
        Date endDate = getDateTime(endDateStr, YMD);
        int rst = daysBetween(startDate, endDate);
        return rst;
    }

    /**
     * 计算两个日期之间相差的天数
     *
     * @param startDate 较小的时闿
     * @param endDate   较大的时闿
     * @return 相差天数
     */
    public static int daysBetween(Date startDate, Date endDate) {

        startDate = getDateTime(startDate, YMD);
        endDate = getDateTime(endDate, YMD);

        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        long time1 = cal.getTimeInMillis();

        cal.setTime(endDate);
        long time2 = cal.getTimeInMillis();

        long between_days = (time2 - time1) / (1000 * 3600 * 24);
        int rst = Integer.parseInt(String.valueOf(between_days));
        return rst;
    }


    /**
     * 获取星期中的某一天
     *
     * @param date
     * @return
     */
    public static String getWeekOfNum(String date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDateTime(date, YMD));
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        return "" + week_index;
    }


    /**
     * 获取上几周第一天(周日)
     *
     * @param date
     * @param day  上几周
     * @return 返回类型yyyy-MM-dd
     */
    public static String getLastDay(String date, int day, int dayOfWeek) {
        Calendar calendar = Calendar.getInstance();// 此时打印它获取的是系统当前时闿
        calendar.setTime(getDateTime(date, YMD));
        calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, day);
        calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        return getDateTimeStr(calendar.getTime(), YMD);
    }

    /**
     * 获取上周第一天(周日)
     *
     * @param date 传入当前日期
     * @return 返回类型yyyy-MM-dd
     */
    public static String getLastSunDay(String date) {
        return getLastSunDay(date, -1);
    }

    /**
     * 获取上几周第一天(周日)
     *
     * @param date
     * @param day  上几周
     * @return 返回类型yyyy-MM-dd
     */
    public static String getLastSunDay(String date, int day) {
        String lastDay = getLastDay(date, day, Calendar.SUNDAY);
        return lastDay;
    }

    /**
     * 获取上周最后一天(周六)
     *
     * @param date
     * @return 返回类型yyyy-MM-dd
     */
    public static String getLastSaturday(String date) {
        String lastDay = getLastDay(date, -1, Calendar.SATURDAY);
        return lastDay;
    }

    /**
     * 获取上几周最后一天(周六)
     *
     * @param date
     * @param day  上几周
     * @return 返回类型yyyy-MM-dd
     */
    public static String getLastSaturday(String date, int day) {
        String lastDay = getLastDay(date, day, Calendar.SATURDAY);
        return lastDay;
    }

    /**
     * 获取前后day月最后一天 正数为未来负数为过去day日期
     *
     * @param date       当前日期
     * @param month      月份，正数
     * @param dayOfMonth 月中的天，正负数
     * @param regex      返回日期格式类型
     */
    public static String getDayOfMonth(String date, int month, int dayOfMonth, String regex) {

        if (StringUtils.isEmpty(regex)) {
            regex = YMD;
        }

        Calendar cal = calendar();
        cal.setTime(parse(date, regex));
        cal.add(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth); // M月置1

        return format(cal.getTime(), regex);
    }


    /**
     * 获取前后month月最后一天， 正数为未来month,负数为过去month
     *
     * @param date 传入当前日期
     * @return 返回类型yyyy-MM-dd
     */
    public static String getMonthDay(String date, int month) {
        String dayOfMonth = getDayOfMonth(date, month + 1, 0, YMD);
        return dayOfMonth;
    }

    /**
     * 获取前后month月第几天数， 正数为未来month,负数为过去month
     *
     * @param date  日期字符串
     * @param day   天数，正负数，加减几天日期
     * @param regex 日期格式
     * @return 返回类型yyyy-MM-dd
     */
    public static String getMonthFirstDay(String date, int day, String regex) {
        String dayOfMonth = getDayOfMonth(date, day, 1, regex);
        return dayOfMonth;
    }

    /**
     * 获取传入时间的月份最后一天一天
     *
     * @param date  当前日期
     * @param month 月份，正数-表示当前时间加上月份，负数-表示当前月份减去月份
     * @param regex 返回日期格式类型
     * @return String yyyy-MM-dd
     */
    public static String getMonthLastDay(String date, int month, String regex) {
        Calendar cal = calendar();
        int actualMaximum = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        String dayOfMonth = getDayOfMonth(date, month, actualMaximum, regex);
        return dayOfMonth;
    }

    /**
     * 获取当前月的最后一天
     *
     * @param @param  date
     * @param @return
     * @param
     * @return String
     */
    public static String getMonthLastDayStr(String date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDateTime(date, YMDHMS));

        int actualMaximum = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, actualMaximum);

        return format(cal.getTime(), YMDHMS);
    }

    /**
     * TODO 中国习惯上一周第一天(上周一)
     *
     * @param date
     * @param day
     * @return
     */
    public static String getMonday(String date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parse(date, YMDHMS));
        int dayofWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayofWeek == 1) {
            --day;
        }
        calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, day);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        return format(calendar.getTime(), YMDHMS);
    }

    /**
     * TODO 中国习惯上一周最后一天(上周日)
     *
     * @param date
     * @param day
     * @return
     */
    public static String getSun(String date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parse(date, YMDHMS));
        int dayofWeek = calendar.get(Calendar.DAY_OF_WEEK);

        if (dayofWeek != 1) {
            ++day;
        }
        calendar.add(Calendar.DAY_OF_WEEK_IN_MONTH, day);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

        return format(calendar.getTime(), YMDHMS);
    }

    /**
     * 将utc 时间转为本地时间
     *
     * @param time
     * @param partten
     * @return
     */
    public static String utc2Local(String time, String partten) {

        try {
            if (StringUtils.isEmpty(partten)) {
                partten = YMDHMS;
            }
            String pattern = time.contains(".") ? YMDHMSSZ : YMDHMSZ;
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

            return getDateTimeStr(sdf.parse(time), partten);
        } catch (Exception e) {
        }
        return null;
    }


    /**
     * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
     *
     * @param nowTime   当前时间
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     * @author jqlin
     */
    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 获取开始时间
     *
     * @param date： 日期
     * @return
     */
    public static Date getStartDate(Date date) {
        String datePrefix = format(date, YMD);
        String begin = datePrefix + " 00:00:00";
        Date beginTime = DateUtils.parse(begin, DateUtils.YMDHMS);
        return beginTime;
    }

    /**
     * 获取开始时间
     *
     * @param datePrefix： 日期前缀，格式为yyyy-MM-dd
     * @return
     */
    public static Date getStartDate(String datePrefix) {
        String begin = datePrefix + " 00:00:00";
        Date beginTime = DateUtils.parse(begin, DateUtils.YMDHMS);
        return beginTime;
    }

    /**
     * 获取结束时间
     *
     * @param datePrefix： 日期前缀，格式为yyyy-MM-dd
     * @return
     */
    public static Date getEndDate(String datePrefix) {
        String end = datePrefix + " 23:59:59";
        Date endTime = DateUtils.parse(end, DateUtils.YMDHMS);
        return endTime;
    }


    /**
     * 获取结束时间
     *
     * @param date： 日期
     * @return
     */
    public static Date getEndDate(Date date) {
        String datePrefix = format(date, YMD);
        String end = datePrefix + " 23:59:59";
        Date beginTime = DateUtils.parse(end, DateUtils.YMDHMS);
        return beginTime;
    }

    /**
     * 生成日期(将日期前缀与后缀拼接生成新的日期)
     *
     * @param date：日期
     * @param dateSubfix：日期后缀（格式 HH:mm:ss）
     * @return
     */
    public static Date rightPad(Date date, String dateSubfix) {
        String datePrefix = format(date, YMD);
        String end = datePrefix + " " + dateSubfix;
        Date time = DateUtils.parse(end, DateUtils.YMDHMS);
        return time;
    }

    /**
     * 获取指定时间
     *
     * @param date        日期
     * @param orgFormat   还原日期格式
     * @param desFormat   转换日期格式
     * @param dateSubfix： 日期后缀
     * @return
     */
    public static Date getDateTime(Date date, String orgFormat, String desFormat, String dateSubfix) {
        if (StringUtils.isEmpty(orgFormat)) orgFormat = "yyyy-MM-dd";
        if (StringUtils.isEmpty(desFormat)) desFormat = YMDHMS;

        String begin = DateUtils.format(date, orgFormat);
        if (StringUtils.isEmpty(dateSubfix)) {
            begin += dateSubfix;
        }
        Date time = DateUtils.parse(begin, desFormat);
        return time;
    }


    /**
     * 判断当前日期是否为工作日
     * 工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2
     *
     * @param httpArg：日期，格式为yyyyMMdd
     * @return
     */
    public static int holiday(String httpArg) {

        int date = 0;//工作日对应结果为 0, 休息日对应结果为 1, 节假日对应的结果为 2
        try {
            String httpUrl = "http://www.easybots.cn/api/holiday.php";
            StringBuffer sbf = new StringBuffer();
            httpUrl = httpUrl + "?d=" + httpArg;

            URL url = new URL(httpUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            InputStream is = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String strRead = null;
            while ((strRead = reader.readLine()) != null) {
                sbf.append(strRead);
                sbf.append("\r\n");
            }
            reader.close();
            String result = sbf.toString();
            JSONObject ob = JSONObject.parseObject(result);
            if (ob != null) {
                date = Integer.parseInt(ob.getString(httpArg));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }


    public static void main(String[] args) {
        String dateTimeStr = DateUtils.getDateTimeStr(new Date(), YMDHMS);
        System.out.println("dateTimeStr: " + dateTimeStr);

        String monthLastDay = getMonthLastDay(dateTimeStr, -2, YMDHMS);
        System.out.println("monthLastDay: " + monthLastDay);


        String date = "2021-03-20 08:35:49";
        String dateStr = "2021-05-20 09:35:49";
        List<String> fiveWorkDayFromTody = getFiveWorkDayFromTody(date, dateStr);
        System.out.println("fiveWorkDayFromTody: " + fiveWorkDayFromTody.toString());

        System.out.println("getWeek: " + getWeek(date));
    }

}
