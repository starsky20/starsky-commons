package com.starsky.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *@desc: 属性名转换工具
 *@author: wangsh
 *
 */
public class EntityUtils {
	
	private static Pattern linePattern = Pattern.compile("_(\\w)");
	private static Pattern humpPattern = Pattern.compile("[A-Z]");
	 
	/**
     * <p>下划线转驼峰</p>
     * <pre>
     * </pre>
	 * @param str 下划线字段名
	 * @return String
	 */
	public static String lineToHump(String str) {
		str = str.toLowerCase();
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
 
	/**
     * <p>驼峰转下划线(简单写法，效率低于{@link #humpToLine2(String)})</p>
     * <pre>
     * </pre>
	 * @param str 驼峰线字段名
	 * @return String
	 */
	public static String humpToLine(String str) {
		return str.replaceAll("[A-Z]", "_$0").toLowerCase();
	}
 
	/**
     * <p>驼峰转下划线,效率比上面高</p>
     * <pre>
     * </pre>
	 * @param str 驼峰线字段名
	 * @return String
	 */
	public static String humpToLine2(String str) {
		Matcher matcher = humpPattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
}
