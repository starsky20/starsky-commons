package com.starsky.common.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.HashSet;
import java.util.Set;

/**
 * @desc: 处理汉字和对应拼音转换的工具类
 * @author: wangsh
 */
public class PinYinUtil {

    public static void main(String[] args) {
        String s = "和乐";
        for (String ss : PinYinUtil.getPinYinSet(s)) {
            System.out.println(PinYinUtil.getPinYin(ss));
        }
    }

    /**
     * 传入汉字字符串，拼接成对应的拼音,返回拼音的集合
     *
     * @param chinese 中文汉字
     * @return
     */
    public static Set<String> getPinYinSet(String chinese) {
        Set<String> lstResult = new HashSet<String>();
        char[] t1 = null;
        // 字符串转换成char数组
        t1 = chinese.toCharArray();
        // ①迭代汉字
        for (char ch : t1) {
            String s[] = getPinYin(ch);
            Set<String> lstNew = new HashSet<String>();
            // ②迭代每个汉字的拼音数组
            for (String str : s) {
                if (lstResult.size() == 0) {
                    lstNew.add(str);
                } else {
                    for (String ss : lstResult) {
                        ss += str;
                        lstNew.add(ss);
                    }
                }
            }
            lstResult.clear();
            lstResult = lstNew;
        }
        return lstResult;
    }

    /**
     * 传入中文汉字，转换出对应拼音 (出现同音字，默认选择汉字全拼的第一种读音)
     *
     * @param chinese 中文汉字
     * @return
     */
    public static String getPinYin(String chinese) {
        char[] t1 = null;
        t1 = chinese.toCharArray();
        String[] t2 = new String[t1.length];

        // 设置汉字拼音输出的格式
        HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();
        // 用小写
        t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        // 不带声调
        t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        // 用v表示ü
        t3.setVCharType(HanyuPinyinVCharType.WITH_V);
        String t4 = "";
        int t0 = t1.length;
        try {
            for (int i = 0; i < t0; i++) {
                // 判断能否为汉字字符
                if (Character.toString(t1[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    // 将汉字的几种全拼都存到t2数组中
                    t2 = PinyinHelper.toHanyuPinyinStringArray(t1[i], t3);
                    // 取出该汉字全拼的第一种读音并连接到字符串t4后
                    t4 += t2[0];
                } else {
                    // 如果不是汉字字符，间接取出字符并连接到字符串t4后
                    t4 += Character.toString(t1[i]);
                }
            }
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            e.printStackTrace();
        }
        return t4;
    }

    /**
     * 将单个汉字转换成汉语拼音，考虑到同音字问题，返回字符串数组的形式
     *
     * @return
     */
    public static String[] getPinYin(char chinese) {
        char[] t1 = {chinese};
        String[] t2 = new String[t1.length];

        // 设置汉字拼音输出的格式
        HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();
        // 小写
        t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        // 无声调
        t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        // 用v表示ü
        t3.setVCharType(HanyuPinyinVCharType.WITH_V);
        // 判断能否为汉字字符
        if (Character.toString(t1[0]).matches("[\\u4E00-\\u9FA5]+")) {
            try {
                // 将汉字的几种全拼都存到t2数组中
                t2 = PinyinHelper.toHanyuPinyinStringArray(t1[0], t3);
            } catch (BadHanyuPinyinOutputFormatCombination e) {
                e.printStackTrace();
            }
        } else {
            // 如果不是汉字字符，则把字符直接放入t2数组中
            t2[0] = String.valueOf(chinese);
        }
        return t2;
    }

    /**
     * 传入没有多音字的中文汉字，转换出对应拼音(如果传入的中文中有同音字会返回字符串信息：含有同音字)
     *
     * @return
     */
    public static String getNoPolyphone(String chinese) {
        char[] t1 = null;
        t1 = chinese.toCharArray();
        String[] t2 = new String[t1.length];
        // 设置汉字拼音输出的格式
        HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();
        // 小写
        t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        // 无声调
        t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        // 用v表示ü
        t3.setVCharType(HanyuPinyinVCharType.WITH_V);
        String t4 = "";
        int t0 = t1.length;
        try {
            for (int i = 0; i < t0; i++) {
                // 判断能否为汉字字符
                if (Character.toString(t1[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    // 将汉字的几种全拼都存到t2数组中
                    t2 = PinyinHelper.toHanyuPinyinStringArray(t1[i], t3);
                    if (t2.length > 1) {
                        return "含有同音字";
                    } else {
                        // 取出该汉字全拼的第一种读音并连接到字符串t4后
                        t4 += t2[0];
                    }
                } else {
                    // 如果不是汉字字符，间接取出字符并连接到字符串t4后
                    t4 += Character.toString(t1[i]);
                }
            }
        } catch (BadHanyuPinyinOutputFormatCombination e) {
            e.printStackTrace();
        }
        return t4;
    }

    /**
     * 汉字转换位汉语拼音首字母，英文字符不变
     *
     * @param chinese 汉字
     * @return 拼音
     */
    public static String getPinYinFirstSpell(String chinese) {
        String pinyinName = "";
        char[] nameChar = chinese.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < nameChar.length; i++) {
            if (nameChar[i] > 128) {
                try {
                    pinyinName += PinyinHelper.toHanyuPinyinStringArray(
                            nameChar[i], defaultFormat)[0].charAt(0);
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            } else {
                pinyinName += nameChar[i];
            }
        }
        return pinyinName;
    }

}
