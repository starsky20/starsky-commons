package com.starsky.common.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 *@desc: 对象转换工具
 *@author: wangsh
 *
 */
public class MapUtils {

    public static void main(String[] args) {
        Map<String, String[]> map = new HashMap<String, String[]>();
        map.put("name", new String[]{"zhangsan"});
        map.put("like", new String[]{"pingpang", "basketball"});
        Map<String, String> map2 = getParameterMap(map);
        for (Entry<String, String> entry : map2.entrySet()) {
            System.out.println("key:" + entry.getKey() + ",value锛�" + entry.getValue());
        }
    }

    /**
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static Map<String, String> getParameterMap(Map properties) {
        // 杩斿洖鍊糓ap
        Map<String, String> returnMap = new HashMap<String, String>();
        // 鍙傛暟Map
        Iterator entries = properties.entrySet().iterator();
        while (entries.hasNext()) {
            Entry entry = (Entry) entries.next();
            String name = (String) entry.getKey();
            Object valueObj = entry.getValue();
            String value = "";
            if (null == valueObj) {
                value = "";
            } else if (valueObj instanceof String[]) {
                String[] values = (String[]) valueObj;
                for (int i = 0; i < values.length; i++) {
                    value += values[i] + ",";
                }
                value = value.substring(0, value.length() - 1);
            } else {
                value = valueObj.toString();
            }
            returnMap.put(name, value);
        }
        return returnMap;
    }


    /**
     * 利用反射将map集合封装成bean对象
     *
     * @param map
     * @param clazz
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, Class<?> clazz) {

        Object obj = null;
        try {
            obj = clazz.newInstance();
            if (map != null && !map.isEmpty() && map.size() > 0) {
                for (Entry<String, Object> entry : map.entrySet()) {
                    String propertyName = entry.getKey();    // 属性名
                    Object value = entry.getValue();        // 属性值
                    String setMethodName = "set" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
//                    try {
                    //获取和map的key匹配的属性名称
                    Field field = getClassField(clazz, propertyName);
                    if (field == null) {
                        continue;
                    }
                    Class<?> fieldTypeClass = field.getType();
                    value = convertValType(value, fieldTypeClass);
                    field.set(obj, value);
//                        clazz.getMethod(setMethodName, field.getType()).invoke(obj, value);
//                    } catch (NoSuchMethodException e) {
//                        e.printStackTrace();
//                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) obj;
    }

    /**
     * 根据给定对象类匹配对象中的特定字段
     *
     * @param clazz
     * @param fieldName
     * @return
     */
    private static Field getClassField(Class<?> clazz, String fieldName) {
        if (Object.class.getName().equals(clazz.getName())) {
            return null;
        }
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.getName().equals(fieldName)) {
                return field;
            }
        }
        //如果该类还有父类，将父类对象中的字段也取出
        Class<?> superClass = clazz.getSuperclass();
        if (superClass != null) {
            //递归获取
            return getClassField(superClass, fieldName);
        }
        return null;
    }

    /**
     * 将map的value值转为实体类中字段类型匹配的方法
     *
     * @param value
     * @param fieldTypeClass
     * @return
     */
    private static Object convertValType(Object value, Class<?> fieldTypeClass) {
        Object retVal = null;
        if (value == null || "".equals(value)) {
            return retVal;
        }
        if (Long.class.getName().equals(fieldTypeClass.getName())
                || long.class.getName().equals(fieldTypeClass.getName())) {
            retVal = Long.parseLong(value.toString());
        } else if (Integer.class.getName().equals(fieldTypeClass.getName())
                || int.class.getName().equals(fieldTypeClass.getName())) {
            retVal = Integer.parseInt(value.toString());
        } else if (Float.class.getName().equals(fieldTypeClass.getName())
                || float.class.getName().equals(fieldTypeClass.getName())) {
            retVal = Float.parseFloat(value.toString());
        } else if (Double.class.getName().equals(fieldTypeClass.getName())
                || double.class.getName().equals(fieldTypeClass.getName())) {
            retVal = Double.parseDouble(value.toString());
        } else if (java.util.Date.class.getName().equals(fieldTypeClass.getName())) {
            DateUtils.getDateTimeStr(value.toString(), null);


            retVal = Double.parseDouble(value.toString());
        } else {
            retVal = value;
        }
        return retVal;
    }


    /**
     * 判断集合是否为空，是否小于等于0
     *
     * @param resMap
     * @return
     */
    public static boolean isEmpty(Map resMap) {
        return !isNotEmpty(resMap);
    }

    /**
     * 判断集合是否为空，是否大于0
     *
     * @param resMap
     * @return
     */

    public static boolean isNotEmpty(Map resMap) {
        if (resMap != null && resMap.size() > 0) {
            return true;
        }
        return false;
    }

}
