package com.starsky.common.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wangsh
 * @date 2019/8/7 18:50
 * @Description
 */
public class CacheUtils {
    //缓存数据
    private static ConcurrentHashMap<String, String> map = new ConcurrentHashMap();

    private static final CacheUtils cache = new CacheUtils();

    private CacheUtils() {
    }

    public static synchronized CacheUtils getInstance() {
        return cache;
    }

    public String getValue(String key) {
        return map.get(key);
    }

    public static Map<String, String> getCacheMap() {
        return map;
    }

}
