package com.starsky.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.starsky.common.constant.BaseConstant;
import com.starsky.common.enums.impl.EnumResult;
import com.starsky.common.exception.impl.ExceptionBusiness;
import com.starsky.common.utils.encrypt.MD5Utils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

/**
 * @Desc 签名认证
 */
public class SignValidate {

    /**
     * 签名认证
     * obj为类对象
     */
    public static boolean ValidateSign(Object obj) {

        //检查是否开启签名校验
        if (!BaseConstant.CHK_SIGN_SWITCH) {
            return true;
        }

        // 检查对象是否为空
        if (obj == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000008);
        }

        // 将对象转为Json对象
        JSONObject validateJson = (JSONObject) JSONObject.toJSON(obj);

        // 必要签名元素校验
        if (validateJson.size() <= 0 || !validateJson.containsKey("sign") || validateJson.get("sign") == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000008);
        }
        // 移除不参与签名的key
        validateJson.remove("tails");
        validateJson.remove("version");
        // 移除sign
        String sign = validateJson.remove("sign").toString();


        // 数据转map
        SortedMap<String, String> jsonmap = GsonUtils.toHashMap(validateJson);
        // 获取签名
        String valsign = MD5Utils.createSign(jsonmap, BaseConstant.ELEC_SIGN);
        // 对比签名
        if (!StringUtils.equals(sign.toUpperCase(), valsign.toUpperCase())) {
            throw ExceptionBusiness.getIntance(EnumResult.W000008);
        }
        return true;
    }

    /**
     * 签名认证
     * obj为类对象
     */
    public static boolean ValidateSign(Object obj, String elecSign) {

        //检查是否开启签名校验
        if (!BaseConstant.CHK_SIGN_SWITCH) {
            return true;
        }

        // 检查对象是否为空
        if (obj == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000008);
        }

        // 将对象转为Json对象
        JSONObject validateJson = (JSONObject) JSONObject.toJSON(obj);

        // 必要签名元素校验
        if (validateJson.size() <= 0 || !validateJson.containsKey("sign") || validateJson.get("sign") == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000008);
        }
        // 移除不参与签名的key
        validateJson.remove("tails");
        validateJson.remove("version");
        // 移除sign
        String sign = validateJson.remove("sign").toString();


        // 数据转map
        SortedMap<String, String> jsonmap = GsonUtils.toHashMap(validateJson);
        // 获取签名
        String valsign = MD5Utils.createSign(jsonmap, elecSign);
        // 对比签名
        if (!StringUtils.equals(sign.toUpperCase(), valsign.toUpperCase())) {
            throw ExceptionBusiness.getIntance(EnumResult.W000008);
        }
        return true;
    }

    /**
     * 返回签名
     * obj为类对象
     */
    public static String getSign(Object obj) {

        // 检查对象是否为空
        if (obj == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000008);
        }

        // 将对象转为Json对象
        JSONObject validateJson = (JSONObject) JSONObject.toJSON(obj);

        // 移除不参与签名的key
        validateJson.remove("tails");
        validateJson.remove("version");

        // 数据转map
        SortedMap<String, String> jsonmap = GsonUtils.toHashMap(validateJson);

        return MD5Utils.createSignTest(jsonmap, BaseConstant.ELEC_SIGN);
    }

    /**
     * 返回签名
     * obj为类对象
     */
    public static String getSign(Object obj, String elecSign) {

        // 检查对象是否为空
        if (obj == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000008);
        }

        // 将对象转为Json对象
        JSONObject validateJson = (JSONObject) JSONObject.toJSON(obj);

        // 移除不参与签名的key
        validateJson.remove("tails");
        validateJson.remove("version");

        // 数据转map
        SortedMap<String, String> jsonmap = GsonUtils.toHashMap(validateJson);

        return MD5Utils.createSignTest(jsonmap, elecSign);
    }

    public static void main(String[] args) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("b", "天成");
//        map.put("B", 1);
//        map.put("A", 2);
//        map.put("a", "你好");
//        map.put("sign", "sfasdfasdfsd");
//        ValidateSign(map);
        Map<String, Object> map = new HashMap<>();
        map.put("group", "发送到");
        map.put("name", "namea是");
        map.put("value", "23");
        System.out.println(getSign(map));
    }
}

