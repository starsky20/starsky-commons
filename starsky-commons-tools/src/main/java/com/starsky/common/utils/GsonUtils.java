package com.starsky.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Type;
import java.util.*;

/**
 * Json对象装换工具
 */
public class GsonUtils {

	private static Log log = LogFactory.getLog(GsonUtils.class);
	/**
	 * key 分隔符
	 */
	private static String KEY_SPLIT_FLAG = ".";

	/**
	 * Gson实例 disableHtmlEscaping 去掉转成json字符串时进行特殊字符的格式化操作
	 */
	private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").disableHtmlEscaping().create();

	private static Gson format = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").setPrettyPrinting().disableHtmlEscaping().create();

	private static Gson hasNull = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").serializeNulls().disableHtmlEscaping().create();

	/**
	 * TODO 将为null值的字段也返回
	 * 
	 * @return
	 */
	public static String parseBeanToJsonHasNull(Object object) {
		return hasNull.toJson(object);
	}

	/**
	 * 
	 @Desecration
	 * @return String
	 */
	public static String parseBeanToJson(Object object) {
		return gson.toJson(object);
	}

	/**
	 * TODO 格式化json
	 * 
	 */
	public static String parseBeanToFormatJson(Object object) {
		return format.toJson(object);
	}

	/**
	 * 将json串转换成java对象
	 * 
	 * @param <T>
	 * @param classes
	 * @param json
	 * @return
	 */
	public static <T> T parseJsonToBean(Class<T> classes, String json) {
		T t = gson.fromJson(json, classes);
		return t;
	}

	/**
	 * 将json串转换成java对象
	 * 
	 * @param str
	 * @param type
	 * @return
	 */
	public static <T> T fromJson(String str, Type type) {
		if (type == null) {
			type = new TypeToken<List<Map<String, Object>>>() {
			}.getType();
		}
		T t = gson.fromJson(str, type);
		return t;
	}

	/**
	 * 判断String是否是空 为空的情况包含：对象为空,为空字符串（""）,为字符串"null"
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isBlank(String value) {
		if (value == null || "".equals(value.trim()) || "null".equals(value.trim())) {
			return true;
		}
		return false;
	}

	/**
	 * 判断是否为一个合法的json串
	 * 
	 * @param json
	 * @return
	 */
	public static boolean isJson(String json) {
		try {
			GsonUtils.parseJsonToBean(Map.class, json);
			return true;
		} catch (Exception e) {
			return false;
		}
	}


	/**
	 * 校验Json
	 */
	public static boolean chkJson(String jsonStr) {

		if (StringUtils.isBlank(jsonStr)) {
			return false;
		}
		try {
			@SuppressWarnings("unused")
			JSONObject jsonObject = JSONObject.parseObject(jsonStr);
		} catch (Exception e) {
			try {
				@SuppressWarnings("unused")
				JSONArray jsonArray = JSONObject.parseArray(jsonStr);
			} catch (Exception ex) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 将一级json转换为map
	 */
	public static SortedMap<String, String> toHashMap(JSONObject jObject) {
		SortedMap<String, String> data = new TreeMap<>();
		// 遍历jsonObject数据，添加到Map对象
		for (String key : jObject.keySet()) {
			String value = jObject.getString(key);
			data.put(key, value);
		}
		return data;
	}


	public static void main(String[] args) {

		List list = new ArrayList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("8");

		Map<String, List> map = new HashMap<String, List>();
		map.put("-1", list);

		String json = GsonUtils.parseBeanToJson(map);
		System.out.println(json);

		String st = "{'-1':[ '32', '40', '41', '42', '43', '47', '48', '45', '44', '46', '49', '50', '58', '63','1001', '1002', '1003', '1004', '1005', '1006', '1007' ],'-2':[ '32', '40', '41', '42', '43', '47', '48', '45', '44', '46', '49', '50', '58', '63'],'-3':[ '1001', '1002', '1003', '1004', '1005', '1006', '1007' ]}";

		Map map2 = GsonUtils.parseJsonToBean(Map.class, st);
		Object object = map2.get("-1");

		System.out.println(object);
		System.out.println(map2.toString());
	}
}
