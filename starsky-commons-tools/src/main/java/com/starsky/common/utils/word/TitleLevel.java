package com.starsky.common.utils.word;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * TODO word标题的级别
 * @date	2016-12-2
 */
public enum TitleLevel {
	Null(0),
	Level_One(1),
	Level_Two(2),
	Level_Three(3),
	Level_Four(4),
	Level_Five(5),
	Level_Six(6),
	Level_Seven(7),
	Level_Eight(8),
	Level_Nine(9)
	;

	private int value;

	private TitleLevel(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	private static final Map<Integer, TitleLevel> toEnumMap = new LinkedHashMap<Integer, TitleLevel>();

	static {
		for (TitleLevel type : values()) {
			toEnumMap.put(type.getValue(), type);
		}
	}

	public static TitleLevel toEnum(int type) {
		if (toEnumMap.containsKey(type))
			return toEnumMap.get(type);
		return TitleLevel.Null;
	}
}
