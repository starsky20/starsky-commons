//package com.starsky.common.utils.file;
//
//import com.github.tobato.fastdfs.domain.MetaData;
//import com.github.tobato.fastdfs.domain.StorePath;
//import com.github.tobato.fastdfs.exception.FdfsUnsupportStorePathException;
//import com.github.tobato.fastdfs.proto.storage.DownloadByteArray;
//import com.github.tobato.fastdfs.service.FastFileStorageClient;
//import org.apache.commons.io.FilenameUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.*;
//import java.nio.charset.Charset;
//import java.util.Set;
//
///**
// * @author wansh
// * @version 1.0
// * @desc FastDfs工具类
// * @date 2021/6/14 17:04
// * @email 1057718341@qq.com
// */
//@Component
//public class FastDfsUtil {
//
//    private final Logger logger = LoggerFactory.getLogger(FastDfsUtil.class);
//
//    @Autowired
//    private FastFileStorageClient storageClient;
//
//
//    /**
//     * MultipartFile类型的文件上传ַ
//     *
//     * @param file
//     * @return
//     * @throws IOException
//     */
//    public String uploadFile(MultipartFile file) throws IOException {
//        StorePath storePath = storageClient.uploadFile(file.getInputStream(), file.getSize(),
//                FilenameUtils.getExtension(file.getOriginalFilename()), null);
//        return getResAccessUrl(storePath);
//    }
//
//    /**
//     * 普通的文件上传
//     *
//     * @param file
//     * @return
//     * @throws IOException
//     */
//    public String uploadFile(File file) throws IOException {
//        FileInputStream inputStream = new FileInputStream(file);
//        StorePath path = storageClient.uploadFile(inputStream, file.length(),
//                FilenameUtils.getExtension(file.getName()), null);
//        return getResAccessUrl(path);
//    }
//
//    /**
//     * 带输入流形式的文件上传
//     *
//     * @param is
//     * @param size
//     * @param fileName
//     * @return
//     */
//    public String uploadFileStream(InputStream is, long size, String fileName) {
//        StorePath path = storageClient.uploadFile(is, size, fileName, null);
//        return getResAccessUrl(path);
//    }
//
//    /**
//     * 将一段文本文件写到fastdfs的服务器上
//     *
//     * @param content
//     * @param fileExtension
//     * @return
//     */
//    public String uploadFile(String content, String fileExtension) {
//        byte[] buff = content.getBytes(Charset.forName("UTF-8"));
//        ByteArrayInputStream stream = new ByteArrayInputStream(buff);
//        StorePath path = storageClient.uploadFile(stream, buff.length, fileExtension, null);
//        return getResAccessUrl(path);
//    }
//
//    /**
//     * 返回文件上传成功后的地址名称ַ
//     *
//     * @param storePath
//     * @return
//     */
//    private String getResAccessUrl(StorePath storePath) {
//        String fileUrl = storePath.getFullPath();
//        return fileUrl;
//    }
//
//    /**
//     * 删除文件
//     *
//     * @param filePath
//     */
//    public void deleteFile(String filePath) {
//        if (StringUtils.isEmpty(filePath)) {
//            return;
//        }
//        try {
//            StorePath storePath = StorePath.praseFromUrl(filePath);
//            storageClient.deleteFile(storePath.getGroup(), storePath.getPath());
//        } catch (FdfsUnsupportStorePathException e) {
//            logger.warn(e.getMessage());
//        }
//    }
//
//    /**
//     * 上传图片
//     *
//     * @param is
//     * @param size
//     * @param fileExtName
//     * @param metaDataSet
//     * @return
//     */
//    public String upfileImage(InputStream is, long size, String fileExtName, Set<MetaData> metaDataSet) {
//        StorePath path = storageClient.uploadImageAndCrtThumbImage(is, size, fileExtName, metaDataSet);
//        return getResAccessUrl(path);
//    }
//
//    /**
//     * 文件下载
//     *
//     * @param srcFilePath 源文件路径
//     * @param group       分组名称
//     * @param outFilePath 下载后文件存储路径
//     */
//    public void download(String srcFilePath, String group, String outFilePath) {
//        // 进行文件下载
//        byte[] buffer = storageClient.downloadFile(group, srcFilePath, new DownloadByteArray());
//        // 创建输出文件源
//        File target = new File(outFilePath);
//        OutputStream os = null;
//        try {
//            // 获取文件输出字节流
//            os = new FileOutputStream(target);
//            // 将字节数组内容写入文件源
//            os.write(buffer);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                // 关闭流资源
//                if (os != null) {
//                    os.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 文件下载
//     */
//    public void download(String fullPath, String outFilePath) {
//        // 文件访问地址
////        String fullPath = "group1/M00/00/00/eccnMl9PT9uAUK9VAAB1IQneShA844.jpg";
//        // 分离文件分组
//        String group = fullPath.substring(0, fullPath.indexOf("/"));
//        // 分离文件路径
//        String path = fullPath.substring(fullPath.indexOf("/") + 1);
//        // 进行文件下载
//        byte[] buffer = storageClient.downloadFile(group, path, new DownloadByteArray());
//        // 创建输出文件源
////        File target = new File("D://util", "target" + fullPath.substring(fullPath.indexOf(".")));
//        download(path, group, outFilePath);
//    }
//
//
//}