package com.starsky.common.utils.word;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO word内容
 */
public class WordContent {
	
	/**
	 * 文件描述
	 */
	private String fileDesc;
	/**
	 * 文件集
	 */
	private Map<String, File> map = new LinkedHashMap<String, File>();
	/**
	 * 表格描述信息
	 */
	private TableDesc tableDesc;
	/**
	 * 表格内容
	 */
	private Map<TableDesc, List<?>> tableInfo = new LinkedHashMap<TableDesc, List<?>>();
	/**
	 * 文本内容
	 */
	private List<String> textInfo;
	/**
	 * 内容类型
	 */
	private ContentType contentType;
	
	public String getFileDesc() {
		return fileDesc;
	}
	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}
	public Map<String, File> getMap() {
		return map;
	}
	public void setMap(Map<String, File> map) {
		this.map = map;
	}
	public TableDesc getTableDesc() {
		return tableDesc;
	}
	public void setTableDesc(TableDesc tableDesc) {
		this.tableDesc = tableDesc;
	}
	public Map<TableDesc, List<?>> getTableInfo() {
		return tableInfo;
	}
	public void setTableInfo(Map<TableDesc, List<?>> tableInfo) {
		this.tableInfo = tableInfo;
	}
	public List<String> getTextInfo() {
		return textInfo;
	}
	public void setTextInfo(List<String> textInfo) {
		this.textInfo = textInfo;
	}
	public ContentType getContentType() {
		return contentType;
	}
	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
	
}
