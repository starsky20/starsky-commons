package com.starsky.common.page;

import io.swagger.annotations.ApiModelProperty;

/**
 * @desc:分页基本参数
 * @author: wangsh
 * @time: 2020/9/28 9:01
 */
public class BaseDomain {

    public static final Integer PAGE_NO = 1;

    public static final Integer PAGE_SIZE = 10;

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer page = PAGE_NO;
//
//    /**
//     * 当前页
//     */
//    @ApiModelProperty(value = "当前页")
//    private Integer pageIndex = PAGE_NO;

    /**
     * 每页记录数
     */
    @ApiModelProperty(value = "每页记录数")
    private Integer limit = PAGE_SIZE;

//    /**
//     * 每页记录数
//     */
//    @ApiModelProperty(value = "每页记录数")
//    private Integer pageSize = PAGE_SIZE;

    /**
     * 起始位置
     */
    @ApiModelProperty(value = "起始位置", hidden = true)
    private Integer startIndex;

    /**
     * 排序字段
     */
    @ApiModelProperty(value = "排序字段")
    private String orderField;

    /**
     * 排序方式，ASC/DESC
     */
    @ApiModelProperty(value = "排序方式，ASC/DESC")
    private String order = "DESC";


    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        if (startIndex == null || startIndex <= 0) {
            startIndex = 0;
        }
        this.startIndex = startIndex;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        if (page == null || page <= 0) {
            page = PAGE_NO;
        }
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        if (limit == null || limit <= 0) {
            this.limit = PAGE_SIZE;
        }
        this.limit = limit;
    }

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

//    public Integer getPageIndex() {
//        return pageIndex;
//    }
//
//    public void setPageIndex(Integer pageIndex) {
//        if (pageIndex == null || pageIndex <= 0) {
//            pageIndex = PAGE_NO;
//        }
//        this.pageIndex = pageIndex;
//    }
//
//    public Integer getPageSize() {
//        return pageSize;
//    }
//
//    public void setPageSize(Integer pageSize) {
//        this.pageSize = pageSize;
//    }
}
