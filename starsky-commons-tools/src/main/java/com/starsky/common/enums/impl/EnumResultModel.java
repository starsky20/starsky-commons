package com.starsky.common.enums.impl;

import com.starsky.common.enums.EnumInfoType;

/**
 * @Description TODO 返回信息枚举实现
 */
public enum EnumResultModel implements EnumInfoType {
    /**
     * 成功
     */
    SUCCESS("200", "成功"),
    FAILED("500","系统错误");
    /********************************************
            失败或处理中自己定义
    ********************************************/

    private String code;
    private String msg;

    EnumResultModel(String code, String msg) {
        this.setCode(code);
        this.setMsg(msg);
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "[" + this.code + ":" + this.msg + "]";
    }
}
