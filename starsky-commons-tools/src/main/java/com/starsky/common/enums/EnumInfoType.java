package com.starsky.common.enums;

/**
 * @Description TODO 枚举接口
 */
public interface EnumInfoType {

	String getCode();
	String getMsg();

}
