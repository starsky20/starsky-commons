package com.starsky.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@ConfigurationProperties(prefix = "swagger")
@Configuration
public class SwaggerProperties {

    //swagger host信息
    private String host;
    //是否开启swagger(生产环境则设置为false)
    private Boolean enabled;
    //是否携带token
    private Boolean enableToken;
    //ApiKey请求头名称(如果enable-token设置为true,则api-key-name必填)
    private String apiKeyName;
    //通过注释进行扫描接口
    private String withMethodAnnotation;
    //通过包名进行扫描接口
    private String basePackage;
    //标题
    private String title;
    //描述
    private String description;
    //访问地址
    private String termsOfServiceUrl;
    //联系人
    private String contact;
    //版本
    private String version;

}
