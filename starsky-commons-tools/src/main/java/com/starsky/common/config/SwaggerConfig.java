package com.starsky.common.config;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.*;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 *
 *  o.s.web.servlet.PageNotFound: No mapping for POST /app/tMemeber/login
 *  解决办法：将WebMvcConfigurerAdapter  替换为 WebMvcConfigurer
 *
 *  spring 5抛弃了WebMvcConfigurerAdapter  使用 WebMvcConfigurationSupport 代替了它
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {
    @Autowired
    private SwaggerProperties swaggerProperties;
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
            "classpath:/META-INF/resources/", "classpath:/resources/",
            "classpath:/static/", "classpath:/public/",
            "/","/css/**","/js/**","/img/**","/media/**","/vendors/**" };

    @Bean
    public Docket createRestApi() throws Exception {
        ApiSelectorBuilder builder = new Docket(DocumentationType.SWAGGER_2)
                .enable(swaggerProperties.getEnabled())
//                .host("8.210.248.8:8080")
                .apiInfo(apiInfo())
                .select()
                .paths(PathSelectors.any());
        //加了ApiOperation注解的类，才生成接口文档
        String methodAnnotation = swaggerProperties.getWithMethodAnnotation();
        if (StringUtils.isNotBlank(methodAnnotation)) {
            Class cls = Class.forName(swaggerProperties.getWithMethodAnnotation());
            builder.apis(RequestHandlerSelectors.withMethodAnnotation(cls));
        } else {
            //包下的类，才生成接口文档
            String basePackage = swaggerProperties.getBasePackage();
            if (StringUtils.isNotBlank(basePackage)) {
                builder.apis(RequestHandlerSelectors.basePackage(basePackage));
            }
        }
        //全局配置
        Docket docket = builder.build();
        docket.securitySchemes(security());
//        docket.globalOperationParameters(getParameters());
        return docket;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerProperties.getTitle())
                .description(swaggerProperties.getDescription())
                .termsOfServiceUrl(swaggerProperties.getTermsOfServiceUrl())
                .contact(swaggerProperties.getContact())
                .version(swaggerProperties.getVersion())
                .build();
    }

    private List<ApiKey> security() {
        String apiKeyName = swaggerProperties.getApiKeyName();
        return newArrayList(
                new ApiKey(apiKeyName, apiKeyName, "header")
        );
    }

    /**
     * 设置请求头token
     *
     * @return
     */
    private List<Parameter> getParameters() {
        String apiKeyName = swaggerProperties.getApiKeyName();
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder
                .parameterType("header")
                .name(apiKeyName)
                .description(apiKeyName)
                .modelRef(new ModelRef("string"))
                .required(false).build();
        List<Parameter> aParameters = new ArrayList<Parameter>();
        aParameters.add(aParameterBuilder.build());
        return aParameters;
    }

    /**
     * 访问静态资源
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        /**
         *  对根目录和静态文件不需要进行拦截，如果对根目录（即登录页面）进行拦截，将会导致循环重定向
         * SpringBoot自动配置本身并不会把/swagger-ui.html
         * 这个路径映射到对应的目录META-INF/resources/下面
         * 采用WebMvcConfigurerAdapter将swagger的静态文件进行发布;
         */
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        //将所有/static/** 访问都映射到classpath:/static/ 目录下
        registry.addResourceHandler("/static/**")
                .addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX + "/static/");
        if (!registry.hasMappingForPattern("/**")) {
            registry.addResourceHandler("/**")
                    .addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
        }
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /*
         *  对根目录和静态文件不需要进行拦截，如果对根目录（即登录页面）进行拦截，将会导致循环重定向
         */
//        registry.addInterceptor(拦截器实现类).addPathPatterns("/**").excludePathPatterns(EXCLUDE_PATH);
    }
}
