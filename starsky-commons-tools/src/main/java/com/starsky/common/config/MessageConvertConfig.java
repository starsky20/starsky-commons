package com.starsky.common.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 基于fastjson的日期转换器,将返回值日期类型进行格式化处理，
 * 防止在字符串转为日期类型报错
 */
@Configuration
//@Scope("prototype")
public class MessageConvertConfig extends FastJsonHttpMessageConverter {
    /**
     * 相应结果时，使用fastjson进行日期格式化处理
     */
    @Override
    protected void writeInternal(Object obj, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        String json = JSON.toJSONStringWithDateFormat(obj,
                "yyyy-MM-dd HH:mm:ss",
                SerializerFeature.WriteDateUseDateFormat,
                SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteNullNumberAsZero,
                SerializerFeature.WriteNullListAsEmpty,
                SerializerFeature.WriteNullStringAsEmpty,
                SerializerFeature.WriteNullBooleanAsFalse);
        OutputStream out = outputMessage.getBody();
        byte[] bytes = json.getBytes();
        out.write(bytes);
    }

}
