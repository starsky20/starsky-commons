package com.starsky.common.constant;

public enum ErrorListEnum {
    OK(0,"OK"),
    E200(200,"成功!"),
    
    E10000(10000, "未知错误"),
    E10001(10001,"数据库执行异常"),
    E10002(10002,"必填项不能为空"),
    E10003(10003,"修改信息失败"),
    E10004(10004,"待删除的ID不能为空"),
    E10005(10005,"查询的数据不存在"),
    E10006(10006,"存在相同记录"),
    E10007(10007,"设备已经到最大数量"),
    E10008(10008,"类型转换异常"),
    
	E20001(20001,"系统未注册，不允许访问!"),
	E20002(20002,"服务访问的参数存在被篡改的情况，不允许访问!"),
	E30001(30001,"请求参数错误,必传的参数未传递!"),
	E20003(20003,"验证码失效!"),
	E20004(20004,"验证码错误!"),
	E20005(20005,"密码错误!"),
	E20006(20006,"信息不匹配!"),
	E20007(20007,"状态已变更!"),
	E50001(50001,"服务请求报错!"),
	E50002(50002,"发送短信失败!"),
	E60001(60001,"存在相同数据!"),
	E20010(20010,"删除失败!"),
	E30002(30002,"添加异常!"),
	E30003(30003,"查询异常!"),
	E20008(20008,"账号未被审核!"),
	E20009(20009,"验证不通过!"),
	
	E99999(99999,"session invalid"),   
    E99998(99998,"用户身份不正确"),
    E99997(99997,"文件上传失败，请检查网络或联系管理员"),
    
    E404(404,"请求的信息不存在"),
	;
	
	ErrorListEnum(int key, String value){
		this.key = key;
		this.value =  value;
	}
	
	private final int key;
	
	private final String value;

	public int getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
	// 普通方法
    public static String getValue(int key) {
        for (ErrorListEnum e : ErrorListEnum.values()) {
            if (e.getKey() == key) {
                return e.value;
            }
        }
        return null;
    }

	public static void main(String[] args) {
		System.out.println(ErrorListEnum.E20001.getKey() + ErrorListEnum.E20001.getValue());
		System.out.println(ErrorListEnum.getValue(ErrorListEnum.E20001.getKey()));
	}	
	
}

