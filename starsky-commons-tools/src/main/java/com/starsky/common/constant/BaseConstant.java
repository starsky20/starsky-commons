package com.starsky.common.constant;

/**
 * @Description 存放全局变量
 */
public abstract class BaseConstant {

    // 重置用户默认密码
    public static String DEFAULT_PWD = "a123456";
    // 签名校验是否开启
    public static boolean CHK_SIGN_SWITCH = true;
    // 电子签名Key
    public static String ELEC_SIGN = "wbsElecSign";
    // 密码错误限制次数
    public static Integer PWD_ERROR_NUM = 5;
    // 默认用户
    public static String DEFAULT_USER = "sys";
    // 最大访问次数限制失效时间
    public static final Long LIMIT_IP_REQ_TIME = 60L;
    // 超级管理员
    public static String SUPER_USER = "admin";
    // 最大访问次数前缀
    public static String LIMIT_IP_REQ_PRE = "LimitIpRequest";
    // 渠道接入资源前缀
    public static String CUT_RES_PRE = "CutRes";
    // 登录信息前缀
    public static String LOGIN_INFO_PRE = "Login";
    // 系统前缀
    public static String SYS_INFO_PRE = "Sys";
    // 资源前缀
    public static String RES_INFO_PRE = "Res";

}
