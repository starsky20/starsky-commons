package com.starsky.common.constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangsh
 * @date 2019/8/1 8:48
 * @Description 数据库枚举类
 */
public enum DataBaseEnum {

    //    第一季度
    ONE("01", "01"),
    TWO("02", "01"),
    THREE("03", "01"),

    //    第二季度
    FOUR("04", "02"),
    FIVE("05", "02"),
    SIX("06", "02"),

    //    第三季度
    SEVEN("07", "03"),
    EIGHT("08", "03"),
    NINE("09", "03"),

    //    第四季度
    TEN("10", "04"),
    ELEVEN("11", "04"),
    TWELVE("12", "04");

    private String key;

    private String value;

    private Map<String, String> map = new HashMap();

    DataBaseEnum(String key, String value) {
        this.key = key;
        this.value = value;
        map.put(key, value);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    /**
     * 获取表名称后缀
     *
     * @param date
     * @return
     */
    public static String getTableSubffix(Date date) {

        SimpleDateFormat format = new SimpleDateFormat("MM");
        String tableSubffix = format.format(date);
        return tableSubffix;
    }

    /**
     * 获取表名称后缀
     *
     * @param time
     * @return
     */
    public static String getTableSubffix(String time) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(time);
        String tableSubffix = getTableSubffix(date);
        return tableSubffix;
    }

}
