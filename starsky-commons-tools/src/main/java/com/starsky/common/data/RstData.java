package com.starsky.common.data;

import com.starsky.common.enums.EnumInfoType;
import com.starsky.common.enums.impl.EnumResult;
import com.starsky.common.exception.ExceptionInfoType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: fengyuan
 * @date 2018年9月26日 18:13:24
 * @Description TODO 公共返回对象
 */
@Slf4j
@ApiModel(description = "公共返回对象")
public class RstData<T> implements Serializable {

    @ApiModelProperty(value = "状态码标识", dataType = "String")
    private String code;

    @ApiModelProperty(value = "业务信息", dataType = "String")
    private String msg;

    @ApiModelProperty(value = "结果集(泛型)", required = true, dataType = "object")
    private T result;

    @ApiModelProperty(value = "返回当前时间戳", required = true, dataType = "Long")
    private Long ts;

    public T getResult() {
        return result == null ? (T) new HashMap<String, Object>() : result;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = System.currentTimeMillis();
    }

    public RstData() {
    }

    public static RstData successMsg(String msg) {
        RstData rstData = new RstData();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
        rstData.setTs(System.currentTimeMillis());
        return rstData;
    }

    public static RstData successMsg(String msg, Object obj) {
        RstData rstData = new RstData();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
        rstData.setTs(System.currentTimeMillis());
        rstData.setResult(obj);
        return rstData;
    }

    public static RstData successMsg(String msg, String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        RstData rstData = new RstData();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
        rstData.setTs(System.currentTimeMillis());
        rstData.setResult(map);
        return rstData;
    }

    public static RstData success() {
        return successMsg(EnumResult.SUCCESS.getMsg());
    }

    public static <T> RstData<T> success(T obj) {
        return successMsg(EnumResult.SUCCESS.getMsg(), obj);
    }

    public static RstData success(String key, Object value) {
        return successMsg(EnumResult.SUCCESS.getMsg(), key, value);
    }

    //==============================================================================
    public static RstData faild(String code, String msg) {
        RstData rstData = new RstData();
        rstData.setCode(code);
        rstData.setMsg(msg);
        rstData.setTs(System.currentTimeMillis());
        return rstData;
    }

    public static RstData faild(EnumInfoType eit) {
        return faild(eit.getCode(), eit.getMsg());
    }

    public static RstData faild(EnumInfoType er, String msg) {
        if (StringUtils.isEmpty(msg)) {
            return faild(er.getCode(), er.getMsg());
        } else {
            return faild(er.getCode(), msg);
        }
    }

    public static RstData faild(ExceptionInfoType eit) {
        return faild(eit.getCode(), eit.getMsg());
    }

    public static RstData faild(String msg) {
        return faild(EnumResult.FAILED, msg);
    }

    @Override
    public String toString() {
        return "RstData{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", result=" + result +
                ", ts=" + ts +
                '}';
    }

}
