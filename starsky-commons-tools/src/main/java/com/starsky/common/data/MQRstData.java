package com.starsky.common.data;

import com.starsky.common.enums.EnumInfoType;
import com.starsky.common.exception.ExceptionInfoType;
import com.starsky.common.enums.impl.EnumResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 消息队列传输公共类
 */
@Data
@Slf4j
@ApiModel(description = "公共返回对象")
public class MQRstData<T> implements Serializable {

    @ApiModelProperty(value = "发送类型）", dataType = "String")
    private Integer type;

    @ApiModelProperty(value = "操作类型（1-新增、2-修改、3-删除、4-查询等）", dataType = "String")
    private Integer opertionType;

    @ApiModelProperty(value = "状态码标识", dataType = "String")
    private String code;

    @ApiModelProperty(value = "业务信息", dataType = "String")
    private String msg;

    @ApiModelProperty(value = "结果集(泛型)", required = true, dataType = "object")
    private T result;

    @ApiModelProperty(value = "表名称后缀", dataType = "String")
    private String tableSubfix;

    public T getResult() {
        return result == null ? (T) new HashMap<String, Object>() : result;
    }

    private MQRstData() {
    }

    public static MQRstData getInstance() {
        MQRstData rstData = new MQRstData();
        return rstData;
    }

    //==============================================================================
    public static MQRstData successMsg(String msg) {
        MQRstData rstData = getInstance();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
//        log.debug("[成功 rstData：{}]", rstData);
        return rstData;
    }

    public static MQRstData successMsg(String msg, Object obj) {
        MQRstData rstData = getInstance();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
        rstData.setResult(obj);
//        log.debug("[成功 rstData：{}]", rstData);
        return rstData;
    }

    public static MQRstData successMsg(String msg, String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        MQRstData rstData = getInstance();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
        rstData.setResult(map);
//        log.debug("[成功 rstData：{}]", rstData);
        return rstData;
    }

    public MQRstData(Integer type, Integer opertionType, String code, String msg, T result, String tableSubfix) {
        this.type = type;
        this.opertionType = opertionType;
        this.code = code;
        this.msg = msg;
        this.result = result;
        this.tableSubfix = tableSubfix;
    }

    public MQRstData(Integer type, Integer opertionType, T result, String tableSubfix) {
        this.type = type;
        this.opertionType = opertionType;
        this.code = code;
        this.msg = msg;
        this.result = result;
        this.tableSubfix = tableSubfix;
    }

    public static MQRstData success() {
        return successMsg(EnumResult.SUCCESS.getMsg());
    }

    public static MQRstData success(Object obj) {
        return successMsg(EnumResult.SUCCESS.getMsg(), obj);
    }

    public static MQRstData success(String key, Object value) {
        return successMsg(EnumResult.SUCCESS.getMsg(), key, value);
    }

    //==============================================================================
    private static MQRstData faild(String code, String msg) {
        MQRstData rstData = new MQRstData();
        rstData.setCode(code);
        rstData.setMsg(msg);
//        log.debug("[失败：{}]", rstData);
        return rstData;
    }

    public static MQRstData faild(EnumInfoType eit) {
        return faild(eit.getCode(), eit.getMsg());
    }

    public static MQRstData faild(EnumInfoType er, String msg) {
        return faild(er.getCode(), er.getMsg() + "，具体原因：" + msg);
    }

    public static MQRstData faild(ExceptionInfoType eit) {
        return faild(eit.getCode(), eit.getMsg());
    }

    public static MQRstData faild(String msg) {
        return faild(EnumResult.FAILED, msg);
    }
    //==============================================================================


    @Override
    public String toString() {
        return "MQRstData{" +
                "type=" + type +
                ", opertionType=" + opertionType +
                ", code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", result=" + result +
                ", tableSubfix='" + tableSubfix + '\'' +
                '}';
    }
}
