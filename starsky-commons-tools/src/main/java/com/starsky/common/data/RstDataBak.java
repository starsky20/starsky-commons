package com.starsky.common.data;

import com.starsky.common.enums.EnumInfoType;
import com.starsky.common.enums.impl.EnumResult;
import com.starsky.common.exception.ExceptionInfoType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: fengyuan
 * @date 2018年9月26日 18:13:24
 * @Description TODO 公共返回对象
 */
@Slf4j
@ApiModel(description = "公共返回对象")
public class RstDataBak<T> implements Serializable {

    @ApiModelProperty(value = "状态码标识", dataType = "String")
    private String code;

    @ApiModelProperty(value = "业务信息", dataType = "String")
    private String msg;

    @ApiModelProperty(value = "结果集(泛型)", required = true, dataType = "object")
    private T list;

    @ApiModelProperty(value = "返回当前时间戳", required = true, dataType = "Long")
    private Long  ts;

    public T getList() {
        return list == null ? (T) new HashMap<String, Object>() : list;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setList(T result) {
        this.list = list;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = System.currentTimeMillis();
    }

    public RstDataBak() {
    }

    public static RstDataBak successMsg(String msg) {
        RstDataBak rstData = new RstDataBak();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
        rstData.setTs(System.currentTimeMillis());
        return rstData;
    }

    public static RstDataBak successMsg(String msg, Object obj) {
        RstDataBak rstData = new RstDataBak();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
        rstData.setTs(System.currentTimeMillis());
        rstData.setList(obj);
        return rstData;
    }

    public static RstDataBak successMsg(String msg, String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        RstDataBak rstData = new RstDataBak();
        rstData.setCode(EnumResult.SUCCESS.getCode());
        rstData.setMsg(msg);
        rstData.setTs(System.currentTimeMillis());
        rstData.setList(map);
        return rstData;
    }

    public static RstDataBak success() {
        return successMsg(EnumResult.SUCCESS.getMsg());
    }

    public static <T> RstDataBak<T> success(T obj) {
        return successMsg(EnumResult.SUCCESS.getMsg(), obj);
    }

    public static RstDataBak success(String key, Object value) {
        return successMsg(EnumResult.SUCCESS.getMsg(), key, value);
    }

    //==============================================================================
    private static RstDataBak faild(String code, String msg) {
        RstDataBak rstData = new RstDataBak();
        rstData.setCode(code);
        rstData.setMsg(msg);
        rstData.setTs(System.currentTimeMillis());
        return rstData;
    }

    public static RstDataBak faild(EnumInfoType eit) {
        return faild(eit.getCode(), eit.getMsg());
    }

    public static RstDataBak faild(EnumInfoType er, String msg) {
        if (StringUtils.isEmpty(msg)) {
            return faild(er.getCode(), er.getMsg());
        } else {
            return faild(er.getCode(), msg);
        }
    }

    public static RstDataBak faild(ExceptionInfoType eit) {
        return faild(eit.getCode(), eit.getMsg());
    }

    public static RstDataBak faild(String msg) {
        return faild(EnumResult.FAILED, msg);
    }

}
