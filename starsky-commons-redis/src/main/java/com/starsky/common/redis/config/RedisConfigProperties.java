package com.starsky.common.redis.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/** redis配置实体 */
@Data
@Configuration
@ConfigurationProperties(prefix = "spring.redis")
public class RedisConfigProperties {

    @ApiModelProperty(value = "服务器地址", dataType = "String")
    private String host;
    @ApiModelProperty(value = "服务器连接端口", dataType = "String")
    private Integer port;
    @ApiModelProperty(value = "服务器连接密码", dataType = "String")
    private String password;
    @ApiModelProperty(value = "数据库索引", dataType = "Integer")
    private Integer database = 0;
    @ApiModelProperty(value = "连接超时时间（毫秒）", dataType = "Integer")
    private Integer timeout = 3000;
    @ApiModelProperty(value = "在空闲时检查有效性", dataType = "Integer")
    private Boolean testOnBorrow = false;
    @ApiModelProperty(value = "连接池最大连接数（使用负值表示没有限制）", dataType = "Integer")
    private Integer maxActive = 200;
    @ApiModelProperty(value = "连接池中的最大空闲连接", dataType = "Integer")
    private Integer maxIdle = 20;
    @ApiModelProperty(value = "连接池中的最小空闲连接", dataType = "Integer")
    private Integer minIdle = 5;
    @ApiModelProperty(value = "连接池最大阻塞等待时间（使用负值表示没有限制）", dataType = "Integer")
    private Integer maxWait = -1;

}
