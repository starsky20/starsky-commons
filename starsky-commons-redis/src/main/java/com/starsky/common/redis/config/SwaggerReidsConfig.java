//package com.starsky.common.redis.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.service.ApiKey;
//import springfox.documentation.service.AuthorizationScope;
//import springfox.documentation.service.SecurityReference;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spi.service.contexts.SecurityContext;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@EnableSwagger2
//@Configuration
//public class SwaggerReidsConfig {
//
//    @Configuration
//    @EnableSwagger2
//    @ConditionalOnProperty(name = "swagger.enable",havingValue = "true",matchIfMissing = true)
//    public static class SwaggerReids{
//
//        @Value("${swagger.contact}")
//        private String contact;
//        @Value("${swagger.package}")
//        private String swgpackage;
//
//        @Bean
//        @ConditionalOnMissingBean(Docket.class)
//        public Docket createRestApi() {
//            return new Docket(DocumentationType.SWAGGER_2)
//                    .apiInfo(apiInfo())
//                    .select()
//                    .apis(RequestHandlerSelectors.basePackage(swgpackage))
//                    .paths(PathSelectors.any())
//                    .build();
//        }
//
//        private ApiInfo apiInfo() {
//            return new ApiInfoBuilder()
//                    .title("API接口说明文档")
//                    .description("restful 风格接口")
//                    .version("1.0")
//                    .build();
//        }
//    }
//
//
//    /* 安全模式，这里指定token通过Authorization头请求头传递 */
//    public static List<ApiKey> securitySchemes() {
//        List<ApiKey> apiKeyList = new ArrayList<>();
//        apiKeyList.add(new ApiKey(SpringSessionConfig.SESSION_ID_HEADER, SpringSessionConfig.SESSION_ID_HEADER, "header"));
//        return apiKeyList;
//    }
//
//    /* 安全上下文 */
//    public static List<SecurityContext> securityContexts() {
//        List<SecurityContext> securityContexts = new ArrayList<>();
//        securityContexts.add(
//                SecurityContext.builder()
//                        .securityReferences(defaultAuth())
//                        .forPaths(PathSelectors.any())
//                        .build());
//        return securityContexts;
//    }
//
//    /* 默认的安全饮用 */
//    public static List<SecurityReference> defaultAuth() {
//        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
//        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
//        authorizationScopes[0] = authorizationScope;
//        List<SecurityReference> securityReferences = new ArrayList<>();
//        securityReferences.add(new SecurityReference(SpringSessionConfig.SESSION_ID_HEADER, authorizationScopes));
//        return securityReferences;
//    }
//
//}
