package com.starsky.common.redis.utils;

public class RedisKeys {
	/**
	 * 登录用户Key
	 */
	public static String getSecurityUserKey(Long id) {
		return "sys_" + id;
	}
	/**
	 * 登录用户Key
	 */
	public static String getSecurityUserKey(String token) {
		return "user_" + token;
	}
}
