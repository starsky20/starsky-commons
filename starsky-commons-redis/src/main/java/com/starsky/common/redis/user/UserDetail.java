package com.starsky.common.redis.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wangsh
 * @email 1057718341@qq.com
 * @date 2021-04-11 22:58:13
 */
@Data
@ApiModel(value = "t_member ")
public class UserDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id", name = "id", dataType = "Long")
    private Long id;
    @ApiModelProperty(value = "用户部门id", name = "deptId", dataType = "Long")
    private Long deptId;
    @ApiModelProperty(value = "是否管理员", name = "superAdmin", dataType = "Long")
    private Integer superAdmin;
    @ApiModelProperty(value = "用户名称", name = "userName", dataType = "String")
    private String userName;
    @ApiModelProperty(value = "用户真实名称", name = "realName", dataType = "String")
    private String realName;
    @ApiModelProperty(value = "用户密码", name = "password", dataType = "String")
    private String password;
    @ApiModelProperty(value = "", name = "headUrl", dataType = "String")
    private String headUrl;
    @ApiModelProperty(value = "手机号", name = "phone", dataType = "String")
    private String phone;
    @ApiModelProperty(value = "手机号", name = "mobile", dataType = "String")
    private String mobile;
    @ApiModelProperty(value = "邮箱", name = "mail", dataType = "String")
    private String mail;
    @ApiModelProperty(value = "用户类型", name = "type", dataType = "String")
    private String type;
    @ApiModelProperty(value = "性别", name = "sex", dataType = "String")
    private String sex;
    @ApiModelProperty(value = "生日", name = "birth", dataType = "String")
    private String birth;
    @ApiModelProperty(value = "身份证号", name = "cardNo", dataType = "String")
    private String cardNo;
    @ApiModelProperty(value = "身份住址", name = "address", dataType = "String")
    private String address;
    @ApiModelProperty(value = "加密颜值", name = "salt", dataType = "String")
    private String salt;
    @ApiModelProperty(value = "区号", name = "areaCode", dataType = "String")
    private String areaCode;
    @ApiModelProperty(value = "", name = "userStatus", dataType = "String")
    private String status;
    @ApiModelProperty(value = "用户token", name = "token", dataType = "String")
    private String token;

}
