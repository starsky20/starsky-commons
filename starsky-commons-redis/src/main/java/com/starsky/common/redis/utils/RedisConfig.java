//package com.starsky.common.redis.utils;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//
//import javax.annotation.Resource;
//
///**
// * Redis配置
// */
//@Configuration
//public class RedisConfig {
//    @Resource
//    private RedisConnectionFactory factory;
//
//    /**
//     * 设置自定义序列化
//     *
//     * @return
//     */
//    @Bean
//    public RedisTemplate<String, Object> redisTemplate() {
//        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
//        redisTemplate.setKeySerializer(new StringRedisSerializer());
//        redisTemplate.setValueSerializer(new JsonRedisSerializer<>(Object.class));
//        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
//        redisTemplate.setHashValueSerializer(new JsonRedisSerializer<>(Object.class));
//        redisTemplate.setConnectionFactory(factory);
//
//        return redisTemplate;
//    }
//}
