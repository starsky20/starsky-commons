package com.starsky.common.redis.user;

import com.starsky.common.redis.utils.RedisKeys;
import com.starsky.common.redis.utils.RedisUtils;
import com.starsky.common.utils.HttpContextUtils;
import com.starsky.common.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户
 */
@Slf4j
public class SecurityUser {
    private static RedisUtils redisUtils;
    private static String USER_KEY = "userId";
    private static String TOKEN_HEADER = "token";
    private static Long EXPIRE = 3600L;

    static {
        redisUtils = SpringContextUtils.getBean(RedisUtils.class);
    }

    /**
     * 缓存用户信息
     *
     * @param user
     */
    public static void set(UserDetail user) {
        if (user == null) {
            return;
        }
        set(user, EXPIRE);
    }

    /**
     * 缓存用户信息
     *
     * @param user
     * @param expire
     */
    public static void set(UserDetail user, Long expire) {
        if (user == null) {
            return;
        }
        //用户id作为key缓存
        String key = RedisKeys.getSecurityUserKey(user.getId());
        redisUtils.set(key, user, expire);

        //用户token缓存id
        redisUtils.set(user.getToken(), user.getId() + "", expire);

        //用户token作为key缓存
        String tokenkey = RedisKeys.getSecurityUserKey(user.getToken());
        redisUtils.set(tokenkey, user, expire);
    }

    /**
     * 根据用户id取用户信息
     *
     * @param id
     * @return
     */
    public static UserDetail get(Long id) {

        String key = RedisKeys.getSecurityUserKey(id);
        Object obj = redisUtils.get(key);
        if (obj == null) return null;
        UserDetail user = (UserDetail) obj;

        return user;
    }

    /**
     * 根据token 取用户
     *
     * @param token
     * @return
     */
    public static UserDetail get(String token) {
        String tokenkey = RedisKeys.getSecurityUserKey(token);
        Object obj = redisUtils.get(tokenkey);
        if (obj == null) return null;
        UserDetail user = (UserDetail) obj;
        return user;
    }

    /**
     * 用户退出
     *
     * @param userId 用户ID
     */
    public static void logout(Long userId) {
        String key = RedisKeys.getSecurityUserKey(userId);
        redisUtils.delete(key);
    }

    /**
     * 用户退出
     *
     * @param token 用户token
     */
    public static void logout(String token) {
        String key = RedisKeys.getSecurityUserKey(token);
        // 清空菜单导航、权限标识
        redisUtils.delete(key);
    }

    /**
     * 获取用户信息
     */
    public static UserDetail getUser() {
        UserDetail user = null;
        //根据id获取用户
        Long userId = getUserId();
        user = (userId != null) ? get(userId) : null;
        if (user != null) return user;

        //根据token获取用户
        String token = getToken();
        user = (StringUtils.isNotBlank(token)) ? get(token) : null;
        return user;
    }

    /**
     * 获取用户ID
     */
    public static Long getUserId() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (request == null) {
            return null;
        }

        String userId = request.getHeader(USER_KEY);
        if (StringUtils.isNotBlank(userId)) {
            return Long.parseLong(userId);
        }

        String token = getToken();
        if (StringUtils.isEmpty(token)) {
            log.error("token为空");
            return null;
        }

        UserDetail user = get(token);
        if (user != null) {
            return user.getId();
        }
        return null;
    }

    /**
     * 获取用户ID
     */

    public static String getToken() {
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (request == null) {
            return null;
        }
        String token = request.getHeader(TOKEN_HEADER);
        if (StringUtils.isEmpty(token)) {
            log.error("token为空");
            return null;
        }
        return token;
    }

}
