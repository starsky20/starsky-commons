//package com.starsky.common.redis.config;
//
//import com.alibaba.fastjson.support.spring.GenericFastJsonRedisSerializer;
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.config.BeanPostProcessor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.session.data.redis.RedisOperationsSessionRepository;
//import org.springframework.session.web.http.HeaderHttpSessionIdResolver;
//import org.springframework.session.web.http.HttpSessionIdResolver;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.service.ApiKey;
//import springfox.documentation.service.AuthorizationScope;
//import springfox.documentation.service.SecurityReference;
//import springfox.documentation.spi.service.contexts.SecurityContext;
//
///** http session配置 */
//@Configuration
//public class SpringSessionConfig implements BeanPostProcessor {
//
//    /** session id 通过它来传递 */
//    public static final String SESSION_ID_HEADER = "X-Auth-Token";
//
//    @Bean
//    public HttpSessionIdResolver sessionIdResolver(){
//        return new HeaderHttpSessionIdResolver(SESSION_ID_HEADER);
//    }
//
//    @Override
//    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//        /**  为spring session 存储的hash-value添加序列化器 */
//        if(bean instanceof RedisOperationsSessionRepository){
//            RedisOperationsSessionRepository sessionRepository = (RedisOperationsSessionRepository)bean;
//            RedisTemplate redisTemplate = (RedisTemplate)sessionRepository.getSessionRedisOperations();
//            redisTemplate.setHashValueSerializer(new GenericFastJsonRedisSerializer());
//        }
//        return bean;
//    }
//
//}
