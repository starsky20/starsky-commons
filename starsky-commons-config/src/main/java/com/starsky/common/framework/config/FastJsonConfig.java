package com.starsky.common.framework.config;

import com.alibaba.fastjson.parser.ParserConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FastJsonConfig {

    static {
        /** 启用自动类型支持，即在json中使用@type字段存储对象类型 */
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
    }

}
