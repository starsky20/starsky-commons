package com.starsky.common.framework.image.controller;

import com.starsky.common.data.RstData;
import com.starsky.common.utils.easypoi.ExcelTestEntity;
import com.starsky.common.utils.easypoi.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Api("excel操作")
@RestController
@RequestMapping("/excel")
public class ExcelController {

    @ApiOperation(value = "导入excel", notes = "导入excel")
    @GetMapping("importExcel")
    public RstData importExcel(@ApiIgnore @RequestParam("file") MultipartFile file) throws IOException {
        int titleRows = 1;
        int headRows = 1;
        List<ExcelTestEntity> dtos = ExcelUtils.importExcel(file, titleRows, headRows, ExcelTestEntity.class);
        return RstData.success(dtos);
    }

    @ApiOperation(value = "导出execl文件", notes = "导出execl文件")
    @GetMapping("exportExcel")
    public void exportExcel(HttpServletRequest request, HttpServletResponse response, String uuid) throws IOException {
        try {
            //导出
            List<ExcelTestEntity> list = new ArrayList<>();
            ExcelTestEntity excelTest = new ExcelTestEntity();
            excelTest.setId(10000);
            excelTest.setFileName("测试excel.xlsx");
            excelTest.setFilePath("c:\\excel");
            excelTest.setType(1);
            excelTest.setCreatorName("admin");
            excelTest.setCreateDate(new Date());
            list.add(excelTest);

            excelTest = new ExcelTestEntity();
            excelTest.setId(111111);
            excelTest.setFileName("测试20200922.xlsx");
            excelTest.setFilePath("c:\\excel");
            excelTest.setType(1);
            excelTest.setCreatorName("admin");
            excelTest.setCreateDate(new Date());
            list.add(excelTest);

            ExcelUtils.exportExcelToTarget(response, "测试excel.xlsx", list, ExcelTestEntity.class);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
