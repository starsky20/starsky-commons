package com.starsky.common.framework.config;//package com.starsky.config;
//
//import io.micrometer.core.instrument.MeterRegistry;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @desc: Prometheus 普罗米修斯监控
// * @author: wangsh
// * @time: 2020/12/4 15:37
// */
//@Configuration
//public class PrometheusConfig {
//
//    @Value("${spring.application.name}")
//    private String applicationName;
//
//    @Bean
//    MeterRegistryCustomizer<MeterRegistry> configurer() {
//        return (registry) -> registry.config().commonTags("application", applicationName);
//    }
//}
