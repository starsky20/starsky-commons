package com.starsky.common.framework.image.entity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

@Data
@Api("系统验证码实体")
public class SysCaptchaEntity {

    private String uuid;
    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码",dataType ="String")
    private String code;
    /**
     * 过期时间
     */
    @ApiModelProperty(value = "过期时间",dataType ="java.util.Date")
    private Date expireTime;

}
