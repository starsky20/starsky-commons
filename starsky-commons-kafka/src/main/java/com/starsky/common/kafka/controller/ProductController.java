package com.starsky.common.kafka.controller;

import com.starsky.common.kafka.service.KafkaProductService;
import com.starsky.common.data.RstData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/kafka")
public class ProductController {
    @Autowired
    private KafkaProductService kafkaProductService;

    @RequestMapping(value = "/send", method = RequestMethod.GET)
    public RstData sendKafka(String topic,String message) {
        try {
            log.info("kafka的消息={}", message);
            kafkaProductService.sendKafka(topic,  message);

            log.info("发送kafka成功.");
            return RstData.success("发送kafka成功");
        } catch (Exception e) {
            log.error("发送kafka失败", e);
            return RstData.faild("发送kafka失败");
        }
    }

}
