package com.starsky.common.cache.dictionary.dao;

import com.starsky.common.cache.dictionary.entity.CacheEntity;
import org.beetl.sql.core.annotatoin.SqlStatement;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @desc 查询数据字典
 * @Author wangsh
 * @return
 */
public interface CacheEntityDao extends BaseMapper<CacheEntity> {

    @SqlStatement(params = "record")
    List<Map<String, Object>> getDataFromDBSaveToTempCache(CacheEntity dto);

    /**
     * 查询订单相关分表
     *
     * @param ce
     * @return
     */
    @SqlStatement(params = "record")
    List<Map<String, Object>> getOrderList(CacheEntity ce);


}
