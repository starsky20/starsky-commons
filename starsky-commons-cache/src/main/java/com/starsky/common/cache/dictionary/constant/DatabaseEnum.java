package com.starsky.common.cache.dictionary.constant;

import org.apache.commons.lang3.StringUtils;

/**
 * @desc
 * @Author wangsh
 * @date 2018/5/6 18:35
 */
public enum DatabaseEnum {
    /*MYSQL*/
    MYSQL("mysql", "mysql"),
    //ORACLE
    ORACLE("oracle", "oracle"),
    //SQLSERVER
    SQLSERVER("sqlserver", "sqlserver"),
    // other
    other("other", "other");

    private String key;
    private String value;

    DatabaseEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String getDataValue(String key) {
        String value = null;
        if (StringUtils.isEmpty(key)) {
            return value;
        }
        DatabaseEnum[] values = DatabaseEnum.values();
        for (DatabaseEnum type : values) {
            if (type.getKey().equals(key.trim())) {
                value = type.getValue();
            }
        }
        return value;
    }


}
