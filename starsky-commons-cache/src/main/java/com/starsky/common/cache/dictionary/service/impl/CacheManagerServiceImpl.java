package com.starsky.common.cache.dictionary.service.impl;

import com.starsky.common.cache.dictionary.dao.CacheEntityDao;
import com.starsky.common.cache.dictionary.entity.CacheEntity;
import com.starsky.common.cache.dictionary.service.CacheManagerService;
import com.starsky.common.cache.dictionary.util.XmlUtils;
import com.starsky.common.utils.DateUtils;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * @desc 缓存处理类
 * @Author wangsh
 * @date 2018/5/6 18:07
 * @return
 */
@Slf4j
@Service
public class CacheManagerServiceImpl implements CacheManagerService {

    //缓存结果数据
    private static ConcurrentMap<String, CacheEntity> CACHE = new ConcurrentHashMap<>();
    //缓存表结构数据
    private static List<CacheEntity> cacheTabledebugList = new ArrayList<CacheEntity>();

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private CacheEntityDao cacheEntityDao;

    //缓存订单退单数据的时间（N天内） 配置文件配置
    @Value("${basedata.cache.timeInterval}")
    private Integer timeInterval;

    @Value("${basedata.cache.expiretime}")
    public long REDIS_DEF_EXPIRE = 60 * 60 * 72L;

    //    @Value("${datasource.type}")
    private String datasourceType;


    private void SaveToCacheManager() {

        try {
            synchronized (CacheManagerServiceImpl.class) {
                log.debug("[ 保存缓存数据start ]");
                for (CacheEntity ce : cacheTabledebugList) {
                    String key = ce.getKey();
                    log.debug("[ 写入数据到redis缓存start ：表名称：{}，数据：{} ]", key, ce);
                    //key是表名称,放入缓存,设置缓存失效时间
                    setRedisDefExpire(key, ce);
                    log.debug("[ 写入数据到redis缓存 end ：表名称：{}，数据：{} ]", key, ce);
                }
                cacheTabledebugList.clear();
                log.debug("[ 保存缓存数据 end ]");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[ 写入数据到redis缓存失败：{} ]", e);
        }
    }

    /**
     * @param @param ce
     * @return void
     * @author cjy
     * @Description: 针对配置需要独立成一个map的缓存数据进行遍历，并保存到全局map中
     * @date 2017-6-8
     */
    private void cacheDataToMap(CacheEntity ce, List<Map<String, Object>> list) {
        // 配置并遍历所有需要单独成map的字段
        String toMapFiled = ce.getToMapField();
        // 判断需要把数据独立成map的配置是否为空
        if (StringUtils.isEmpty(toMapFiled)) {
            log.debug("缓存管理-->添加缓存:" + ce.getTable() + "表没有要独立成map的字段返回!");
            return;
        }

        if (null == list || list.size() == 0) {
            log.debug("缓存管理-->添加缓存:" + ce.getTable() + "表没有查询出有效的数据返回!");
            return;
        }

        String[] fields = toMapFiled.split(",", -1);
        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];
            // 获取哪个字段需要独立成map
            String[] keys = field.split(":", -1);
            if (null == keys || keys.length != 3) {
                log.debug("缓存管理-->添加缓存:把数据添加到缓存map中出错，原因:" + ce.getTable() + "表配置的独立缓存没有配置!");
                continue;
            }
            String tmpKey = keys[0];
            String key = keys[1];
            String value = keys[2];
            log.debug("缓存管理-->添加缓存:独立缓存" + ce.getTable() + "表设置了独立map缓存，其中取独立缓存的key:" + tmpKey + " 缓存是以" + key + "字段值做key " + value + "字段值做value!");

            Map<String, Object> tmp = new HashMap<String, Object>();
            // 遍历所有的数据，并把数据添加到临时map中
            for (Map<String, Object> data : list) {
                tmp.put(data.get(key).toString(), data.get(value));
            }
            // 改组字段遍历完成，把临时map添加到全局对象中
            ce.getCacheMapData().put(tmpKey, tmp);
        }

    }

    private void getDataFromDBSaveToTempCache() {
        for (CacheEntity ce : cacheTabledebugList) {
            try {
                // 查询数据 ,封装缓存数据
                getCacheDataToDB(ce);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("缓存管理-->添加缓存:查询数据库报错", e);
            }
        }
    }

    /**
     * 解析缓存的xml文件
     */
    private void parseCacheXml() {

        log.debug("缓存管理-->解析缓存的xml文件 start ");
        try {
            String CacheXml = "cacheManage.xml";
            InputStream inputStream = CacheManagerServiceImpl.class.getClassLoader().getResourceAsStream(CacheXml);
//            String filePath = Constants.cacheXmlPath;
//            Document doc = XmlUtils.loadFileByPath(filePath);
            Document doc = XmlUtils.loadFile(inputStream);
            Element ele = XmlUtils.rootElement(doc);
            List<Element> fileNodes = ele.selectNodes("/cacheManager/cache");
            for (Element e : fileNodes) {
                CacheEntity ce = new CacheEntity();
                ce.setDbName(e.attributeValue("dbName"));
                ce.setDbo(e.attributeValue("dbo"));
                ce.setTable(e.attributeValue("table"));
                //生成缓存key
                String key = e.attributeValue("key");
                if (StringUtils.isEmpty(key)) {
                    key = e.attributeValue("table");
                }
                ce.setKey(key);
                setTableAndCondition(ce, e);

                ce.setDatasourceType(datasourceType);
                String columns = StringUtils.isEmpty(e.attributeValue("columns")) ? "*" : e.attributeValue("columns");
                ce.setColumns(columns);

                ce.setToMapField(e.attributeValue("toMapField"));
                String fullCache = StringUtils.isEmpty(e.attributeValue("fullCache")) ? "true" : e
                        .attributeValue("fullCache");
                ce.setFullCache(fullCache);
                ce.setCacheMapData(new HashMap<String, Map<String, Object>>());

                CacheEntity ce2 = new CacheEntity();
                BeanUtils.copyProperties(ce, ce2);
                CACHE.put(ce.getKey(), ce2);
                cacheTabledebugList.add(ce);
            }
            log.debug("缓存管理-->解析缓存的xml文件 end ");
        } catch (DocumentException e) {
            e.printStackTrace();
            log.error("缓存管理-->解析缓存的xml文件异常：{} ", e);
        }
    }

    private boolean isOrderTable(String tableName) {
        if (StringUtils.isEmpty(tableName)) {
            return false;
        }
        return false;
    }

    private void setTableAndCondition(CacheEntity ce, Element e) {

        String tableName = e.attributeValue("table");
        ce.setTable(tableName);
        ce.setConditions(e.attributeValue("conditions"));
//        if (isOrderTable(tableName)) {
//            String tableSubffix = DateUtils.getDateTimeStr(new Date(), DateUtils.YEAR);
//            String endTime = DateUtils.getDateTimeStr(new Date(), DateUtils.YMD) + "23:59:59";
//            String startTime = DateUtils.getDateDayStr(-timeInterval, DateUtils.YMD) + "00:00:00";
//        }
    }

    /**
     * 生成缓存的key
     *
     * @param dbName
     * @param dbo
     * @param table
     * @return
     */
    private String getDatabaseKey(String dbName, String dbo, String table) {

        String key = null;
        if (StringUtils.isNotBlank(datasourceType)) {
            if ("mysql".equals(datasourceType.toLowerCase())) {
                key = dbName + "_" + table;
            } else if ("oracle".equals(datasourceType.toLowerCase())) {
                key = dbName + "_" + table;
            } else if ("sqlserver".equals(datasourceType.toLowerCase())) {
                key = dbName + "_" + dbo + "_" + table;
            } else {
                key = dbName + "_" + table;
            }
        } else {
            //默认mysql
            key = dbName + "_" + table;
        }
        return key;
    }


    /**
     * 刷新缓存中的数据
     */
    @Synchronized
    @Override
    public void refreshCacheData() {
        log.debug("缓存管理-->刷新缓存:缓存刷新开启调用!");
        try {
            // 解析xml文件,并将要缓存的表信息保存入cacheTabledebugList;
            parseCacheXml();
            // 从数据库获取数据，并保存数据
            getDataFromDBSaveToTempCache();
            // 放入缓存中
            SaveToCacheManager();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("缓存管理-->刷新缓存:缓存刷新出现异常", e);
        }
        log.debug("缓存管理-->刷新缓存:缓存刷新调用完成!");
    }


    /**
     * 查询数据 ,封装缓存数据
     *
     * @param ce
     */
    private void getCacheDataToDB(CacheEntity ce) {
        if (ce == null) {
            return;
        }
        try {
            List<Map<String, Object>> list = null;
            ce.setConditions(StringUtils.isEmpty(ce.getConditions()) ? null : ce.getConditions());
//            String table = ce.getTable();
//            if (isOrderTable(table)) {
//                // 查询数据
//                list = cacheEntityDao.getOrderList(ce);
//            } else {
//                // 查询数据
//                list = cacheEntityDao.getDataFromDBSaveToTempCache(ce);
//            }
            // 查询数据
            list = cacheEntityDao.getDataFromDBSaveToTempCache(ce);
//            if (null == ce.getList()) {
            ce.setList(new ArrayList<Map<String, Object>>());
//            }
            // 判断是否需要把全量数据添加到缓存中
            if ("true".equals(ce.getFullCache())) {
                // 添加全量数据到缓存中
                ce.getList().addAll(list);
            }
            // 把需要独立成map的字段遍历，并添加到临时map中
            cacheDataToMap(ce, list);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("缓存管理-->添加缓存:查询数据库报错", e);
        }
    }

    /**
     * 如果缓存为空，再次查询数据库
     *
     * @param cacheEntity
     * @param key
     * @return
     */
    public CacheEntity getEmptyCacheToDB(CacheEntity cacheEntity, String key) {

        log.debug("[ 缓存管理-->查询数据库 start ]");
        if (cacheEntity == null) {
            //查询缓存条件
            cacheEntity = CACHE.get(key);
            if (cacheEntity == null) return cacheEntity;
            //查询数据库
            getCacheDataToDB(cacheEntity);
            //重新放入缓存,设置缓存失效时间
            setRedisDefExpire(key, cacheEntity);
        }
        log.debug("[ 缓存管理-->查询数据库：{}]", cacheEntity);
        log.debug("[ 缓存管理-->查询数据库 end ]");
        return cacheEntity;
    }

    /**
     * 如果缓存为空，再次查询数据库
     *
     * @param json
     * @param key
     * @return
     */
    private CacheEntity getEmptyCacheToDBStr(String json, String key) {

        log.debug("[ 缓存管理-->查询数据库 start ]");
        CacheEntity cacheEntity = null;
        if (StringUtils.isEmpty(json)) {
            //查询缓存条件
            cacheEntity = CACHE.get(key);
            if (cacheEntity == null) return cacheEntity;
            //查询数据库
            getCacheDataToDB(cacheEntity);
            //重新放入缓存,设置缓存失效时间
            setRedisDefExpire(key, cacheEntity);
        }
        log.debug("[ 缓存管理-->查询数据库：{}]", cacheEntity);
        log.debug("[ 缓存管理-->查询数据库 end ]");
        return cacheEntity;
    }


    /**
     * 查询获取数据
     *
     * @param key
     * @return
     */
    public CacheEntity getRedisCache(String key) {

//        log.debug("[ 缓存管理-->查询缓存:查询数据库 start ]");
        CacheEntity cacheEntity = null;
        try {
            cacheEntity = (CacheEntity) redisTemplate.opsForValue().get(key);
            if (cacheEntity == null) {
                //查询数据库
                cacheEntity = getEmptyCacheToDB(cacheEntity, key);
            }
//            String json = redisSelfService.get(key);
//            if (StringUtils.isEmpty(json)) {
//                //查询数据库
//                cacheEntity = getEmptyCacheToDB(json, key);
//            } else {
//                cacheEntity = JSONObject.parseObject(json, CacheEntity.class);
//            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("缓存管理-->查询缓存:查询数据库报错", e);
        }
        return cacheEntity;
    }


    // 获取某个缓存
    @Override
    public List<Map<String, Object>> getCache(String key) {

//        log.debug("[ 缓存管理-->查询缓存:查询数据库 start ]");
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> list = new ArrayList<>();
        try {
//            CacheEntity cacheEntity = getRedisCache(key);
//            if (cacheEntity == null) {
//                cacheEntity = getEmptyCacheToDB(cacheEntity, key);
//                list = null == cacheEntity ? null : cacheEntity.getList();
//            } else {
//                list = cacheEntity.getList();
//                if (list == null || list.size() <= 0) {
//                    cacheEntity = null;
            CacheEntity emptyCacheToDB = getEmptyCacheToDB(null, key);
            list = emptyCacheToDB.getList();
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("缓存管理-->查询缓存:查询数据库报错", e);
        }
        log.debug("[ 缓存管理-->查询缓存耗时: {}]", (System.currentTimeMillis() - startTime));
        return list;
    }


    // 获取某个缓存
    public List<Map<String, Object>> getEmptyCacheToDB(String key) {

//        log.debug("[ 缓存管理-->查询缓存:查询数据库 start ]");
        long startTime = System.currentTimeMillis();
        List<Map<String, Object>> list = new ArrayList<>();
        try {
            CacheEntity cacheEntity = getEmptyCacheToDB(null, key);
            list = cacheEntity.getList();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("缓存管理-->查询缓存:查询数据库报错", e);
        }
        log.debug("[ 缓存管理-->查询缓存耗时: {}]", (System.currentTimeMillis() - startTime));
        return list;
    }


    // 获取某个表的map缓存
    @Override
    public Map<String, Object> getCache(String key, String field) {

//        log.debug("[ 缓存管理-->查询缓存:查询数据库 start ]");
        Map<String, Object> map = new HashMap<>();
        try {
            CacheEntity cacheEntity = getRedisCache(key);
            if (cacheEntity == null) {
                return map;
            } else {
                if (null == cacheEntity.getCacheMapData().get(field)) {
                    return map;
                }
                map = cacheEntity.getCacheMapData().get(field);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[ 缓存管理-->查询缓存异常：{} ]", e);
        }
//        log.debug("[ 缓存管理-->查询缓存:查询数据库 end ]");
        return map;
    }

    /**
     * @param @param tableKey 修改的表名称(例如:WFBDCMain_dbo_WFBDC_tb_Machine,
     *               表示是WFBDCMain数据库dbo对象中的WFBDC_tb_Machine表)
     * @param @param key 单独成map的key
     * @param @param subKey 要修改的子类map的key
     * @param @param subValue 要修改的子类map的key对应的值
     * @return void
     * @author cjy
     * @Description: 修改某个表中具体一个map中的值, 例如：cacheManager.setCacheData("WFBDCMain_dbo_ISIP_tb_Basedebug", "cacheCcicBasedebug", "sdf", "fdasdfffffffff");
     * @date 2017-7-5
     */
    @Synchronized
    @Override
    public void setCacheData(String tableKey, String key, String subKey, Object subValue) {

        log.debug("[ 缓存管理-->修改缓存数据 start ]");
        log.debug("[ 缓存管理-->入参：tableKey：{}, key: {}, subKey: {}, subValue: {} ", tableKey, key, subKey, subValue);
        try {
            CacheEntity cacheEntity = getRedisCache(key);
            if (null != cacheEntity) {
                // 获取子类map是否为空
                if (null != cacheEntity.getCacheMapData().get(key)) {
                    // 更新缓存中的map数据
                    cacheEntity.getCacheMapData().get(key).put(subKey, subValue);
                }
                //重新发入缓存
                setRedisDefExpire(key, cacheEntity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[ 缓存管理-->修改缓存数据异常：{} ]", e);
        }
        log.debug("[ 缓存管理-->修改缓存数据 end ]");
    }

    @Synchronized
    @Override
    public void setCache(String key, Map<String, Object> map) {

        log.debug("[ 缓存管理-->修改缓存数据 start ]");
        log.debug("[ 缓存管理-->入参：tableKey：{}, 内容: {}, ", key, map.size());
        try {
            CacheEntity cacheEntity = getRedisCache(key);
            if (null != cacheEntity) {
                List<Map<String, Object>> list = cacheEntity.getList();
                if (list.contains(map)) {
                    list.remove(map);
                }
                list.add(map);
                //重新发入缓存
                setRedisDefExpire(key, cacheEntity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[ 缓存管理-->修改缓存数据异常：{} ]", e);
        }
        log.debug("[ 缓存管理-->修改缓存数据 end ]");
    }

    @Synchronized
    public void setCache(String key, List<Map<String, Object>> list2) {

        log.debug("[ 缓存管理-->修改缓存数据 start ]");
        log.debug("[ 缓存管理-->入参：tableKey：{}, 内容: {}, ", key, list2.size());
        try {
            if (list2 == null || list2.size() <= 0)
                return;

            CacheEntity cacheEntity = getRedisCache(key);
            if (null != cacheEntity) {
                List<Map<String, Object>> list = cacheEntity.getList();
                if (list.containsAll(list2)) {
                    list.removeAll(list2);
                } else {
                    list.addAll(list2);
                }
                //重新发入缓存
                setRedisDefExpire(key, cacheEntity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[ 缓存管理-->修改缓存数据异常：{} ]", e);
        }
        log.debug("[ 缓存管理-->修改缓存数据 end ]");
    }

    /**
     * 删除缓存数据
     *
     * @param key：缓存key
     */
    @Synchronized
    @Override
    public void remove(String key) {
        CacheEntity cacheEntity = getRedisCache(key);
        List<Map<String, Object>> list = cacheEntity.getList();
        cacheEntity.setList(null);
        redisTemplate.delete(key);
    }

    @Synchronized
    // 缓存中删除数据
    @Override
    public void remove(String key, Map<String, Object> obj) {

        CacheEntity cacheEntity = getRedisCache(key);
        List<Map<String, Object>> list = cacheEntity.getList();
        if (list != null && list.size() > 0) {
            list.forEach(to -> {
                if (to.equals(obj)) {
                    list.remove(obj);
                } else {
                    log.debug(obj + " 缓存中不存在该对象！");
                }
            });
        }
        // 设置缓存失效时间
        setRedisDefExpire(key, cacheEntity);
    }


    /**
     * 设置缓存失效时间
     *
     * @param key
     * @param cacheEntity
     */
    public void setRedisDefExpire(String key, CacheEntity cacheEntity) {
        try {
            //先删除，后添加
            redisTemplate.delete(key);
            redisTemplate.opsForValue().set(key, cacheEntity, REDIS_DEF_EXPIRE, TimeUnit.SECONDS);
            //        redisSelfService.setRedisExpire(key, cacheEntity, REDIS_DEF_EXPIRE);
        } catch (Exception e) {
            log.error("保存缓存数据异常：{}", e.getMessage());
            e.printStackTrace();
        }
    }

    // 销毁缓存
    public void destory() {
//        CACHE.clear();
    }

    /**
     * 刷新缓存
     */
    @Synchronized
    @Override
    public void refreshCacheData(String tableKey) {
        log.debug("[ 写入数据到redis缓存 start ：表名称：{}]", tableKey);
        log.debug("[本地表结构缓存数据：{}]", CACHE);
        CacheEntity cacheEntity = CACHE.get(tableKey);
        log.debug("[本地表结构缓存tableKey：{}，表结构内容：{}]", tableKey, cacheEntity);
        // 查询数据 ,封装缓存数据
        getCacheDataToDB(cacheEntity);
        setRedisDefExpire(tableKey, cacheEntity);
        log.debug("[ 写入数据到redis缓存 end ：表名称：{}，数据：{} ]", cacheEntity, cacheEntity);
    }


    /**
     * 修改或新增物流缓存
     *
     * @param key：自定义物流KEY
     * @param map:         修改数据
     */
    @Synchronized
    @Override
    public void setExpressCache(String key, Map<String, Object> map) {

        log.debug("[ 缓存管理-->修改缓存数据 start ]");
        log.debug("[ 缓存管理-->入参：tableKey：{}, 内容: {}, ", key, map);
        try {
            Map<String, Object> tmap = (Map<String, Object>) redisTemplate.opsForValue().get(key);
            if (tmap != null && tmap.size() > 0) {
                redisTemplate.delete(key);
                redisTemplate.opsForValue().set(key, map);
            } else {
                redisTemplate.delete(key);
                redisTemplate.opsForValue().set(key, map, REDIS_DEF_EXPIRE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[ 缓存管理-->修改缓存数据异常：{} ]", e);
        }
        log.debug("[ 缓存管理-->修改缓存数据 end ]");
    }

    /**
     * 查询物流缓存
     *
     * @param key：自定义物流KEY
     * @return
     */
    @Override
    public Map<String, Object> getExpressCache(String key) {
        Map<String, Object> result = (Map<String, Object>) redisTemplate.opsForValue().get(key);
//        String json = redisSelfService.get(key);
//        Map<String, Object> result = (Map<String, Object>) JSONObject.parse(json);
        return result;
    }
}
