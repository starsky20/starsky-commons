package com.starsky.common.cache.dictionary.job;

import com.starsky.common.cache.dictionary.service.CacheManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Component
@EnableScheduling
public class CacheManager implements InitializingBean {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private ConfigurableEnvironment env;

    @Autowired
    private CacheManagerService cacheManagerService;

    /**
     * 项目启动执行，后续每天晚上12点执行清空，重新加载数据
     *
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() {
        refreshCacheData();
    }


    /**
     * 一个cron表达式有至少6个（也可能7个）有空格分隔的时间元素。按顺序依次为：
     * <p>
     * 秒（0~59）
     * 分钟（0~59）
     * 3 小时（0~23）
     * 4 天（0~31）
     * 5 月（0~11）
     * 6 星期（1~7 1=SUN 或 SUN，MON，TUE，WED，THU，FRI，SAT）
     * 年份（1970－2099）
     * ---------------------
     * 定时执行，每天晚上12点执行清空，重新加载数据
     * <p>
     * fixedDelayString 上一次执行完毕时间点后再次执行；每天定点执行
     */
//    @Scheduled(cron = "0 25 23 ? * *")
    @Scheduled(cron = "0 0 7,13,19 * * ?")  //早7、中午一点、下午23d点执行
//    @Scheduled(cron = "0 0/1 * * * ?")  //每个2分钟执行异常
    protected void loadCacheData() {
        long startTime = System.currentTimeMillis();
        log.info("[ fixedDelayString >>>>>上个任务完成后，每天定点执行一次 ]");
        log.info("[ 基础数据缓存，定时任务执行时间 start：{} ]：", dateFormat.format(new Date()));

        //加载缓存数据
        refreshCacheData();

        log.info("[ 基础数据缓存，定时任务执行时间 end：{} ]：", dateFormat.format(new Date()));
        log.info("[ 基础数据缓存，定时任务执行耗时：{} ]：", (System.currentTimeMillis() - startTime));

    }


    /**
     * 加载缓存数据
     */
    private void refreshCacheData() {
        log.info("缓存管理-->缓存初始化:服务启动!");
        try {
            cacheManagerService.refreshCacheData();
        } catch (Exception e) {
            log.error("缓存管理-->刷新缓存:定时调用刷新服务出现异常", e);
        }
        log.info("缓存管理-->缓存初始化:服务加载结束!");
    }

}
