package com.starsky.common.cache.dictionary.service;

import java.util.List;
import java.util.Map;

/**
 * @desc 缓存处理类
 * @Author wangsh
 * @date 2018/5/6 18:06
 * @return
 */
public interface CacheManagerService {

    /**
     * 刷新缓存
     */
    void refreshCacheData();

    /**
     * 刷新缓存
     */
    void refreshCacheData(String tableKey);

    /**
     * 根据key获取值
     *
     * @param key
     * @return
     */
    List<Map<String, Object>> getCache(String key);

    /**
     * 如果缓存为空，再次查询数据库
     *
     * @param key
     * @param key
     * @return
     */
    List<Map<String, Object>> getEmptyCacheToDB(String key);

    /**
     * 获取某个表的map缓存
     *
     * @param key
     * @param field
     * @return
     */
    Map<String, Object> getCache(String key, String field);


    /**
     * @param @param tableKey 修改的表名称(例如:WFBDCMain_dbo_WFBDC_tb_Machine,
     *               表示是WFBDCMain数据库dbo对象中的WFBDC_tb_Machine表)
     * @param @param key 单独成map的key
     * @param @param subKey 要修改的子类map的key
     * @param @param subValue 要修改的子类map的key对应的值
     * @return void
     * @author cjy
     * @Description: 修改某个表中具体一个map中的值, 例如：cacheManager.setCacheData("WFBDCMain_dbo_ISIP_tb_BaseInfo", "cacheCcicBaseInfo", "sdf", "fdasdfffffffff");
     * @date 2017-7-5
     */

    void setCacheData(String tableKey, String key, String subKey, Object subValue);

    /**
     * 修改数据，更新缓存
     *
     * @param key：表名称作为key
     * @param list2:       修改数据
     */
    void setCache(String key, List<Map<String, Object>> list2);

    /**
     * 修改数据，更新缓存
     *
     * @param key：表名称作为key
     * @param map:         修改数据
     */
    void setCache(String key, Map<String, Object> map);

    /**
     * 删除缓存数据
     *
     * @param key：缓存key
     * @param obj       :数据
     */
    void remove(String key, Map<String, Object> obj);

    /**
     * 删除缓存数据
     *
     * @param key：缓存key
     */
    void remove(String key);

    /**
     * 物流订单缓存
     *
     * @param key：自定义物流KEY
     * @param map:         修改数据
     */
    void setExpressCache(String key, Map<String, Object> map);

    /**
     * 物流订单缓存获取
     *
     * @param key：自定义物流KEY
     */
    Map<String, Object> getExpressCache(String key);
}
