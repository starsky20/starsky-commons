package com.starsky.common.cache.dictionary.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @desc 缓存实体类
 * @Author wangsh
 * @date 2018/5/6 18:11
 * @return
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CacheEntity implements Serializable {
    //数据库名称
    private String dbName;
    //实例名
    private String dbo;
    //表名
    private String table;
    //数据库类型
    private String datasourceType;
    //列名称（多个以逗号分割）
    private String columns;
    //查询条件
    private String conditions;
    private String key;
    //	是否缓存
    private String fullCache;
    private String toMapField;
    private Map<String, Map<String, Object>> cacheMapData;
    private List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

    public CacheEntity() {

    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbo() {
        return dbo;
    }

    public void setDbo(String dbo) {
        this.dbo = dbo;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    /**
     * 获取list
     *
     * @return list list
     */
    public List<Map<String, Object>> getList() {
        return list;
    }

    /**
     * 设置list
     *
     * @param list list
     */
    public void setList(List<Map<String, Object>> list) {
        this.list = list;
    }

    /**
     * 获取columns
     *
     * @return columns columns
     */
    public String getColumns() {
        return columns;
    }

    /**
     * 设置columns
     *
     * @param columns columns
     */
    public void setColumns(String columns) {
        this.columns = columns;
    }

    /**
     * 获取conditions
     *
     * @return conditions conditions
     */
    public String getConditions() {
        return conditions;
    }

    /**
     * 设置conditions
     *
     * @param conditions conditions
     */
    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    /**
     * 获取toMapField
     *
     * @return toMapField toMapField
     */
    public String getToMapField() {
        return toMapField;
    }

    /**
     * 设置toMapField
     *
     * @param toMapField toMapField
     */
    public void setToMapField(String toMapField) {
        this.toMapField = toMapField;
    }

    /**
     * 获取cacheMapData
     *
     * @return cacheMapData cacheMapData
     */
    public Map<String, Map<String, Object>> getCacheMapData() {
        return cacheMapData;
    }

    /**
     * 设置cacheMapData
     *
     * @param cacheMapData cacheMapData
     */
    public void setCacheMapData(Map<String, Map<String, Object>> cacheMapData) {
        this.cacheMapData = cacheMapData;
    }

    /**
     * 获取fullCache
     *
     * @return fullCache fullCache
     */
    public String getFullCache() {
        return fullCache;
    }

    /**
     * 设置fullCache
     *
     * @param fullCache fullCache
     */
    public void setFullCache(String fullCache) {
        this.fullCache = fullCache;
    }

    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }

    @Override
    public String toString() {
        return "CacheEntity{" +
                "dbName='" + dbName + '\'' +
                ", dbo='" + dbo + '\'' +
                ", table='" + table + '\'' +
                ", datasourceType='" + datasourceType + '\'' +
                ", columns='" + columns + '\'' +
                ", conditions='" + conditions + '\'' +
                ", key='" + key + '\'' +
                ", fullCache='" + fullCache + '\'' +
                ", toMapField='" + toMapField + '\'' +
                ", cacheMapData=" + cacheMapData +
                ", list=" + list +
                '}';
    }
}
