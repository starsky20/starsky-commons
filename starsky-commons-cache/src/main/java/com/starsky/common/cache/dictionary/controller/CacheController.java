package com.starsky.common.cache.dictionary.controller;

import com.starsky.common.cache.dictionary.service.CacheManagerService;
import com.starsky.common.data.RstData;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @desc 测试缓存加载数据
 * @Author wangsh
 */
@Api("缓存处理")
@RestController
@RequestMapping("/cache")
public class CacheController {

    @Autowired
    private CacheManagerService cacheManagerService;

    @ApiOperation(value = "刷新缓存信息", notes = "刷新缓存信息", httpMethod = "GET")
    @RequestMapping(value = "/refreshCacheData", method = RequestMethod.GET)
    public RstData refreshCacheData() {
        cacheManagerService.refreshCacheData();
        return RstData.success();
    }

    @ApiOperation(value = "刷新缓存信息", notes = "刷新缓存信息", httpMethod = "GET")
    @RequestMapping(value = "/refreshCacheDataForKey", method = RequestMethod.GET)
    public RstData refreshCacheData(String key) {
        cacheManagerService.refreshCacheData(key);
        return RstData.success();
    }

    @ApiImplicitParam(value = "key", required = true, dataType = "string")
    @ApiOperation(value = "根据key获取缓存信息", notes = "根据key获取缓存信息", httpMethod = "GET")
    @RequestMapping(value = "/getCache", method = RequestMethod.GET)
    public RstData getCache(@ApiParam(value = "缓存信息key") String key) {
        List<Map<String, Object>> list = cacheManagerService.getCache(key);
        System.out.println("缓存数据： " + list);
        return RstData.success(list);
    }

    @ApiImplicitParams({
            @ApiImplicitParam(value = "key", name = "key", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(value = "mapKey", name = "mapKey", required = true, dataType = "string", paramType = "query")
    })
    @ApiOperation(value = "根据key获取缓存信息", notes = "根据key获取缓存信息", httpMethod = "GET")
    @RequestMapping(value = "/getCacheMap", method = RequestMethod.GET)
    public RstData getCacheMap(@ApiParam(value = "缓存信息key") String key, @ApiParam(value = "缓存信息mapKey") String mapKey) {
        Map<String, Object> map = cacheManagerService.getCache(key, mapKey);
        System.out.println("缓存数据： " + map);
        return RstData.success(map);
    }

}
