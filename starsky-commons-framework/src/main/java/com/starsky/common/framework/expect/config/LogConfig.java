package com.starsky.common.framework.expect.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "sys")
@Configuration
public class LogConfig {
    //#在权限系统注册的系统编码
    private String appNo;
    //#在权限系统注册的系统名称
    private String appName;
    //#是否开启异步日志传输
    private Boolean logAsync;
    //异步日志调用接口地址
    private String logAsyncUrl;
    //是否开启登录用户token校验
    private Boolean isAuth;
    //不拦截路径
    private List<String> urls;


//    //1-操作日志保存 OPER_URL
//    private String operUrl = "/operateLog/save";
//
//    //2-系统异常日志保存EXCEP_URL
//    private String excepUrl = "/exceptionLog/save";
//
//    //3-业务异常日志保存 BUSSEXCEP_URL
//    private String bussExcepUrl = "/bussexcepLog/save";
//
//    //4-登陆日志保存 LOGIN_URL
//    private String loginExcepUrl = "/loginLog/save";

}
