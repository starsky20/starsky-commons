package com.starsky.common.framework.expect.constant;

/**
 * @Description TODO log服务请求路径常量类
 */
public enum LogServiceUrlEnum {

    /**
     * 操作日志
     */
    REQ_SERVICE_OPER_URL("/am/v1/pOperateLog/saveRecord"),

    /**
     * 操作日志
     */
    REQ_SERVICE_FIND_OPER_URL("/am/v1/pOperateLog/findRecord"),

    /**
     * 操作日志
     */
    REQ_SERVICE_FIND_PAGE_OPER_URL("/am/v1/pOperateLog/findRecordPage"),

    /**
     * 系统异常日志
     */
    REQ_SERVICE_EXCEP_URL("/am/v1/pExceptionLog/saveRecord"),

    /**
     * 系统异常日志
     */
    REQ_SERVICE_FIND_EXCEP_URL("/am/v1/pExceptionLog/findRecord"),

    /**
     * 系统异常日志
     */
    REQ_SERVICE_FIND_PAGE_EXCEP_URL("/am/v1/pExceptionLog/findRecordPage"),

    /**
     * 业务异常日志
     */
    REQ_SERVICE_BUSSEXCEP_URL("/am/v1/pBussexcepLog/saveRecord"),

    /**
     * 业务异常日志查询
     */
    REQ_SERVICE_FIND_BUSSEXCEP_URL("/am/v1/pBussexcepLog/findRecord"),

    /**
     * 业务异常日志查询
     */
    REQ_SERVICE_FIND_PAGE_BUSSEXCEP_URL("/am/v1/pBussexcepLog/findRecordPage"),

    /**
     * 登陆日志查询
     */
    REQ_SERVICE_FIND_LOGIN_URL("/am/v1/pLoginLog/findRecord"),

    /**
     * 登陆日志查询
     */
    REQ_SERVICE_FIND_PAGE_LOGIN_URL("/am/v1/pLoginLog/findRecordPage");

    LogServiceUrlEnum(String url) {
        this.url = url;
    }

    private String url;

    public String getUrl() {
        return url;
    }

    //通过value获取对应的枚举对象
    public static boolean contains(String value) {
        for (LogServiceUrlEnum examType : LogServiceUrlEnum.values()) {
            if (examType.getUrl().equals(value)) {
                return true;
            }
        }
        return false;
    }

}
