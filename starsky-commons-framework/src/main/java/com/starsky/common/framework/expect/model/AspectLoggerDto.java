package com.starsky.common.framework.expect.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * 操作日志
 */
@Data
@ToString(callSuper = true)
@ApiModel(description = "操作日志")
public class AspectLoggerDto {

    /**
     * id
     */
    @ApiModelProperty(value = "id", name = "id", dataType = "Long")
    private Long id;

    /**
     * 系统编码(唯一)
     */
    @ApiModelProperty(value = "系统编码(唯一)", name = "appNo", dataType = "String")
    private String appNo;
    /**
     * 应用名称(用户所属机构)
     */
    @ApiModelProperty(value = "应用名称(用户所属机构)", name = "appName", dataType = "String")
    private String appName;
    /**
     * 回话ID
     */
    @ApiModelProperty(value = "回话ID", name = "sessionId", dataType = "String")
    private String sessionId;

    /**
     * 日志类型(1-操作日志保存；2-系统异常日志；3-业务异常日志；4-登陆日志)
     */
    @ApiModelProperty(value = "日志类型(1-操作日志保存；2-系统异常日志；3-业务异常日志；4-登陆日志)", name = "logType", dataType = "Integer")
    private Integer logType;
    /**
     * 登陆名
     */
    @ApiModelProperty(value = "登陆名", name = "loginName", dataType = "String")
    private String loginName;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", name = "userId", dataType = "String")
    private String userId;
    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名", name = "userName", dataType = "String")
    private String userName;
    /**
     * 服务名称
     */
    @ApiModelProperty(value = "服务名称", name = "serverName", dataType = "String")
    private String serverName;

    @ApiModelProperty(value = "操作名称", dataType = "opertionTime")
    private Date opertionTime;

    @ApiModelProperty(value = "操作名称", dataType = "String")
    private String opertionName;

    @ApiModelProperty(value = "操作说明", dataType = "String")
    private String opertionDesc;
    /**
     * 请求Ip
     */
    @ApiModelProperty(value = "请求Ip", name = "reqIp", dataType = "String")
    private String reqIp;
    /**
     * 请求Url
     */
    @ApiModelProperty(value = "请求Url", name = "reqUrl", dataType = "String")
    private String reqUrl;
    /**
     * 请求类型(get/post/delete/put)
     */
    @ApiModelProperty(value = "请求类型", name = "methodType", dataType = "String")
    private String methodType;
    /**
     * 请求数据类型(json/txt/html/xml)
     */
    @ApiModelProperty(value = "请求数据类型(json/txt/html/xml)", name = "contentType", dataType = "String")
    private String contentType;
    /**
     * 请求编码方式(gbk/utf-8)
     */
    @ApiModelProperty(value = "请求编码方式(gbk/utf-8)", name = "characterSet", dataType = "String")
    private String characterSet;
    /**
     * 请求国家或者语言
     */
    @ApiModelProperty(value = "请求国家或者语言", name = "locale", dataType = "String")
    private String locale;
    /**
     * 请求浏览器及操作系统
     */
    @ApiModelProperty(value = "请求浏览器及操作系统", name = "userAgent", dataType = "String")
    private String userAgent;
    /**
     * 类名
     */
    @ApiModelProperty(value = "类名", name = "className", dataType = "String")
    private String className;
    /**
     * 方法名称
     */
    @ApiModelProperty(value = "方法名称", name = "methodName", dataType = "String")
    private String methodName;
    /**
     * 方法描述
     */
    @ApiModelProperty(value = "方法描述", name = "methodDesc", dataType = "String")
    private String methodDesc;
    /**
     * 请求参数
     */
    @ApiModelProperty(value = "请求参数", name = "reqParam", dataType = "String")
    private String reqParam;
    /**
     * 响应结果
     */
    @ApiModelProperty(value = "响应结果", name = "resParam", dataType = "String")
    private String resParam;

    /**
     * 响应编码
     */
    @ApiModelProperty(value = "响应编码", name = "respCode", dataType = "String")
    private String respCode;
    /**
     * 响应消息
     */
    @ApiModelProperty(value = "响应消息", name = "respMsg", dataType = "String")
    private String respMsg;
    /**
     * 异常结果
     */
    @ApiModelProperty(value = "异常结果", name = "excepRst", dataType = "String")
    private String excepRst;

    /**
     * 起始时间
     */
    @ApiModelProperty(value = "起始时间", name = "startTime", dataType = "Date")
    private Date startTime;
    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间", name = "endTime", dataType = "Date")
    private Date endTime;
    /**
     * 耗时(ms)
     */
    @ApiModelProperty(value = "耗时(ms)", name = "useTime", dataType = "String")
    private String useTime;

    /**
     * 进程日志Id
     */
    @ApiModelProperty(value = "进程日志Id", name = "execLogId", dataType = "String")
    private String execLogId;
    /**
     * 执行服务器Ip
     */
    @ApiModelProperty(value = "执行服务器Ip", name = "execServerIp", dataType = "String")
    private String execServerIp;
    /**
     * 执行服务器时间
     */
    @ApiModelProperty(value = "执行服务器时间", name = "execServerTime", dataType = "Date")
    private Date execServerTime;
    /**
     * 创建用户
     */
    @ApiModelProperty(value = "创建用户", name = "creator", dataType = "String")
    private String creator;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", name = "createDate", dataType = "Date")
    private Date createDate;
    /**
     * 最终修改用户
     */
    @ApiModelProperty(value = "最终修改用户", name = "updater", dataType = "String")
    private String updater;
    /**
     * 最终修改时间
     */
    @ApiModelProperty(value = "最终修改时间", name = "updateDate", dataType = "Date")
    private Date updateDate;

}
