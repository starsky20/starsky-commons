package com.starsky.common.framework.expect.service;

import com.starsky.common.framework.expect.model.AspectLoggerDto;
import org.springframework.scheduling.annotation.Async;

/**
 * @Description TODO 公共基础服务接口
 */
public interface LogService {

    /**
     * 异步发送日志
     *
     * @param reqUrl:访问url
     * @param obj:         日志内容
     * @param logType:     1-操作日志保存；2-系统异常日志；3-业务异常日志；4-登陆日志
     */
    @Async
    void sendLog(String reqUrl, AspectLoggerDto obj, int logType);

}
