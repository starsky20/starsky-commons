package com.starsky.common.hbase.util;

import com.starsky.common.hbase.constant.HBaseConst;
import com.starsky.common.hbase.entity.HBaseUser;
import com.starsky.common.hbase.entity.InsertData;
import com.starsky.common.utils.FastJsonUtils;
import com.starsky.common.utils.UUIDUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * rowkey生成工具类
 */
public class HBaseUtil {

    /**
     * 生成hbase数据主键rowkey
     * @param rowkeyPrefix
     * @param userId
     * @return
     */
    public static String getRowKey(String rowkeyPrefix, String userId) {
        return rowkeyPrefix + userId + "_" + Math.abs(UUIDUtils.newGUID().hashCode());
    }

    public static void main(String[] args) {
        System.out.println(getRowKey("R", "123456"));
    }


    /**
     * 获取hbase存储数据格式
     *
     * @param family
     * @param qualifier
     * @param value
     * @return
     */
    public static InsertData getInsertData(String family, String qualifier, String value) {
        InsertData insertData = new InsertData(family, qualifier, value);
        return insertData;
    }

    /**
     * 获取hbase默认数据格式
     *
     * @param qualifier
     * @param value
     * @return
     */
    public static InsertData getDefaultInsertData(String qualifier, String value) {
        InsertData insertData = new InsertData(HBaseConst.FAMILY, qualifier, value);
        return insertData;
    }

    /**
     * 创建hbase数据格式
     *
     * @param json: 对象json字符串
     * @return
     */
    public static List<InsertData> getHbaseData(String json) {

        List<InsertData> resList = new ArrayList<>();
        Map<String, String> map = FastJsonUtils.stringToMap(json);
        if (map == null || map.size() <= 0) {
            return resList;
        }
        for (String key : map.keySet()) {
            InsertData insertData = new InsertData(HBaseConst.FAMILY, key, map.get(key));
            resList.add(insertData);
        }
        return resList;
    }

    /**
     * 创建hbase数据格式
     *
     * @param rowKeyPrefix: 主键rowkey
     * @param userId: 用户id
     * @param json: 对象json字符串
     * @return
     */
    public static HBaseUser getHBaseData(String tableName, String rowKeyPrefix, String userId, String json) {
        HBaseUser user = new HBaseUser();
        user.setRowKey(HBaseUtil.getRowKey(rowKeyPrefix, userId));
        List<InsertData> hbaseData = getHbaseData(json);
        user.setQualifierValues(hbaseData);
        return user;
    }

    /**
     * 创建hbase数据格式
     * @param rowKeyPrefix: 主键rowkey
     * @param userId: 用户id
     * @param userData: 存储数据
     * @return
     */
    public static HBaseUser getHBaseData(String tableName,String rowKeyPrefix, String userId, Map<String, Object> userData) {

        HBaseUser user = new HBaseUser();
        user.setTableName(tableName);
        user.setRowKey(HBaseUtil.getRowKey(rowKeyPrefix, userId));
        user.setQualifierValues(new ArrayList<>());
        for (String key : userData.keySet()) {
            user.getQualifierValues().add(HBaseUtil.getDefaultInsertData(key, FastJsonUtils.toJson(userData.get(key))));
        }
        return user;
    }
}
