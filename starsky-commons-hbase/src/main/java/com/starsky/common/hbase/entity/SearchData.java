package com.starsky.common.hbase.entity;

import lombok.Data;

import java.util.Date;

/**
 * Title: SearchData类
 * Description: TODO 类描述
 */
@Data
public class SearchData {

    // 表名
    private String tableName;
    // 键名
    private String family;
    // 键族名
    private String qualifier;
    // 值
    private String val;
    // 查询开始行
    private String startRow;
    // 查询结束行
    private String endRow;
    // 查询开始时间
    private Date searchStartTime;
    // 查询结束时间
    private Date searchEndTime;

}
