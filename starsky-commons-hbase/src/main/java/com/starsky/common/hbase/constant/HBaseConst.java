package com.starsky.common.hbase.constant;

public class HBaseConst {

    public static final String FAMILY = "INFO";

    // 用户登录注册表
    public static final String LOG_USER = "LOG_USER";

    //产品表
    public static final String LOG_PROD = "LOG_PROD";
    //优惠卷表
    public static final String LOG_COUPON = "LOG_COUPON";

    //订单表
    public static final String LOG_ORDER = "LOG_ORDER";

    //用户浏览
    public static final String LOG_BROWSE = "LOG_BROWSE";

    //搜索表
    public static final String LOG_KEYWORD = "LOG_KEYWORD";

    //客服咨询
    public static final String LOG_CONSULT = "LOG_CONSULT";

    //商品评价
    public static final String LOG_APPRAISE = "LOG_APPRAISE";

    //用户预约
    public static final String LOG_RESERVATION = "LOG_RESERVATION";


}
