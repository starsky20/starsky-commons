package com.starsky.common.hbase.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Title: InsertData类
 * Description: TODO 类描述
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsertData {
    // 列族名称(例如，info)
    private String family;
    // 列名称(例如，info:userName)
    private String qualifier;

    // 列对应的值（例如，info:userName-> 1 ）
    private String data;
}
