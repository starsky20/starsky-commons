package com.starsky.common.hbase.config;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.hadoop.hbase.HbaseTemplate;

/**
 * Title: HbaseConfig类
 * Description: TODO 类描述
 * <p>
 * 修改记录:
 * 版本号    修改人        修改日期       修改内容
 *
 * @author tianboqun
 * @version 1.00.00 创建时间：2020/6/1 14:20
 */
@org.springframework.context.annotation.Configuration
public class HBaseConfig {

    @Value("${hbase.zookeeper.property.clientPort}")
    private String zookeeperProt;
    @Value("${hbase.zookeeper.quorum}")
    private String zookeeperQuorum;
    @Value("${zookeeper.recovery.retry}")
    private String zookeeperRetrys;
    @Value("${hbase.client.retries.number}")
    private String clientRetrys;


    @Bean
    public HbaseTemplate hbaseTemplate() {
        HbaseTemplate hbaseTemplate = new HbaseTemplate();
        hbaseTemplate.setConfiguration(configuration());
        hbaseTemplate.setAutoFlush(true);
        return hbaseTemplate;
    }

    public Configuration configuration() {

        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.property.clientPort",zookeeperProt);
        configuration.set("hbase.zookeeper.quorum",zookeeperQuorum);
        configuration.set("hbase.client.retries.number",clientRetrys);
        configuration.set("zookeeper.recovery.retry",zookeeperRetrys);
        return configuration;
    }


}
