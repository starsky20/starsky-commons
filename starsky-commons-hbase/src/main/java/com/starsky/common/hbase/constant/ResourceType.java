package com.starsky.common.hbase.constant;


public class ResourceType {
    //列族
    public static final String FAMILY = "INFO";

    // 用户打开关闭小程序
    public static final String USER_APP = "A";

    //用户浏览
    public static final String USER_BROWSE = "B";

    //客服咨询
    public static final String CONSULT = "C";

    // 用户页面分享
    public static final String Page_SHARE = "D";

    // 页面元素触发
    public static final String PAGE_EVENT = "E";

    // 优惠卷活动
    public static final String COUPON = "N";

    // 渠道上报
    public static final String CHANNEL = "H";

    // 用户登录
    public static final String USER_LOGIN ="L";

    //会员接口
    public static final String MEMBER = "M";

    // 订单采集
    public static final String ORDER = "O";

    //商品采集
    public static final String PROD_COLLECT = "P";

    // 用户注册
    public static final String USER_REGIST = "R";

    //搜索运营
    public static final String SEEARCH = "S";

    // 用户预约
    public static final String USER_RESERVATION = "T";

    // 数据仓库
    public static final String WARE_HOUSE = "W";

}
