package com.starsky.common.hbase.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * Title: HBaseProperties类
 * Description: TODO 类描述
 * <p>
 * 修改记录:
 * 版本号    修改人        修改日期       修改内容
 *
 * @author tianboqun
 * @version 1.00.00 创建时间：2020/6/1 14:21
 */
@Data
@ConfigurationProperties(prefix = "hbase")
public class HBaseProperties {
    //初始化hbase属性
    private Map<String, String> config;
}
