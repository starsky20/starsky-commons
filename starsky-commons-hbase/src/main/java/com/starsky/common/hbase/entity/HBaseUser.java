package com.starsky.common.hbase.entity;

import lombok.Data;

import java.util.List;

/**
 * Title: HBaseUser类
 * Description: TODO 类描述
 */
@Data
public class HBaseUser {

    // 表名
    private String tableName;

    // 用户对应的rowKey
    private String rowKey;

    // 列族-值 集合
    private List<InsertData> qualifierValues;


}
