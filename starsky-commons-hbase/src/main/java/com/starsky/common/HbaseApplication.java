package com.starsky.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;

/**
 * 营销系统-数据分析
 */
@EnableAutoConfiguration(exclude = {RedisAutoConfiguration.class})
@SpringBootApplication(scanBasePackages = "com.*")
public class HbaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(HbaseApplication.class, args);
    }

}
