package com.starsky.common.beetlsql.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * 系统用户，用于session存储
 */
@Data
@ToString(callSuper = true)
public class SysUser extends BaseDomain {

    @ApiModelProperty(value = "登录用户名", dataType = "String")
    private String loginName;

    @ApiModelProperty(value = "登录密码", dataType = "String")
    private String loginPwd;

    @ApiModelProperty(value = "加密key", dataType = "String")
    private String salt;

    @ApiModelProperty(value = "姓名", dataType = "String")
    private String userName;

    @ApiModelProperty(value = "性别(字典表)", dataType = "Integer")
    private Integer userSex;

    @ApiModelProperty(value = "出生日期", dataType = "Date")
    private Date userBirth;

    @ApiModelProperty(value = "移动电话", dataType = "String")
    private String userPhone;

    @ApiModelProperty(value = "身份证号", dataType = "String")
    private String userCardNo;

    @ApiModelProperty(value = "Email", dataType = "String")
    private String userEmail;

    @ApiModelProperty(value = "头像", dataType = "String")
    private String userPhoto;

    @ApiModelProperty(value = "用户地址", dataType = "String")
    private String userAddr;

    @ApiModelProperty(value = "用户类型(0-平台用户 1-仓库管理者 2-委托公司用户 3-注册仓储用户)", dataType = "Integer")
    private Integer userType;

    @ApiModelProperty(value = "省编码", dataType = "String")
    private String provinceCode;

    @ApiModelProperty(value = "省名称", dataType = "String")
    private String provinceName;

    @ApiModelProperty(value = "市编码", dataType = "String")
    private String cityCode;

    @ApiModelProperty(value = "市名称", dataType = "String")
    private String cityName;

    @ApiModelProperty(value = "区编码", dataType = "String")
    private String areaCode;

    @ApiModelProperty(value = "区名称", dataType = "String")
    private String areaName;

    @ApiModelProperty(value = "启用状态(1-启用 0-停用)", dataType = "Integer")
    private Integer enableStatus;

    @ApiModelProperty(value = "紧急联系人", dataType = "String")
    private String emergencyContact;

    @ApiModelProperty(value = "紧急联系人电话", dataType = "String")
    private String emergencyContactPhone;

    @ApiModelProperty(value = "紧急联系人邮箱", dataType = "String")
    private String emergencyContactEmail;

    @ApiModelProperty(value = "上级用户Id", dataType = "String")
    private String parentId;

    @ApiModelProperty(value = "备注", dataType = "String")
    private String remark;

    @ApiModelProperty(value = "审核状态(0-未审核 1-已审核 )(预留)", dataType = "Integer")
    private Integer chkStatus;

    @ApiModelProperty(value = "认证状态(0-未认证 1-已认证 )(预留)", dataType = "Integer")
    private Integer authStatus;

    @ApiModelProperty(value = "激活时间", dataType = "Date")
    private Date effectStartTime;

    @ApiModelProperty(value = "截止时间", dataType = "Date")
    private Date effectEndTime;

    @ApiModelProperty(value = "登录失败次数", dataType = "Integer")
    private Integer loginFailNum;

    @ApiModelProperty(value = "锁定状态(1-锁定 0-正常)", dataType = "Integer")
    private Integer lockStatus;

    @ApiModelProperty(value = "微信头像", dataType = "String")
    private String wxHeadImg;

    @ApiModelProperty(value = "微信昵称", dataType = "String")
    private String wxNickName;

    @ApiModelProperty(value = "微信openId", dataType = "String")
    private String wxOpenId;

    @ApiModelProperty(value = "微信unionId", dataType = "String")
    private String wxUnionId;

    @ApiModelProperty(value = "微信公众号关注状态", dataType = "String")
    private String wxSubscribe;

    //*************** ↓↓↓↓↓↓↓↓↓↓↓↓↓ 自定义属性 ↓↓↓↓↓↓↓↓↓↓↓↓↓ ****************************************************************
    @ApiModelProperty(value = "新登陆密码", dataType = "String")
    private String loginPwdNew;

    @ApiModelProperty(value = "使用起止时间", dataType = "String")
    private String useStartTime;

    @ApiModelProperty(value = "使用起止时间", dataType = "String")
    private String useEndTime;

    @ApiModelProperty(value = "创建起止时间", dataType = "String")
    private String createStartTime;

    @ApiModelProperty(value = "创建起止时间", dataType = "String")
    private String createEndTime;

    @ApiModelProperty(value = "最后操作Ip地址", dataType = "String")
    private String lastOperIp;

    @ApiModelProperty(value = "最后操作mac地址", dataType = "String")
    private String lastOperMac;

    @ApiModelProperty(value = "执行服务器Ip", dataType = "String")
    private String execServerIp;

    @ApiModelProperty(value = "执行服务器时间", dataType = "Date")
    private Date execServerTime;

    @ApiModelProperty(value = "所属公司", dataType = "String")
    private String orgName;

    @ApiModelProperty(value = "用户角色Id", dataType = "String")
    private String roleId;

    @ApiModelProperty(value = "用户角色", dataType = "String")
    private String roleName;

    @ApiModelProperty(value = "系统编码", dataType = "String")
    private String appNo;

    @ApiModelProperty(value = "用户令牌", dataType = "String")
    private String userToken;
    //*************** ↑↑↑↑↑↑↑↑↑↑↑↑↑ 自定义属性 ↑↑↑↑↑↑↑↑↑↑↑↑↑ ****************************************************************
    @ApiModelProperty(value = "菜单类型", dataType = "Integer")
    private Integer type;

}
