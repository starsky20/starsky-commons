package com.starsky.common.beetlsql.config.druid;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @author: fengyuan
 * @date 2018-12-25 13:38:57
 * @Description TODO druid监控配置初始化
 */
@Configuration
public class DruidMonitorConfig {

    @Autowired
    Environment env;

    @Bean
    public ServletRegistrationBean DruidStatViewServle() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        // 添加初始化参数：initParams
        String allow = env.getProperty("druidMonitor.allow");
        String deny = env.getProperty("druidMonitor.deny");
        String loginUsername = env.getProperty("druidMonitor.loginUsername");
        String loginPassword = env.getProperty("druidMonitor.loginPassword");
        String resetEnable = env.getProperty("druidMonitor.resetEnable");
        if (StringUtils.isNotEmpty(allow)) {
//		// 白名单：
            servletRegistrationBean.addInitParameter("allow", allow);
        }
        if (StringUtils.isNotEmpty(deny)) {
            // IP黑名单 (存在共同时，deny优先于allow) : 如果满足deny的话提示:Sorry, you are not
            // permitted to view this page.
            servletRegistrationBean.addInitParameter("deny", deny);
        }
        // 登录查看信息的账号密码.
        servletRegistrationBean.addInitParameter("loginUsername", loginUsername);
        servletRegistrationBean.addInitParameter("loginPassword", loginPassword);
        if (StringUtils.isNotEmpty(resetEnable)) {
            // 是否能够重置数据.
            servletRegistrationBean.addInitParameter("resetEnable", resetEnable);
        }

        return servletRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean druidStatFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
        // 添加过滤规则.
        filterRegistrationBean.addUrlPatterns("/*");
        // 添加不需要忽略的格式信息.
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");

        return filterRegistrationBean;

    }

}
