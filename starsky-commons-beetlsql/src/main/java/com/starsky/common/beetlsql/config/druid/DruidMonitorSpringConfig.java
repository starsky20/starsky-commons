package com.starsky.common.beetlsql.config.druid;

import com.alibaba.druid.support.spring.stat.BeanTypeAutoProxyCreator;
import com.alibaba.druid.support.spring.stat.DruidStatInterceptor;
import com.starsky.common.beetlsql.base.BaseService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: fengyuan
 * @date 2018-12-25 13:38:57
 * @Description TODO druid spring方法监控引入
 */
@Configuration
public class DruidMonitorSpringConfig {

	@Bean(name="druid-stat-interceptor")
	public DruidStatInterceptor druidStatInterceptorInit() {
		DruidStatInterceptor druidStatInterceptor = new DruidStatInterceptor();
		return druidStatInterceptor;
	}

	@Bean(name="druid-type-proxyCreator")
	public BeanTypeAutoProxyCreator beanTypeAutoProxyCreatorInit(){
		BeanTypeAutoProxyCreator beanTypeAutoProxyCreator = new BeanTypeAutoProxyCreator();
		beanTypeAutoProxyCreator.setTargetBeanType(BaseService.class);
		beanTypeAutoProxyCreator.setInterceptorNames("druid-stat-interceptor");
		return beanTypeAutoProxyCreator;
	}

}
