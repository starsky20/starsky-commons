package com.starsky.common.beetlsql.exception;

import com.starsky.common.data.RstData;
import com.starsky.common.enums.impl.EnumResult;
import org.beetl.sql.core.BeetlSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理
 */
@ControllerAdvice
public class BeetlSqlExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(BeetlSqlExceptionHandler.class);

    @ResponseBody
    @ExceptionHandler(BeetlSQLException.class)
    public RstData beetlSQLException(BeetlSQLException e) {
        logger.error("捕获到beetlsql异常", e);
        return RstData.faild(EnumResult.FAILED, "查询数据库异常");
    }

}
