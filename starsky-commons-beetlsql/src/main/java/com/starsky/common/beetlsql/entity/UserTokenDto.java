package com.starsky.common.beetlsql.entity;

import lombok.Data;

import java.util.List;

/**
 * 用于会话存储，前端用户信息
 */
@Data
public class UserTokenDto {
    //用户基本信息
    public String userId;
    public String userName;
    public String userToken;
    public Integer userType;

    //当前登录用户机构id（平台用户/仓库经营者/委托公司/仓库用户）
    private String curOrgId;

    //当前登录用户机构id（平台用户/仓库经营者/委托公司/仓库用户）
    private String curOrgName;

    //当前登录仓库用户机构id（仓库id）
    private String curWareId;
    //当前登录仓库用户机构id（仓库id）
    private String curWareName;

    //委托公司机构
    private String wtgsOrgId;

    //委托公司用户机构id集合
    private List<String> orgIds;

    //仓库用户机构id（仓库id）
    private List<String> wareIds;

    //用户信息
    private SysUser user;

    // 登录设备0-主机登录 1-PDA登录
    private int loginToken;

    // 设备编码
    private String terminalCode;

    /**
     * 默认构造函数，支持反序列化
     */
    public UserTokenDto() {
    }


}
