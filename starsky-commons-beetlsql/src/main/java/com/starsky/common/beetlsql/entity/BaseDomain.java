package com.starsky.common.beetlsql.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.beetl.sql.core.TailBean;
import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Version;

import java.util.Date;
import java.util.List;

@Data
@ToString(callSuper = true)
@ApiModel(value = "entity基类")
public class BaseDomain extends TailBean {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "实体基类ID", dataType = "String")
    private String id;

    @ApiModelProperty(value = "创建时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "创建时间", dataType = "String")
    private String createUser;

    @ApiModelProperty(value = "修改时间", dataType = "Date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastModifyTime;

    @ApiModelProperty(value = "修改人", dataType = "String")
    private String lastModifyUser;

    @ApiModelProperty(value = "数据版本", dataType = "Integer")
    private Integer version;

    @ApiModelProperty(value = "数据库名后缀", dataType = "String")
    private String dataBaseSubfix;

    @ApiModelProperty(value = "表名后缀", dataType = "String")
    private String tableSubfix;


    //用户基本信息
    @ApiModelProperty(value = "用户id", dataType = "String")

    public String userId;
    @ApiModelProperty(value = "用户名称", dataType = "String")
    public String userName;

    @ApiModelProperty(value = "用户token", dataType = "String")
    public String userToken;
    @ApiModelProperty(value = "用户类型", dataType = "String")
    public Integer userType;

    @ApiModelProperty(value = "当前登录用户机构id", dataType = "String")
    //当前登录用户机构id（平台用户/仓库经营者/委托公司/仓库用户）
    private String curOrgId;
    @ApiModelProperty(value = "当前登录用户机构名称", dataType = "String")
    //当前登录用户机构id（平台用户/仓库经营者/委托公司/仓库用户）
    private String curOrgName;

    @ApiModelProperty(value = "当前登录用户仓库id", dataType = "String")
    //当前登录仓库用户机构id（仓库id）
    private String curWareId;

    @ApiModelProperty(value = "当前登录用户仓库名称", dataType = "String")
    //当前登录仓库用户机构id（仓库id）
    private String curWareName;

    @ApiModelProperty(value = "委托公司机构id", dataType = "String")
    //委托公司机构
    private String wtgsOrgId;

    //委托公司用户机构id集合
    private List<String> orgIds;

    //仓库用户机构id（仓库id）
    private List<String> wareIds;


    @AssignID("WbsUuidPk")
    public String getId() {
        return id;
    }

    @Version
    public Integer getVersion() {
        return version;
    }

}
