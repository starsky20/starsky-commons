package com.starsky.common.beetlsql.util;

import com.alibaba.fastjson.JSONObject;
import com.starsky.common.beetlsql.entity.UserTokenDto;
import com.starsky.common.exception.impl.ExceptionBusiness;
import com.starsky.common.enums.impl.EnumResult;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 实体参考拷贝
 */
public class ParamsCopy {

    /**
     * 将obj对象数据封装到 指定对象中
     */
    public static Map<String, Object> copyParams(Map<String, Object> paraMap, UserTokenDto obj) {

        if (obj == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000001);
        }
        // 全部采用集合方式传递数据 一级json
        if (paraMap == null) {
            paraMap = new HashMap<>();
        }

        // 业务处理
        String json = JSONObject.toJSONString(obj);
        if (StringUtils.isEmpty(json)) {
            return paraMap;
        }
        Map map = JSONObject.parseObject(json);
        if (map == null || map.size() <= 0) {
            return paraMap;
        }
        paraMap.putAll(map);

        return paraMap;
    }

    /**
     * 将obj对象数据封装到 指定对象中
     */
    public static Object copyParams(Object src, Object desc,Class clazz) {

        if (src == null) {
            throw ExceptionBusiness.getIntance(EnumResult.W000001);
        }
        // 全部采用集合方式传递数据 一级json
        if (desc == null) {
            return desc;
        }

        JSONObject jObject = (JSONObject) JSONObject.toJSON(src);
        JSONObject descObj = (JSONObject) JSONObject.toJSON(desc);
        descObj.putAll(jObject);
        Object obj = JSONObject.toJavaObject(descObj, clazz);

        return obj;
    }
}
