package com.starsky.common.beetlsql.base;

import com.alibaba.fastjson.JSONObject;
import com.starsky.common.beetlsql.entity.SysUser;
import com.starsky.common.beetlsql.entity.UserTokenDto;
import com.starsky.common.enums.impl.EnumResult;
import com.starsky.common.exception.impl.ExceptionBusiness;
import com.starsky.common.redis.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
public class BaseServiceImpl {

    /**
     * session存储key
     */
    public static final String SESSION_USER = "user";

    @Autowired
    protected HttpServletRequest request;
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 从session中获取用户对象，只能适用权限系统(wbs-order-auth)获取用户
     */
    public UserTokenDto getUserToken() {
        String userToken = request.getHeader("X-Auth-Token");
        log.info("从请求头获取用户信息userToken： " + userToken);
        String userInfo = (String) this.redisUtils.hGet("spring:sessions:" + userToken, "sessionAttr:user");
        if (StringUtils.isEmpty(userInfo)) {
            log.info("从redis取出登录用户信息为空!");
            throw ExceptionBusiness.getIntance(EnumResult.W000010);
        }
        UserTokenDto userTokenDto = JSONObject.parseObject(userInfo, UserTokenDto.class);
        if (userTokenDto == null) {
            log.error("从redis取出登录用户信息，转换为UserTokenDto对象为空！");
            throw ExceptionBusiness.getIntance(EnumResult.W000010);
        }
        return userTokenDto;
    }

    /**
     * 从session中获取用户对象，只能适用订单中心管理、仓库端获取用户
     */
    public UserTokenDto getToken() {
        //方便测试使用，权限接入后修改此处
        SysUser sysUser = new SysUser();
        sysUser.setUserName("admin");
        sysUser.setId("1");

        UserTokenDto userTokenDto = new UserTokenDto();
        if (request == null) {
            userTokenDto.setUser(sysUser);
        } else {
            String userId = request.getHeader("X-Auth-Id");
            log.info("从请求头获取用户信息userId： " + userId);
            String json = (String) redisUtils.get(userId);
            if (StringUtils.isEmpty(json)) {
                log.error("从redisPubService中取出登录用户信息为空，userId： " + userId);
                throw ExceptionBusiness.getIntance(EnumResult.W000010);
            }
            UserTokenDto userTokenDto2 = JSONObject.parseObject(json, UserTokenDto.class);
            if (userTokenDto2 == null) {
                log.error("从redisPubService中取出登录用户信息为空，userId： " + userId);
                userTokenDto.setUser(sysUser);
                return userTokenDto;
            } else {
                log.info("从redisPubService中取出登录用户UserTokenDto成功！ ");
                return userTokenDto2;
            }
        }
        return userTokenDto;
    }


//    public UserTokenDto getToken() {
//        //方便测试使用，权限接入后修改此处
//        SysUser sysUser = new SysUser();
//        sysUser.setUserName("admin");
//        sysUser.setId("1");
//
//        String userId = request.getHeader("X-Auth-Id");
//        String json = redisSelfService.get(userId);
//
//        if (request == null) {
//            return new UserTokenDto("1", "1", sysUser, null);
//        } else if (request.getSession().getAttribute(SESSION_USER) == null) {
//            return new UserTokenDto("1", "1", sysUser, null);
//        }
//        UserTokenDto userTokenDto = (UserTokenDto) request.getSession().getAttribute(SESSION_USER);
//        if (userTokenDto == null) {
//            userTokenDto = new UserTokenDto("1", "1", sysUser, null);
//        }
//        return userTokenDto;
//    }

    /**
     * 获取http session
     */
    public HttpSession getSession() {
        if (request == null) {
            return null;
        }
        return request.getSession();
    }

    /**
     * 获取session值
     */
    public <T> T getSessionValue(String key) {
        HttpSession session = getSession();
        if (session != null) {
            return (T) session.getAttribute(key);
        }
        return null;
    }

    /**
     * 键值对存入session
     */
    public void putSessionValue(String key, Object value) {
        HttpSession session = getSession();
        if (session != null) {
            session.setAttribute(key, value);
        }
    }

    /**
     * 获取session id
     */
    public String getSessionId() {
        HttpSession session = getSession();
        if (session != null) {
            return session.getId();
        }
        return null;
    }

    /**
     * 无效session
     */
    public void invalidSession() {
        HttpSession session = getSession();
        if (session != null) {
            session.invalidate();
        }
    }

}






